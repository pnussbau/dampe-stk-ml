# A CNN method for trajectory reconstruction of gamma rays with the DAMPE space mission

* This project contains the tools necessary to generate the training data, train the model, infere tracks from data using the trained models,and diverse model performance visualization tools.
* Disclaimer: This project does not contain any DAMPE data as the author does **not** have permission to share data!

## Content of the repository
* [Event display](tools/visualizeEvent.py) ```tools/visualizeEvent.py```

CNN tool chain:

* [Training data generator](preprocess_data.py) ```preprocess_data.py```
* [Model training and hyperparameter search](train.py) ```train.py```
* [Using a model to perform inference](inference.py) ```inference.py```
* [SBATCH scripts](cluster/) ```cluster/``` for executing the abovementioned programs on the Yggdrasil SLURM cluster
* [Trained models](models/) ```models/```
* [Visualization tools](tools/) ```tools/```


## Software Requirements
The code was developed and tested for the CVMFS environment [```LCG104_cuda```](https://lcginfo.cern.ch/release/104cuda/)

## Event display
Python-based [event display](tools/visualizeEvent.py) for DAMPE event data, utilizing Numpy and Matplotlib for rendering and Uproot5 for ROOT file reading, featuring CLI for event analysis, filtering, and PDF export capabilities.

If drawing a MC event the ```--mc``` option has to be specified to display information about the true particle.

The command line tool can draw a single event using the ```--id``` option or drawing multiple events by specifying certain required features. For example by specifying a reconstructed energy $E_{reco}$ range using the ```--e-range``` option. Further it is also possible to ask for at least one track in the STK using the ```--must-contain-stk-track``` option. 
The projections that shall be drawn are specified using the ```--projections``` option.

The output is stored in the directory specified by the ```--output-dir``` option. The file format is specified using the ```--suffix``` option. 
The files are either stored as separate files or as a single PDF file. The latter is specified using the ```--single-file``` option.

**A complete  overview of the command line options can be obtained by using the `--help` option.**

By default only ```N_BEST_TRACKS = 5```best tracks are drawn.

#### Usage examples

Drawing a single MC event (ID 38) that contains at least one STK track and the legend in a single PDF file:

    python tools/visualizeEvent.py --projection XZ --file-path data/performance/DmlNtup_gamma_part124_MPhotonSelProj_TQ_SkimAllVar.root  --id 38 --verbose  --mc --legend --output gfx/ --suffix cnn.pdf 

Drawing 4 MC events (both projections) that contain at least one STK track, as well as the CNN predicted track, and the legend in a single PDF file:

    python tools/visualizeEvent.py --projection both --file-path data/performance/DmlNtup_gamma_part124_MPhotonSelProj_TQ_SkimAllVar.root  --must-contain-stk-track  --verbose  --mc ---output gfx/ --max-n-events 4  --cnn-file data/DmlNtup_gamma_part124_MPhotonSelProj_TQ_SkimAllVar_cnn_track.root  --n-workers 2 

Draw 2 MC events (both projections) in the energy range from 1 to 20 GeV, that contain at least one STK track, as well as the CNN predicted track, and the legend in a single PDF file:

    python tools/visualizeEvent.py --projection both --file-path data/parser/raw/DmlNtup_gamma_part000_MPhotonSelProj_TQ_SkimAllVar.root  --e-range 1-20 --max-n-events 2 --verbose --must-contain-stk-track --single-pdf  --legend
## CNN tool chain
If you only want to use pre-trained models refer to the subsection "Inference using trained models"

### Generation of training data
The [preprocess_data.py](preprocess_data.py) program reads the Root files and applies the preprocessing to generate the training data for the CNN (applying Hough transformation etc.). The training data is stored as a ```Tensorflow.data.Dataset``` object. 
The desired resolution with units $\mu\text{m}/\text{pixel}$ has to be specified using the ```--resolution``` option. 

The use of the ```--clean-data``` option is recommended for better results. It drops events with empty Hough images and events where the true track is not contained in the Hough image.

The script also allows to display the Hough images using the ```--display-id``` option.

For a complete overview over the command line options refer to the output of the ```--help``` option

A [SBATCH script](cluster/preprocess_data.sh) is provided. Please set the variables (PATH, MODELRESOLUTION, ...) accordingly. 
The batch scrip matches the files in the input directory using a regex and then sorts them by name. This list of files is stores in a temporary directory. Every job in the job array processes a single file, the job array id corresponds to the row that will be selected from the file list.

### Hyperparameter search
The hyperparameter search is performed using the [train.py](train.py) program using the ```--hp-tuning``` option. 
This program uses the [keras-tuner](https://keras-team.github.io/keras-tuner/) package to perform the hyperparameter search. 
 Refer to the output in the log file for the best hyperparameters.

 #### Specifying test and validation data

The ```--train-ratio``` option is used to specify the training data split. The ```-validation-ratio``` option is used to specify the validation data split. 
Note that if you want to achieve a $0.7$ training, $0.15$ validation and $0.15$ test split you will have to set ```--train-ratio 0.7``` and ```--validation-ratio 0.15```. The remaining data should be kept for the final model training with the fixed hyperparameters.


#### Tensorboard
 Besides the console output the program logs data in the folder specified by ```--log-dir```. This data includes the hyperparameter experiments, the test and validation losses, the learning rate and more. The data may be visualized using [Tensorboard](https://www.tensorflow.org/tensorboard)  (```tensorboard --logdir ./logs```). Some logging features do not work when training on GPU (logging some Hough images every few steps). 


### Model training
The [train.py](train.py) program is used to train a model on training data. You will have to specify the path to the respective training data, the split ratios for training-, validation and test-data, as well as the hyperparameters, found in the hyperparameter search, as command line options.

 It is not recommended using the ```--remove-outside-window```and the ```--remove-blank``` options for data cleaning. Consider doing this at the data generation stage ([preprocess_data.py](preprocess_data.py)) using the ```--clean-data``` option.

An example [SBATCH script](cluster/train_model.sh) is provided.

The training script will save a checkpoint (in the form of a .hdf5 file) after every epoch if the validation loss has decreased. 
The ```--model-weights``` option may be used to start the training process from a previous checkpoint.

Refer to the output of the ```--help``` option for a complete overview over the command line options.

#### Specifying test and validation data
There are two command line options used to specify the test and validation data split. In the context of training once the hyperparameters have been chosen.
The training data split is specified using the ```--train-ratio``` option. The test data split is specified using the ```--validation-ratio``` option. For the abovementioned example of a $0.7$ training, $0.15$ validation and $0.15$ test split you will have to set ```--train-ratio 0.85``` (test and validation data) and ```--validation-ratio 0.15``` (here referring to the test-data). 

#### Tensorboard

The training data may be accessed by opening the log data using [Tensorboard](https://www.tensorflow.org/tensorboard) (refer to the hyperparameter search section). To obtain the learning curve plots from the master project report, export the learning curve data from Tensorboard and then pass them to the [tools/visualizeTrainingHistory.py](tools/visualizeTrainingHistory.py) script.


### Inference using trained models
The command line interface allows for only for the use of either a single model or two models (sequential application). The underlying code is ready to handle more than two models.
The [inference.py](inference.py) program is used to perform inference on the  data using a trained model. 
Refer to the output of the ```--help``` option for a complete overview over the command line options.

Two example SBATCH scripts are provided: One for [applying a single model](cluster/infere_with_model.sh) and one for [applying two models sequentially](cluster/infere_with_multiple_models.sh).
The logic for the job arrays is the same as SBATCH script for the data preprocessing. 

The CNN track is stored in a Friend Tree which will be written to the output folder (with the suffix "_cnn_track.root"). The format is the same as for the other tracks (intersection point with the top and the bottom layer of the PSD). The CNN track is stored in the branches ```cnn_stk_track_x1```, ```cnn_stk_track_y1```, ```cnn_stk_track_x2``` and ```cnn_stk_track_y2```.


#### Trained models 

The models that were trained during my master project may be found in the [models/](models/) folder. 
