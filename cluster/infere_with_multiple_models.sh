#!/bin/bash
#SBATCH --job-name infere-800-050    # this is a parameter to help you sort your job when listing it
#SBATCH --cpus-per-task 8             # number of cpus for each task.
#SBATCH --partition shared-cpu
#SBATCH --time 00:30:00
#SBATCH --mem-per-cpu=4000 # in MB
#SBATCH --output=slurm-output/%x-%A/%a.out
#SBATCH --array=0-126%40

COARSE_RESOLUTION=800 #um/pixel
COARSE_ONLY=0 # If using only the coarse model, set to 1
FINE_RESOLUTION=50 #um/pixel

#Adapt the following lines to your needs
PATH_TO_EXE=$HOME/dampe-stk-ml/inference.py
TMP_SSD_PATH_ON_NODE=/tmp/$SLURM_JOB_ID/$SLURM_ARRAY_TASK_ID/
INPUT_DATA_PATH=/srv/beegfs/scratch/groups/dpnc/dampe/public/gamma/forParzival/new_MC_Skimmed/
REGEX_DATA_FILE="DmlNtup_gamma_part[0-9][0-9][0-9]_TQ_FullSel_MCSkim\.root"
OUTPUT_DIR=/home/users/n/nussbaup/dampe-stk-ml/cluster/fix_reference_bgo/inference_fixed/
LIST_OF_ALL_FILES="${TMP_SSD_PATH_ON_NODE}files_to_process.txt"
PATH_TO_CVMFS=/cvmfs/sft.cern.ch/lcg/views/LCG_104/x86_64-centos8-gcc11-opt/setup.sh
COARSE_MODEL="/home/users/n/nussbaup/dampe-stk-ml/cluster/fix_reference_bgo/inference_fixed/best_models/${COARSE_RESOLUTION}um/best.hdf5"
FINE_MODEL="/home/users/n/nussbaup/dampe-stk-ml/cluster/fix_reference_bgo/inference_fixed/best_models/${FINE_RESOLUTION}um/best.hdf5"
NUM_WORKERS=8
BATCH_SIZE=1024

#Load the modules needed to activate the cvmfs environment later on
module load GCCcore
module load Python
module load libreadline

#Exit on error
set -e

#Print the task id and other info
echo "I am task_id " ${SLURM_ARRAY_TASK_ID} " on node " $(hostname)" in directory " $(pwd)

#Create the cache directory on the SSD of the cluster
srun -J "Cache folder" zsh -c "mkdir -p ${TMP_SSD_PATH_ON_NODE}"

#Generate a list of all files to process
#They are located in the INPUT_DATA_PATH and match the REGEX_DATA_FILE using bash
srun -J "Preparing" zsh -c "\
	echo 'Starting with the creation of the file list'; \
	files=\$(ls ${INPUT_DATA_PATH} | grep -E '${REGEX_DATA_FILE}' || true); \
	if [[ -z \"\$files\" ]]; then \
    		echo 'Error: No files matched the regular expression' >&2; \
    		exit 1; \
	else \
    		echo \"\$files\" > ${LIST_OF_ALL_FILES}; \
	fi"
#Launch the inference script on the task_id-th file in the list
if [ $COARSE_ONLY -eq 1 ]
then
srun -J "Parsing" zsh -c "echo 'Launching inference script' \
	&& source \"${PATH_TO_CVMFS}\" \
	&& python -u \"${PATH_TO_EXE}\" --root-file \"${INPUT_DATA_PATH}\"\"$(sed -n $((SLURM_ARRAY_TASK_ID + 1))p ${LIST_OF_ALL_FILES})\" \
	 --coarse-model ${COARSE_MODEL} --coarse-resolution ${COARSE_RESOLUTION} --only-coarse-model --num-workers ${NUM_WORKERS} --batch-size ${BATCH_SIZE} \
	 --output-folder ${OUTPUT_DIR} "
else
srun -J "Parsing" zsh -c "echo 'Launching inference script' \
        && source \"${PATH_TO_CVMFS}\" \
        && python -u \"${PATH_TO_EXE}\" --root-file \"${INPUT_DATA_PATH}\"\"$(sed -n $((SLURM_ARRAY_TASK_ID + 1))p ${LIST_OF_ALL_FILES})\" \
         --coarse-model ${COARSE_MODEL} --coarse-resolution ${COARSE_RESOLUTION} --fine-model ${FINE_MODEL} --fine-resolution ${FINE_RESOLUTION} --num-workers ${NUM_WORKERS} --batch-size ${BATCH_SIZE} \
         --output-folder ${OUTPUT_DIR} "
fi
