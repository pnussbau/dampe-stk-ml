#!/bin/bash
#SBATCH --job-name=800um-2
#SBATCH --cpus-per-task=4
#SBATCH --gpus=1
#SBATCH --partition=public-gpu
#SBATCH --time=24:00:00
#SBATCH --mem-per-cpu=6000 # in MB
#SBATCH --output=slurm-output/%x-%j.out

PATH_TO_EXE=$HOME/dampe-stk-ml/train.py
INPUT_DATA_PATH=$HOME/scratch/800um_bgo/
PATH_TO_CVMFS=/cvmfs/sft.cern.ch/lcg/views/LCG_104cuda/x86_64-centos8-gcc11-opt/setup.sh
MODEL_WEIGHTS=/home/users/n/nussbaup/dampe-stk-ml/cluster/fix_reference_bgo/800um/checkpoint-06-8.36.hdf5
# Load the modules needed to activate the cvmfs environment
module load GCCcore
module load Python
module load libreadline

echo "I am on node " $(hostname)" in directory " $(pwd)

# Launch the training script
srun -J "Training" zsh -c "echo 'Launching the python script' \
	&& source \"${PATH_TO_CVMFS}\" \
	&& python -u \"${PATH_TO_EXE}\" --data-folder \"${INPUT_DATA_PATH}\" --epochs 50 --model  \"andrii-simplified\"   --train-ratio 0.85 --validation-ratio 0.14 --test-ratio 0.001 --batch-size 32  --optimizer adam --conv-kernel-initializer None --dense-kernel-initializer he_uniform --cnn-activation tanh --dense-activation leakyrelu  --fc-width 99  --num-fc-layers 5  --learning-rate 0.0001 --dropout 0.013 --compression GZIP --model-weights ${MODEL_WEIGHTS}"

