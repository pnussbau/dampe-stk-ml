import argparse 
import os 
import sys
import tensorflow as tf
import keras 
import numpy as np
import uproot
from preprocess_data import preprocess_data, get_bgo_intercept_and_slope, STK_STRIP_XZ_Z_POS, STK_STRIP_YZ_Z_POS
import multiprocessing
PSD_STRIP_CENTRE_XZ_Z_POS = [-298.5, -284.5] #mm
PSD_STRIP_CENTRE_YZ_Z_POS = [-324.7, -310.7] #mm


HOUGH_IMAGE_DIM = np.array([200,200])
DEBUG = False

class EventData:
    def __init__(self, bgo_energy, bgo_rec_intercept_xz, bgo_rec_slope_xz, bgo_rec_intercept_yz, bgo_rec_slope_yz, stk_cluster_x, stk_cluster_y, stk_cluster_z):
        self.bgo_energy = bgo_energy
        self.bgo_rec_intercept_xz = bgo_rec_intercept_xz
        self.bgo_rec_slope_xz = bgo_rec_slope_xz
        self.bgo_rec_intercept_yz = bgo_rec_intercept_yz
        self.bgo_rec_slope_yz = bgo_rec_slope_yz
        self.stk_cluster = np.array([stk_cluster_x, stk_cluster_y, stk_cluster_z], dtype=np.float32)
    def get_args_for_preprocessing(self):
        #Return a dictionary of arguments that can be passed to the preprocess_data function
        #bgo_rec_intercept_x, bgo_rec_intercept_y, bgo_rec_slope_x, bgo_rec_slope_y, stk_cluster_x, stk_cluster_y, stk_cluster_z,
        return {"bgo_rec_intercept_x": self.bgo_rec_intercept_xz, "bgo_rec_intercept_y": self.bgo_rec_intercept_yz, "bgo_rec_slope_x": self.bgo_rec_slope_xz, "bgo_rec_slope_y": self.bgo_rec_slope_yz, "stk_cluster_x": self.stk_cluster[0], "stk_cluster_y": self.stk_cluster[1], "stk_cluster_z": self.stk_cluster[2]}
def convert_image_coordinates_to_world_coordinates(image_coordinates, resolution, centre_image_position):
    image_distance_from_centre = (image_coordinates) * resolution
    world_coordinates = image_distance_from_centre + centre_image_position
    return world_coordinates
def preprocess_event_data(event_data, resolution):
        result = preprocess_data(**event_data.get_args_for_preprocessing(), roi_window=400*resolution/2, as_sparse_tensor=False, resolution=resolution)
        X = result[0]
        top_image_centre_coordinates = np.array([result[3], result[5]])
        bot_image_centre_coordinates = np.array([result[4], result[6]])
        return X, top_image_centre_coordinates, bot_image_centre_coordinates

def infer_track_using_model(model, resolution, data, num_workers):
    #Preprocess the data
    pool = multiprocessing.Pool(processes=num_workers)
    results = pool.starmap(preprocess_event_data, [(event_data, resolution) for event_data in data])
    pool.close()
    pool.join()

    hough_images, top_image_centre_coordinates, bot_image_centre_coordinates = zip(*results)
    hough_images = list(hough_images)
    top_image_centre_coordinates = list(top_image_centre_coordinates)
    bot_image_centre_coordinates = list(bot_image_centre_coordinates)
    hough_images = tf.convert_to_tensor(hough_images)

    #Infer the track
    predicted_distance_from_centre_pixels = model.predict(hough_images, verbose=2) # x_pixel_top, x_pixel_bot, y_pixel_top, y_pixel_bot

    if DEBUG:
        print("Plotting the hough images")
        #Make a plot with all the hough images
        import matplotlib.pyplot as plt
        fig, axs = plt.subplots(2, len(hough_images))
        print("Shape of hough images", hough_images.shape)
        for idx, hough_image in enumerate(hough_images):
            for i in range(2):
                axs[i][idx].imshow(hough_image[i].numpy(), cmap="jet", origin="lower")
                #Plot the predicted point
                axs[i][idx].scatter(predicted_distance_from_centre_pixels[idx][i*2+1]+200, predicted_distance_from_centre_pixels[idx][i*2]+200, color="red", marker="x")
                if resolution == 0.5:
                    #Draw a rectangle around the predicted point which is 80 x 80 pixels
                    axs[i][idx].add_patch(plt.Rectangle((predicted_distance_from_centre_pixels[idx][i*2+1]+200-40, predicted_distance_from_centre_pixels[idx][i*2]+200-40), 80, 80, fill=False, color="red"))

                #Get the number of non zero pixels
                num_non_zero_pixels = tf.math.count_nonzero(hough_image[i])
                axs[i][idx].set_title("I: {} Pro: {} ".format(idx, i))
                #Add a text box with the prediction_image_coordinates
                text = "x: {:.2f} y: {:.2f}".format(predicted_distance_from_centre_pixels[idx][i*2+1], predicted_distance_from_centre_pixels[idx][i*2])
                axs[i][idx].text(0.05, 0.95, text, transform=axs[i][idx].transAxes, fontsize=10, verticalalignment='top')
        #Display the resolution
        max_pixel_value = np.max(hough_images)
        plt.suptitle("Resolution: {} um/pixel, roi window {}, max pixel value {}".format(resolution*1000, 400*resolution/2, max_pixel_value))        
        plt.savefig("hough_images.png", dpi=800)

        #print the predicted distance from centre
        print("Predicted distance from centre", predicted_distance_from_centre_pixels)
        print("World coordinates of the top image centre", convert_image_coordinates_to_world_coordinates(predicted_distance_from_centre_pixels[:,0:2], resolution, top_image_centre_coordinates))
        print("World coordinates of the bot image centre", convert_image_coordinates_to_world_coordinates(predicted_distance_from_centre_pixels[:,2:4], resolution, bot_image_centre_coordinates))
        #input("Press enter to continue")
        
   
    predicted_distance_from_centre_pixels = np.squeeze(predicted_distance_from_centre_pixels) 
    #permute from x_top_pixel, x_bot_pixel, y_top_pixel, y_bot_pixel 
    #to x_top_pixel, y_top_pixel, x_bot_pixel, y_bot_pixel
    predicted_distance_from_centre_pixels = np.stack([predicted_distance_from_centre_pixels[:,0], predicted_distance_from_centre_pixels[:,2], predicted_distance_from_centre_pixels[:,1], predicted_distance_from_centre_pixels[:,3]], axis=1)
    
    top_world_coordinates = convert_image_coordinates_to_world_coordinates(predicted_distance_from_centre_pixels[:,0:2], resolution, top_image_centre_coordinates)
    bot_world_coordinates = convert_image_coordinates_to_world_coordinates(predicted_distance_from_centre_pixels[:,2:4], resolution, bot_image_centre_coordinates)

    return top_world_coordinates, bot_world_coordinates, top_image_centre_coordinates, bot_image_centre_coordinates




def infer_using_model_chain(models, resolutions, data, num_workers, t_x=None, t_y=None, t_z=None, t_px=None, t_py=None, t_pz=None):
    """
        This function takes a list of models and resolutions and data and returns the final prediction by iteratively using the models
        Args:
            models: A list of models
            resolutions: A list of resolutions
            data: The data to be used
    """
    X_TOP = STK_STRIP_XZ_Z_POS[-1]
    Y_TOP = STK_STRIP_YZ_Z_POS[-1]
    X_BOT = STK_STRIP_XZ_Z_POS[0]
    Y_BOT = STK_STRIP_YZ_Z_POS[0]
    #Sort the models and resolutions in descending order of resolution
    sorted_indices = np.argsort(resolutions)[::-1]
    resolutions = [resolutions[i] for i in sorted_indices]
    models = [models[i] for i in sorted_indices]
    print("Applying the following models {} with resolutions {} mm/pixel".format(models, resolutions))
    intial_array_top = {}
    coordinates_array_top = {}
    intial_array_bot = {}
    coordinates_array_bot = {}
    for model, resolution in zip(models, resolutions):
        #Inference
        coordinates_top, coordinates_bot, initial_top, initial_bot = infer_track_using_model(model, resolution, data, num_workers)
        #Update the data
        coordinates_array_top[resolution] = coordinates_top
        coordinates_array_bot[resolution] = coordinates_bot
        intial_array_top[resolution] = initial_top
        intial_array_bot[resolution] = initial_bot
        #Compute the updated "bgo_rec_intercept" and "bgo_rec_slope" using the new coordinates
        bgo_rec_intercepts_x, bgo_rec_slopes_x = get_bgo_intercept_and_slope(coordinates_top[:,0], X_TOP,  coordinates_bot[:,0], X_BOT, "xz")
        bgo_rec_intercepts_y, bgo_rec_slopes_y = get_bgo_intercept_and_slope(coordinates_top[:,1], Y_TOP,  coordinates_bot[:,1], Y_BOT, "yz")
        #Update the data
        for idx, event_data in enumerate(data):
            event_data.bgo_rec_intercept_xz = bgo_rec_intercepts_x[idx]
            event_data.bgo_rec_slope_xz = bgo_rec_slopes_x[idx]
            event_data.bgo_rec_intercept_yz = bgo_rec_intercepts_y[idx]
            event_data.bgo_rec_slope_yz = bgo_rec_slopes_y[idx]
    #Convert the final answe from x_top, y_top, x_bot, y_bot to the required format x_top, x_bot, y_top, y_bot
    data = np.zeros((len(data), 4), dtype=np.float32)
    for idx, (coordinate_top, coordinate_bot) in enumerate(zip(coordinates_top, coordinates_bot)):
        data[idx] = np.array([coordinate_top[0], coordinate_bot[0], coordinate_top[1], coordinate_bot[1]])

    if DEBUG:
        #Compute the intersection of the ling going through (t_x, t_y, t_z) and(t_x + t_px, t_y + t_py, t_z + t_pz) at z = X_TOP, Y_TOP, X_BOT, Y_BOT
        x_top = t_x + (X_TOP - t_z) * t_px / t_pz
        y_top = t_y + (Y_TOP - t_z) * t_py / t_pz
        x_bot = t_x + (X_BOT - t_z) * t_px / t_pz
        y_bot = t_y + (Y_BOT - t_z) * t_py / t_pz
        #Make a plot for both top and bot for each of the batch data to show how it evolved with model resolution
        import matplotlib.pyplot as plt
        fig, axs = plt.subplots(2,len(data))
        for idx, event_data in enumerate(data):
            for i in range(2):
                if i == 0:
                    axs[i][idx].set_title("Top")
                    axs[i][idx].scatter(x_top[idx], y_top[idx], color="black", marker="x", label="Truth")
                    axs[i][idx].scatter(intial_array_top[resolutions[0]][idx][0], intial_array_top[resolutions[0]][idx][1], color="red", marker="x", label="Initial {}".format(resolutions[0]))
                    axs[i][idx].scatter(coordinates_array_top[resolutions[0]][idx][0], coordinates_array_top[resolutions[0]][idx][1], color="blue", marker="x", label="Final {}".format(resolutions[0]))
                    axs[i][idx].scatter(intial_array_top[resolutions[1]][idx][0], intial_array_top[resolutions[1]][idx][1], color="orange", marker=".", label="Initial {}".format(resolutions[1]))
                    axs[i][idx].scatter(coordinates_array_top[resolutions[1]][idx][0], coordinates_array_top[resolutions[1]][idx][1], color="green", marker="^", label="Final {}".format(resolutions[1]))                  

                else:
                    axs[i][idx].set_title("Bot") 
                    axs[i][idx].scatter(x_bot[idx], y_bot[idx], color="black", marker="x", label="Truth")
                    axs[i][idx].scatter(intial_array_bot[resolutions[0]][idx][0], intial_array_bot[resolutions[0]][idx][1], color="red", marker="x", label="Initial {}".format(resolutions[0]))
                    axs[i][idx].scatter(coordinates_array_bot[resolutions[0]][idx][0], coordinates_array_bot[resolutions[0]][idx][1], color="blue", marker="x", label="Final {}".format(resolutions[0]))
                    axs[i][idx].scatter(intial_array_bot[resolutions[1]][idx][0], intial_array_bot[resolutions[1]][idx][1], color="orange", marker=".", label="Initial {}".format(resolutions[1]))
                    axs[i][idx].scatter(coordinates_array_bot[resolutions[1]][idx][0], coordinates_array_bot[resolutions[1]][idx][1], color="green", marker="^", label="Final {}".format(resolutions[1]))
                # axs[i][idx].set_xlim([-400, 400])
                # axs[i][idx].set_ylim([-400, 400])
        plt.legend()
        plt.tight_layout()
        plt.savefig("track_evolution.png", dpi=800)
        input("Press enter to continue")

    return data

    
def process_batch(start_idx, end_idx, bgo_total_energy, bgo_rec_intercept_x, bgo_rec_slope_x, bgo_rec_intercept_y, bgo_rec_slope_y, stk_cluster_x, stk_cluster_y, stk_cluster_z, models, resolutions, num_workers, t_x=None, t_y=None, t_z=None, t_px=None, t_py=None, t_pz=None):
    batch_data = []
    for idx in range(start_idx, end_idx):
        batch_data.append(EventData(bgo_energy=bgo_total_energy[idx], 
                                    bgo_rec_intercept_xz=bgo_rec_intercept_x[idx],
                                    bgo_rec_slope_xz=bgo_rec_slope_x[idx],
                                    bgo_rec_intercept_yz=bgo_rec_intercept_y[idx],
                                    bgo_rec_slope_yz=bgo_rec_slope_y[idx],
                                    stk_cluster_x=stk_cluster_x[idx],
                                    stk_cluster_y=stk_cluster_y[idx],
                                    stk_cluster_z=stk_cluster_z[idx]))
    if DEBUG: 
        res =  infer_using_model_chain(models, resolutions, batch_data, num_workers, t_x[start_idx:end_idx], t_y[start_idx:end_idx], t_z[start_idx:end_idx], t_px[start_idx:end_idx], t_py[start_idx:end_idx], t_pz[start_idx:end_idx])
    else:
        res =  infer_using_model_chain(models, resolutions, batch_data, num_workers)
    print("Finished batch with start_idx {} and end_idx {}".format(start_idx, end_idx))
    return res

def load_and_process_data_iteratively(root_file, models, resolutions, tree_name, batch_size, num_workers, output_folder):
    with uproot.open(root_file) as file:
        print("Processing root file {}".format(root_file))
        tree = file[tree_name]
        # Read the data in the tree in batches of INFERENCE_BATCH_SIZE
        bgo_total_energy = tree["tt_bgoTotalE_GeV"].array(library="np")
        bgo_rec_intercept_x = tree["tt_bgoRecInterceptX"].array(library="np")
        bgo_rec_intercept_y = tree["tt_bgoRecInterceptY"].array(library="np")
        bgo_rec_slope_x = tree["tt_bgoRecSlopeX"].array(library="np")
        bgo_rec_slope_y = tree["tt_bgoRecSlopeY"].array(library="np")
        stk_cluster_x = tree["StkHit_XPos"].array(library="np")
        stk_cluster_y = tree["StkHit_YPos"].array(library="np")
        stk_cluster_z = tree["StkHit_ZPos"].array(library="np")
        t_x = None
        t_y = None
        t_z = None
        t_px = None
        t_py = None
        t_pz = None

        if DEBUG:
            t_x =  tree["tt_mcprimary_XPos"].array(library="np")
            t_y =  tree["tt_mcprimary_YPos"].array(library="np")
            t_z =  tree["tt_mcprimary_ZPos"].array(library="np")
            t_px = tree["tt_mcprimary_XPart"].array(library="np")
            t_py = tree["tt_mcprimary_YPart"].array(library="np")
            t_pz = tree["tt_mcprimary_ZPart"].array(library="np")
        
        assert len(bgo_total_energy) == len(bgo_rec_intercept_x) == len(bgo_rec_intercept_y) == len(bgo_rec_slope_x) == len(bgo_rec_slope_y) == len(stk_cluster_x) == len(stk_cluster_y) == len(stk_cluster_z), "The number of elements in the branches are not the same"

        # Calculate the total number of batches
        if len(bgo_rec_intercept_x) % batch_size == 0:
            num_batches = len(bgo_rec_intercept_x) // (batch_size)
        else:
            num_batches = len(bgo_rec_intercept_x) // (batch_size) + 1

        results = np.zeros((len(bgo_rec_intercept_x), 4), dtype=np.float32)

        # Write the results to the tree
        for batch_idx in range(num_batches):
            start_idx = batch_idx * batch_size
            if batch_idx == num_batches - 1:
                end_idx = len(bgo_rec_intercept_x)
            else:
                end_idx = (batch_idx + 1) * batch_size
                
            results[start_idx:end_idx][:] = process_batch(start_idx, end_idx, bgo_total_energy, bgo_rec_intercept_x, bgo_rec_slope_x, bgo_rec_intercept_y, bgo_rec_slope_y, stk_cluster_x, stk_cluster_y, stk_cluster_z, models, resolutions, num_workers, t_x, t_y, t_z, t_px, t_py, t_pz)

    #Convert the results (coordinates at the top and bottom of the STK) to the coordinates at the top and bottom of the PSD (!! Depends on the projection !!)
    stk_strip_xz_z_pos_diff = STK_STRIP_XZ_Z_POS[0] - STK_STRIP_XZ_Z_POS[-1]
    stk_strip_yz_z_pos_diff = STK_STRIP_YZ_Z_POS[0] - STK_STRIP_YZ_Z_POS[-1]

    x_top = results[:, 0]
    x_bot = results[:, 1]
    y_top = results[:, 2]
    y_bot = results[:, 3]

    x1 = x_top + (PSD_STRIP_CENTRE_XZ_Z_POS[-1] - STK_STRIP_XZ_Z_POS[-1]) * (x_bot - x_top) / stk_strip_xz_z_pos_diff
    y1 = y_top + (PSD_STRIP_CENTRE_YZ_Z_POS[-1] - STK_STRIP_YZ_Z_POS[-1]) * (y_bot - y_top) / stk_strip_yz_z_pos_diff
    x2 = x_top + (PSD_STRIP_CENTRE_XZ_Z_POS[0] - STK_STRIP_XZ_Z_POS[-1]) * (x_bot - x_top) / stk_strip_xz_z_pos_diff
    y2 = y_top + (PSD_STRIP_CENTRE_YZ_Z_POS[0] - STK_STRIP_YZ_Z_POS[-1]) * (y_bot - y_top) / stk_strip_yz_z_pos_diff
        

    cnn_stk_track_x1 = x1
    cnn_stk_track_y1 = y1
    cnn_stk_track_x2 = x2
    cnn_stk_track_y2 = y2

    #Write the results to a new root file
    name_without_extension = os.path.splitext(os.path.basename(root_file))[0]
    if output_folder is None:
        path = os.path.dirname(root_file)
    else:
        path = output_folder
    new_root_file_name = name_without_extension + "_cnn_track.root"
    new_root_file_full_path = os.path.join(path, new_root_file_name)
    with uproot.recreate(new_root_file_full_path) as file:
        # Create a new tree
        file.mktree("cnn_track", {"cnn_stk_track_x1": np.float32, "cnn_stk_track_y1": np.float32, "cnn_stk_track_x2": np.float32, "cnn_stk_track_y2": np.float32, "cnn_x_top": np.float32, "cnn_y_top": np.float32, "cnn_x_bot": np.float32, "cnn_y_bot": np.float32})
        file["cnn_track"].extend({"cnn_stk_track_x1": cnn_stk_track_x1, "cnn_stk_track_y1": cnn_stk_track_y1, "cnn_stk_track_x2": cnn_stk_track_x2, "cnn_stk_track_y2": cnn_stk_track_y2, "cnn_x_top": x_top, "cnn_y_top": y_top, "cnn_x_bot": x_bot, "cnn_y_bot": y_bot})
    print("Wrote the results to {}".format(new_root_file_full_path))    

    
                                                                                           
def main():
    parser = argparse.ArgumentParser(description="Inference script")
    parser.add_argument("--coarse-model", type=str, required=True, help="Path to the coarse model")
    parser.add_argument("--coarse-resolution", type=float, required=True,  help="Resolution of the coarse model in um/pixel")
    parser.add_argument("--fine-model", type=str, required=False,  help="Path to the fine model")
    parser.add_argument("--fine-resolution", type=float, required=False, help="Resolution of the fine model in um/pixel")
    parser.add_argument("--root-file", type=str, required=True, help="Path to the root file")
    parser.add_argument("--tree-name", type=str, required=False, default="DmlNtup", help="Name of the tree")
    parser.add_argument("--batch-size", type=int, required=False, default=1024, help="Number elements that will be loaded from memory simultaneously")
    parser.add_argument("--num-workers", type=int, required=False, default=6, help="Number of workers to use for inference")
    parser.add_argument("--output-folder", type=str, required=False, default=None, help="Output folder for the root files. If not specified, the root files will be written to the same folder as the input root file")
    parser.add_argument("--only-coarse-model", action="store_true", help="If this flag is set, only the coarse model will be used for inference")
    args = parser.parse_args()

    # Check if all the files exist print ot stderr if they don't
    if not os.path.exists(args.coarse_model):
        print("Coarse model does not exist", file=sys.stderr)
        return 1
    if not args.only_coarse_model and not os.path.exists(args.fine_model):
        print("Fine model does not exist", file=sys.stderr)
        return 1
    if not os.path.exists(args.root_file):
        print("Root file does not exist", file=sys.stderr)
        return 1
    if not args.output_folder is None and not os.path.exists(args.output_folder):
        print("Output folder does not exist", file=sys.stderr)
        return 1
    if not args.only_coarse_model and args.fine_model is None:
        print("Fine model not specified", file=sys.stderr)
        return 1
    if not args.only_coarse_model and args.fine_resolution is None:
        print("Fine resolution not specified", file=sys.stderr)
        return 1

    

    print("Will be using the following root file:", args.root_file)

    # Load the models
    try:
        coarse_model = keras.models.load_model(args.coarse_model)
        if not args.only_coarse_model:
            fine_model = keras.models.load_model(args.fine_model)
    except Exception as e:
        print("Error loading the models", file=sys.stderr)
        print(e, file=sys.stderr)
        return 1
    #Convert from um/pixel to mm/pixel
    coarse_model_res = args.coarse_resolution/1000
    if not args.only_coarse_model:
        fine_model_res = args.fine_resolution/1000

    

    if not args.only_coarse_model:
        assert coarse_model_res >= fine_model_res, "Coarse model resolution should be greater than fine model resolution"
        if coarse_model_res == fine_model_res:
            print("WARNING: Coarse model resolution and fine model resolution are the same.")
    
    if args.only_coarse_model:
        print("Will be using only the coarse model")
        load_and_process_data_iteratively(args.root_file, [coarse_model], [coarse_model_res], args.tree_name, args.batch_size, args.num_workers, output_folder=args.output_folder)
    else:
        load_and_process_data_iteratively(args.root_file, [coarse_model, fine_model], [coarse_model_res, fine_model_res], args.tree_name, args.batch_size, args.num_workers, output_folder=args.output_folder)

    




if __name__ == "__main__":
    main()