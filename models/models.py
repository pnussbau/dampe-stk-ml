import enum
from tensorflow import keras
import tensorflow as tf

class ModelType(enum.Enum):
    SIMPLIFIED = "simplified"
    SIMPLIFIED_MAX_POOLING = "simplified-max"
    ANDRII_SIMPLIFIED = "andrii-simplified"

def get_cnn_stk_andrii_simplified(hyper_params):
    # Extract hyperparameters from hdparams
    learning_rate = hyper_params.get('lr')
    optimizer = hyper_params.get('optimizer')
    conv_kernel_initializer = hyper_params.get('conv_kernel_initializer')
    if conv_kernel_initializer == 'None':
        conv_kernel_initializer = None
    dense_kernel_initializer = hyper_params.get('dense_kernel_initializer')
    if dense_kernel_initializer == 'None':
        dense_kernel_initializer = None
    cnn_activation = hyper_params.get('cnn_activation')
    use_leaky_relu_cnn = False
    if cnn_activation.lower() == 'leakyrelu':
        use_leaky_relu_cnn = True
        cnn_activation = None
    dense_activation = hyper_params.get('dense_activation')
    use_leaky_relu_dense = False
    if dense_activation.lower() == 'leakyrelu':
        use_leaky_relu_dense = True
        dense_activation = None
    num_fc_layers = hyper_params.get('num_fc_layers')
    fc_width = hyper_params.get('fc_width')
    dropout = hyper_params.get('dropout')

    #Print all the hyperparameters
    print("Hyperparameters:")
    print("learning_rate: {}".format(learning_rate))
    print("optimizer: {}".format(optimizer))
    print("conv_kernel_initializer: {}".format(conv_kernel_initializer))
    print("dense_kernel_initializer: {}".format(dense_kernel_initializer))
    print("cnn_activation: {}".format(cnn_activation))
    print("use_leaky_relu_cnn: {}".format(use_leaky_relu_cnn))
    print("dense_activation: {}".format(dense_activation))
    print("use_leaky_relu_dense: {}".format(use_leaky_relu_dense))
    print("num_fc_layers: {}".format(num_fc_layers))
    print("fc_width: {}".format(fc_width))
    print("dropout: {}".format(dropout))
    

    model = keras.Sequential(name="stk_cnn_model_simple")
    model.add(keras.layers.Permute((2, 3, 1), input_shape=(2, 400, 400), dtype=tf.float16))
    # Image rediction
    # 400 * 400 * 2 
    model.add(keras.layers.Conv2D(filters=32, kernel_size=(2, 2), strides=(2, 2), activation=cnn_activation, kernel_initializer=conv_kernel_initializer))
    if use_leaky_relu_cnn:
        model.add(keras.layers.LeakyReLU())

    model.add(keras.layers.Conv2D(filters=16, kernel_size=(2, 2), strides=(2, 2), activation=cnn_activation, kernel_initializer=conv_kernel_initializer))
    if use_leaky_relu_cnn:
        model.add(keras.layers.LeakyReLU())

    model.add(keras.layers.Conv2D(filters=8, kernel_size=(2, 2), strides=(2, 2), activation=cnn_activation, kernel_initializer=conv_kernel_initializer))
    if use_leaky_relu_cnn:
        model.add(keras.layers.LeakyReLU())
    
    model.add(keras.layers.Conv2D(filters=8, kernel_size=(2, 2), strides=(2, 2), activation=cnn_activation, kernel_initializer=conv_kernel_initializer))
    if use_leaky_relu_cnn:
        model.add(keras.layers.LeakyReLU())

    
    #Convolutional layers
    model.add(keras.layers.Conv2D(filters=16, kernel_size=(4, 4), strides=(1, 1), activation=cnn_activation, kernel_initializer=conv_kernel_initializer))
    if use_leaky_relu_cnn:
        model.add(keras.layers.LeakyReLU())
    if dropout > 0:
        model.add(keras.layers.Dropout(dropout))
    model.add(keras.layers.Conv2D(filters=8, kernel_size=(4, 4), strides=(1, 1), activation=cnn_activation, kernel_initializer=conv_kernel_initializer))
    if use_leaky_relu_cnn:
        model.add(keras.layers.LeakyReLU())
    if dropout > 0:
        model.add(keras.layers.Dropout(dropout))

    # Fully connected layers
    model.add(keras.layers.Flatten())
    for _ in range(num_fc_layers):
        model.add(keras.layers.Dense(fc_width, activation=dense_activation, kernel_initializer=dense_kernel_initializer))
        if use_leaky_relu_dense:
            model.add(keras.layers.LeakyReLU())
        if dropout > 0:
            model.add(keras.layers.Dropout(dropout))
    model.add(keras.layers.Dense(4, activation="linear", kernel_initializer=dense_kernel_initializer))

    if optimizer == 'adam':
        opti = keras.optimizers.Adam(learning_rate=learning_rate)
    elif optimizer == 'sgd':
        opti = keras.optimizers.SGD(learning_rate=learning_rate)
    elif optimizer == 'rmsprop':
        opti = keras.optimizers.RMSprop(learning_rate=learning_rate)
    else:
        raise ValueError("Invalid optimizer: {}".format(optimizer))

    model.compile(loss='mean_absolute_error', optimizer=opti, metrics=['mean_absolute_error', 'mean_squared_error'])
    return model  

#Max Pooling
def get_cnn_stk_simplified_max_pooling(hyper_params):
    # Extract hyperparameters from hdparams
    learning_rate = hyper_params.get('lr')
    optimizer = hyper_params.get('optimizer')
    conv_kernel_initializer = hyper_params.get('conv_kernel_initializer')
    if conv_kernel_initializer == 'None':
        conv_kernel_initializer = None
    dense_kernel_initializer = hyper_params.get('dense_kernel_initializer')
    if dense_kernel_initializer == 'None':
        dense_kernel_initializer = None
    cnn_activation = hyper_params.get('cnn_activation')
    use_leaky_relu_cnn = False
    if cnn_activation.lower() == 'leakyrelu':
        use_leaky_relu_cnn = True
        cnn_activation = None
    dense_activation = hyper_params.get('dense_activation')
    use_leaky_relu_dense = False
    if dense_activation.lower() == 'leakyrelu':
        use_leaky_relu_dense = True
        dense_activation = None
    num_fc_layers = hyper_params.get('num_fc_layers')
    fc_width = hyper_params.get('fc_width')
    dropout = hyper_params.get('dropout')

    model = keras.Sequential(name="stk_cnn_model_simple")
    model.add(keras.layers.Permute((2, 3, 1), input_shape=(2, 400, 400), dtype=tf.float16))
    # Image rediction
    # 400 * 400 * 2 
    model.add(keras.layers.Conv2D(filters=8, kernel_size=(5, 5), strides=(1, 1), groups=2, activation=None, kernel_initializer=conv_kernel_initializer))
    # 396 * 396 * 2 # 4
    model.add(keras.layers.MaxPool2D(pool_size=(5, 5), strides=(3, 3)))

    # 131 * 131  * 2 # 4
    model.add(keras.layers.Conv2D(filters=12, kernel_size=(5, 5), strides=(1, 1), groups=2, activation=None, kernel_initializer=conv_kernel_initializer))

    # 127 * 127 * 2 # 6
    model.add(keras.layers.MaxPool2D(pool_size=(5, 5), strides=(5, 5)))
    # 41 * 41 * 2 # 6

    if dropout > 0:
        model.add(keras.layers.Dropout(dropout))

    model.add(keras.layers.Conv2D(filters=6, kernel_size=(5, 5), strides=(1, 1), activation=cnn_activation, kernel_initializer=conv_kernel_initializer))
    if use_leaky_relu_cnn:
        model.add(keras.layers.LeakyReLU())

    if dropout > 0:
        model.add(keras.layers.Dropout(dropout))
    # 37 * 37 * 6
    model.add(keras.layers.Conv2D(filters=6, kernel_size=(7, 7), strides=(3, 3), activation=cnn_activation, kernel_initializer=conv_kernel_initializer))
    if use_leaky_relu_cnn:
        model.add(keras.layers.LeakyReLU())
    if dropout > 0:
        model.add(keras.layers.Dropout(dropout))
    # 11 * 11 * 6

    # Fully connected layers
    model.add(keras.layers.Flatten())
    for _ in range(num_fc_layers):
        model.add(keras.layers.Dense(fc_width, activation=dense_activation, kernel_initializer=dense_kernel_initializer))
        if use_leaky_relu_dense:
            model.add(keras.layers.LeakyReLU())
        if dropout > 0:
            model.add(keras.layers.Dropout(dropout))
    model.add(keras.layers.Dense(4, activation="linear", kernel_initializer=dense_kernel_initializer))

    if optimizer == 'adam':
        opti = keras.optimizers.Adam(learning_rate=learning_rate)
    elif optimizer == 'sgd':
        opti = keras.optimizers.SGD(learning_rate=learning_rate)
    elif optimizer == 'rmsprop':
        opti = keras.optimizers.RMSprop(learning_rate=learning_rate)
    else:
        raise ValueError("Invalid optimizer: {}".format(optimizer))

    model.compile(loss='mean_absolute_error', optimizer=opti, metrics=['mean_absolute_error', 'mean_squared_error'])
    return model



def get_cnn_stk_simplified(hyper_params):
    # Extract hyperparameters from hdparams
    learning_rate = hyper_params.get('lr')
    optimizer = hyper_params.get('optimizer')
    conv_kernel_initializer = hyper_params.get('conv_kernel_initializer')
    if conv_kernel_initializer == 'None':
        conv_kernel_initializer = None
    dense_kernel_initializer = hyper_params.get('dense_kernel_initializer')
    if dense_kernel_initializer == 'None':
        dense_kernel_initializer = None
    cnn_activation = hyper_params.get('cnn_activation')
    use_leaky_relu_cnn = False
    if cnn_activation.lower() == 'leakyrelu':
        use_leaky_relu_cnn = True
        cnn_activation = None
    dense_activation = hyper_params.get('dense_activation')
    use_leaky_relu_dense = False
    if dense_activation.lower() == 'leakyrelu':
        use_leaky_relu_dense = True
        dense_activation = None
    num_fc_layers = hyper_params.get('num_fc_layers')
    fc_width = hyper_params.get('fc_width')
    dropout = hyper_params.get('dropout')

    model = keras.Sequential(name="stk_cnn_model_simple")
    model.add(keras.layers.Permute((2, 3, 1), input_shape=(2, 400, 400), dtype=tf.float16))
    # Image rediction
    # 400 * 400 * 2 
    model.add(keras.layers.Conv2D(filters=8, kernel_size=(5, 5), strides=(2, 2), groups=2, activation=cnn_activation, kernel_initializer=conv_kernel_initializer))
    if use_leaky_relu_cnn:
        model.add(keras.layers.LeakyReLU())
    # 198 * 198 * 2 # 4
    model.add(keras.layers.Conv2D(filters=12, kernel_size=(5, 5), strides=(2, 2), groups=2, activation=cnn_activation, kernel_initializer=conv_kernel_initializer))
    if use_leaky_relu_cnn:
        model.add(keras.layers.LeakyReLU())

    if dropout > 0:
        model.add(keras.layers.Dropout(dropout))
    # 97 * 97 * 2 # 6
    model.add(keras.layers.Conv2D(filters=6, kernel_size=(5, 5), strides=(2, 2), activation=cnn_activation, kernel_initializer=conv_kernel_initializer))
    if use_leaky_relu_cnn:
        model.add(keras.layers.LeakyReLU())

    if dropout > 0:
        model.add(keras.layers.Dropout(dropout))
    # 47 * 47 * 6
    model.add(keras.layers.Conv2D(filters=6, kernel_size=(7, 7), strides=(3, 3), activation=cnn_activation, kernel_initializer=conv_kernel_initializer))
    if use_leaky_relu_cnn:
        model.add(keras.layers.LeakyReLU())
    if dropout > 0:
        model.add(keras.layers.Dropout(dropout))
    # 14 * 14 * 6
    # Fully connected layers
    model.add(keras.layers.Flatten())
    for _ in range(num_fc_layers):
        model.add(keras.layers.Dense(fc_width, activation=dense_activation, kernel_initializer=dense_kernel_initializer))
        if use_leaky_relu_dense:
            model.add(keras.layers.LeakyReLU())
        if dropout > 0:
            model.add(keras.layers.Dropout(dropout))
    model.add(keras.layers.Dense(4, activation="linear", kernel_initializer=dense_kernel_initializer))

    if optimizer == 'adam':
        opti = keras.optimizers.Adam(learning_rate=learning_rate)
    elif optimizer == 'sgd':
        opti = keras.optimizers.SGD(learning_rate=learning_rate)
    elif optimizer == 'rmsprop':
        opti = keras.optimizers.RMSprop(learning_rate=learning_rate)
    else:
        raise ValueError("Invalid optimizer: {}".format(optimizer))

    model.compile(loss='mean_absolute_error', optimizer=opti, metrics=['mean_absolute_error', 'mean_squared_error'])
    return model
