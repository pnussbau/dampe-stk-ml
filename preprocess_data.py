# DATASET SUFFIX
DATASET_SUFFIX = "_dataset"

#STK
STK_STRIP_XZ_Z_POS = [-209.58199763, -176.78199706, -143.98199706, -111.1819942,   -79.18199573, -47.18195758] #mm
STK_STRIP_YZ_Z_POS = [-206.41799808, -173.61799665, -140.81799712, -108.01799655,  -76.01799274, -44.0179584 ] #mm
HOUGH_IMAGE_SIZE = [-400,400] #mm

#BGO
BGO_BAR_VERTICAL_SPACING = 4 #mm
BGO_BAR_WIDTH = 25 #mm
BGO_BOTTOM_MOST_VERTICAL_POS_Y = 58.5 #mm
BGO_BOTTOM_MOST_VERTICAL_POS_X = 58.5 + BGO_BAR_WIDTH + BGO_BAR_VERTICAL_SPACING#mm
BGO_TRACK_INTERCEPT_REFERENCE_XZ = 0
BGO_TRACK_INTERCEPT_REFERENCE_YZ = 0

#MC Truth
MC_TRACK_REFERENCE_XZ = 0
MC_TRACK_REFERENCE_YZ = 0


MODEL_X_SHAPE = (2,400,400)
MODEL_LABEL_SHAPE = (4)

import uproot
import numpy as np
import matplotlib
matplotlib.use('Agg') # Save figures to disk instead of displaying them
import matplotlib.pyplot as plt
import argparse
import os
import sys
import tensorflow as tf

def get_roi_centre(bgo_rec_intercept, bgo_rec_slope, z, projection):
    #Extrapolate the BGO track to the top of the STK
    if projection == "xz":
        horizontal_pos = bgo_rec_intercept + bgo_rec_slope*(z-BGO_TRACK_INTERCEPT_REFERENCE_XZ)
    else:
        horizontal_pos = bgo_rec_intercept + bgo_rec_slope*(z-BGO_TRACK_INTERCEPT_REFERENCE_YZ)
    return horizontal_pos
def get_bgo_intercept_and_slope(top, z_top, bot ,z_bot, projection):
    #Compute the slope and intercept of the BGO track
    if projection == "xz":
        bgo_ref = BGO_TRACK_INTERCEPT_REFERENCE_XZ
    elif projection == "yz":
        bgo_ref = BGO_TRACK_INTERCEPT_REFERENCE_YZ
    else:
        print("Invalid projection: ", projection, file=sys.stderr)
        raise ValueError("Invalid projection: ", projection)
    slope = (top-bot)/(z_top-z_bot)
    intercept = top - slope*(z_top-bgo_ref)
    return intercept, slope

def get_roi_hits(stk_cluster_z, stk_cluster_horizontal, bgo_rec_intercept, bgo_rec_slope, projection, roi_window):
    """
    Returns the hits within ROI the way Andrii does it (select the pixel within the ROI in a given layer
    Args:
        stk_cluster_z (numpy.ndarray): A numpy array containing the z coordinates of the STK hits
        stk_cluster_horizontal (numpy.ndarray): A numpy array containing the horizontal coordinates of the STK hits
    Returns:
        roi_hits (list): A list of tuples containing the z coordinate and the horizontal coordinate of the hits within the ROI
    """
    assert len(stk_cluster_horizontal) == len(stk_cluster_z)
    # Mask that hits outside the ROI
    if len(stk_cluster_horizontal)>0:
        # print("Lenght of horizontal", len(stk_cluster_horizontal))
        # print("Length of z", len(stk_cluster_z))
        # print("Length of abs", len (np.abs(stk_cluster_horizontal)))
        indices_in_stk_range = np.abs(stk_cluster_horizontal) < HOUGH_IMAGE_SIZE[1]
        stk_cluster_horizontal = stk_cluster_horizontal[indices_in_stk_range]
        stk_cluster_z = stk_cluster_z[indices_in_stk_range]

    #Check that there as as many unique z values as there are strip
    assert len(np.unique(np.round(stk_cluster_z,3))) <= len(STK_STRIP_XZ_Z_POS)    
    #Sort the hits by the z coordinate
    sorted_indices = np.argsort(stk_cluster_z)
    stk_cluster_z = stk_cluster_z[sorted_indices]
    stk_cluster_horizontal = stk_cluster_horizontal[sorted_indices]
    
    #Check that all the z values are in the list of z values of the strips either in XZ or YZ
    #assert len(stk_cluster_z) == 0 or (np.all(np.isin(np.round(stk_cluster_z,2), np.round(STK_STRIP_XZ_Z_POS,2))) != np.all(np.isin(np.round(stk_cluster_z,2), np.round(STK_STRIP_YZ_Z_POS,2))))
    
    # Find the unique z values in stk_cluster_z
    unique_z_values = np.unique(stk_cluster_z)

    # No hits in the STK
    if len(unique_z_values) == 0:
        return np.array([]), np.array([])

    # Find the roi centre for each z value
    roi_centres = get_roi_centre(bgo_rec_intercept, bgo_rec_slope, unique_z_values, projection)

    # Find the hits within the ROI for each z value
    
    roi_hits = (np.repeat(unique_z_values, [np.sum(np.abs(stk_cluster_horizontal[z == stk_cluster_z] - roi) < roi_window) for z, roi in zip(unique_z_values, roi_centres)]),
            np.concatenate([stk_cluster_horizontal[z == stk_cluster_z][np.abs(stk_cluster_horizontal[z == stk_cluster_z] - roi) < roi_window] for z, roi in zip(unique_z_values, roi_centres)]))
    
    return roi_hits

def compute_hough_transform_andrii_style(roi_hits_z, roi_hits_horizontal, top_z, roi_top, bot_z, roi_bot, size, resolution, normalize=False):
    #Print the types of the input
    
    hough_transform_result = np.zeros((size, size))
    probe_angles = np.linspace(-np.pi/3, np.pi/3, 3600, endpoint=False)

    # Calculate the top and bottom pixels for all hits in a vectorized manner
    tops = roi_hits_horizontal.reshape((-1, 1)) - np.outer(np.transpose(roi_hits_z) - top_z, np.tan(probe_angles))
    bots = roi_hits_horizontal.reshape((-1, 1)) - np.outer(np.transpose(roi_hits_z) - bot_z, np.tan(probe_angles))
    # Calculate the pixel coordinates for the top and bottom hits
    top_pixels = ((tops - roi_top) / resolution + size / 2).astype(int)
    bot_pixels = ((bots - roi_bot) / resolution + size / 2).astype(int)

    # Filter out hits outside the image boundaries
    valid_indices = np.logical_and.reduce((top_pixels >= 0, top_pixels < size, bot_pixels >= 0, bot_pixels < size))
    top_pixels = top_pixels[valid_indices]
    bot_pixels = bot_pixels[valid_indices]

    # Count the hits for each pixel using numpy's bincount function
    pixel_counts = np.bincount(top_pixels * size + bot_pixels, minlength=size*size)
    hough_transform_result.flat[:len(pixel_counts)] = pixel_counts


    # Normalize the image to range 0-1
    if normalize:
       hough_transform_result /= np.max(hough_transform_result)
    else:
        #Cap the pixel values at 1
        hough_transform_result[hough_transform_result>1] = 1

    return hough_transform_result



def check_strip_range(t_x_top, t_x_bottom, t_y_top, t_y_bottom):
    if t_x_top < HOUGH_IMAGE_SIZE[0] or t_x_top > HOUGH_IMAGE_SIZE[1] or t_x_bottom < HOUGH_IMAGE_SIZE[0] or t_x_bottom > HOUGH_IMAGE_SIZE[1]:
        return False
    if t_y_top < HOUGH_IMAGE_SIZE[0] or t_y_top > HOUGH_IMAGE_SIZE[1] or t_y_bottom < HOUGH_IMAGE_SIZE[0] or t_y_bottom > HOUGH_IMAGE_SIZE[1]:
        return False
    return True

def check_photon_conversion(stk_cluster_x, stk_cluster_y, stk_cluster_z, t_x_top, t_x_bot, t_y_top, t_y_bot):
    #Check how many clusters there are in a certain window around the true track to decide whether the photon converted or not
    #The window is defined around the true track intersection boints with the top and bottom of the STK
    #The window is 5mm wide
    window = 5 #mm
    threshold = 0.7
    #Check how many clusters there are in a certain window around the true track to decide whether the photon converted or not
    STK_XZ_TOP = STK_STRIP_XZ_Z_POS[-1]
    STK_XZ_BOT = STK_STRIP_XZ_Z_POS[0]
    STK_YZ_TOP = STK_STRIP_YZ_Z_POS[-1]
    STK_YZ_BOT = STK_STRIP_YZ_Z_POS[0]

    # Check whether 70% of the hits are in the window 
    # Compute the distance of each hit to the true track
    distance_x = np.abs(stk_cluster_x - (stk_cluster_z - STK_XZ_TOP)/(STK_XZ_BOT-STK_XZ_TOP)*(t_x_bot-t_x_top) + t_x_top)
    distance_y = np.abs(stk_cluster_y - (stk_cluster_z - STK_YZ_TOP)/(STK_YZ_BOT-STK_YZ_TOP)*(t_y_bot-t_y_top) + t_y_top)
    # Check how many hits are within the window
    hits_in_window_x = np.sum(distance_x < window)
    hits_in_window_y = np.sum(distance_y < window)
  
    if hits_in_window_x/len(stk_cluster_x) < threshold or hits_in_window_y/len(stk_cluster_y) < threshold:
        return False
    return True

def preprocess_data(bgo_rec_intercept_x, bgo_rec_intercept_y, bgo_rec_slope_x, bgo_rec_slope_y, stk_cluster_x, stk_cluster_y, stk_cluster_z, as_sparse_tensor, resolution, roi_window):
    # Preprocessing the input data
    # Define the ROI (Region of interest)
    # Find the coordinates of the BGO prediction at the top of the STK in both projections
    roi_centre_top_x = get_roi_centre(bgo_rec_intercept_x, bgo_rec_slope_x, STK_STRIP_XZ_Z_POS[-1], "xz")
    roi_centre_top_y = get_roi_centre(bgo_rec_intercept_y, bgo_rec_slope_y, STK_STRIP_YZ_Z_POS[-1], "yz")
    roi_centre_bot_x = get_roi_centre(bgo_rec_intercept_x, bgo_rec_slope_x, STK_STRIP_XZ_Z_POS[0], "xz")
    roi_centre_bot_y = get_roi_centre(bgo_rec_intercept_y, bgo_rec_slope_y, STK_STRIP_YZ_Z_POS[0], "yz")

    #Check that they have the same length
    assert len(stk_cluster_x) == len(stk_cluster_y) == len(stk_cluster_z)
    stk_roi_z_x, stk_roi_x = get_roi_hits(stk_cluster_z, stk_cluster_x, bgo_rec_intercept_x, bgo_rec_slope_x, "xz", roi_window=roi_window)
    stk_roi_z_y, stk_roi_y = get_roi_hits(stk_cluster_z, stk_cluster_y, bgo_rec_intercept_y, bgo_rec_slope_y, "yz", roi_window=roi_window)           
    
    assert MODEL_X_SHAPE[1] == MODEL_X_SHAPE[2]
    hough_x_raster = compute_hough_transform_andrii_style(stk_roi_z_x, stk_roi_x, STK_STRIP_XZ_Z_POS[-1], roi_centre_top_x, STK_STRIP_XZ_Z_POS[0], roi_centre_bot_x, size=np.max(MODEL_X_SHAPE), resolution=resolution)
    hough_y_raster = compute_hough_transform_andrii_style(stk_roi_z_y, stk_roi_y, STK_STRIP_YZ_Z_POS[-1], roi_centre_top_y, STK_STRIP_YZ_Z_POS[0], roi_centre_bot_y, size=np.max(MODEL_X_SHAPE), resolution=resolution)

    if as_sparse_tensor:
        def create_sparse_tensor_from_numpy_array(numpy_array):
            #Add one axis for the channels
            numpy_array = np.expand_dims(numpy_array, axis=0)
            non_zero_indices = np.nonzero(numpy_array)
            non_zero_values = numpy_array[non_zero_indices]
            dense_tensor = tf.convert_to_tensor(non_zero_values, dtype=tf.float16)
            return tf.sparse.SparseTensor(indices=np.transpose(non_zero_indices), values=dense_tensor, dense_shape=numpy_array.shape)
        
        #Store the data as sparse tensor with data type tf.float16
        X_xz = create_sparse_tensor_from_numpy_array(hough_x_raster)
        X_yz = create_sparse_tensor_from_numpy_array(hough_y_raster)
        X = tf.sparse.concat(axis=0, sp_inputs=[X_xz, X_yz])
    else:
        #Store the data as dense tensor with data type tf.float16
        X = tf.convert_to_tensor([hough_x_raster, hough_y_raster], dtype=tf.float16)

    return X, hough_x_raster, hough_y_raster, roi_centre_top_x, roi_centre_bot_x, roi_centre_top_y, roi_centre_bot_y

def preprocess_label(t_XPos, t_YPos, t_ZPos, t_XPart, t_YPart, t_ZPart, roi_centre_top_x, roi_centre_bot_x, roi_centre_top_y, roi_centre_bot_y, resolution):
    #Preprocessing the labels
    def compute_mc_truth_in_roi(pos, slope, z, projection):
        #Extrapolate the BGO track to the top of the STK
        if projection == "xz":
            horizontal_pos_top = pos + slope*(STK_STRIP_XZ_Z_POS[-1]-(z+MC_TRACK_REFERENCE_XZ))
            horizontal_pos_bot = pos + slope*(STK_STRIP_XZ_Z_POS[0]-(z+MC_TRACK_REFERENCE_XZ))
        elif projection == "yz":
            horizontal_pos_top = pos + slope*(STK_STRIP_YZ_Z_POS[-1]-(z+MC_TRACK_REFERENCE_YZ))
            horizontal_pos_bot = pos + slope*(STK_STRIP_YZ_Z_POS[0]-(z+MC_TRACK_REFERENCE_YZ))
        else:
            raise ValueError("Invalid projection: ", projection)
        return horizontal_pos_top, horizontal_pos_bot            
    
    #Compute slope with respect to the X and Y axis
    t_slope_x = (t_XPart)/(t_ZPart)
    t_slope_y = (t_YPart)/(t_ZPart)
    t_x_top, t_x_bot = compute_mc_truth_in_roi(t_XPos, t_slope_x, t_ZPos, "xz")
    t_y_top, t_y_bot = compute_mc_truth_in_roi(t_YPos, t_slope_y, t_ZPos, "yz")

    #Compute the corresponding pixel in the hough transform
    image_width = np.max(MODEL_X_SHAPE)
    t_pixel_x_bot = np.round((t_x_bot-roi_centre_bot_x)/resolution)
    t_pixel_x_top = np.round((t_x_top-roi_centre_top_x)/resolution)
    t_pixel_y_bot = np.round((t_y_bot-roi_centre_bot_y)/resolution)
    t_pixel_y_top = np.round((t_y_top-roi_centre_top_y)/resolution)

    label = tf.convert_to_tensor([t_pixel_x_top, t_pixel_x_bot, t_pixel_y_top, t_pixel_y_bot], dtype=tf.float16)

    return label, t_x_top, t_x_bot, t_y_top, t_y_bot

def plot_hough_transform(hough_x_raster, hough_y_raster, t_pixel_x_top, t_pixel_x_bot, t_pixel_y_top, t_pixel_y_bot, resolution, roi_window, fig_out_path):
    #Display the hough transform        
    for plane, hough_raster in zip(["x","y"],[hough_x_raster, hough_y_raster]):
        #Only show pixels with more than 0 hit
        plt.figure()
        plt.title("Hough transform")
        plt.imshow(hough_raster, origin="lower", cmap="Blues", vmin=0, vmax=1)
        plt.colorbar()
        plt.ylabel("Distance")
        
        image_width = np.max(MODEL_X_SHAPE)
        plt.xlabel("$\\delta {}_{{bot}}$".format(plane))
        plt.ylabel("$\\delta {}_{{top}}$".format(plane))
        #Draw a hollow (non filled) circle in the centre of the image to indicate the BGO prediction
        plt.scatter(image_width/2, image_width/2, marker="o", label="BGO prediction", facecolor="none", edgecolors="blue", s=15)
        #Draw the MC truth with a red circle
        if plane == "x":
            plt.scatter(t_pixel_x_bot+image_width/2, t_pixel_x_top+image_width/2, marker="o", label="MC truth", facecolor="none", edgecolors="red", s=15)
        else:
            plt.scatter(t_pixel_y_bot+image_width/2, t_pixel_y_top+image_width/2, marker="o", label="MC truth", facecolor="none", edgecolors="red", s=15)
        #Change the y and x ticks so that 0 is -200 and 400 is 200
        plt.xticks(np.linspace(0,image_width,5), np.linspace(-image_width/2,image_width/2,5))
        plt.yticks(np.linspace(0,image_width,5), np.linspace(-image_width/2,image_width/2,5))
        text = "Resolution: {} mm/pixel \nROI selection window: {} mm".format(resolution, roi_window)
        plt.text(0.5, 1.1, text, horizontalalignment='center', verticalalignment='center', transform=plt.gca().transAxes)
        plt.legend()
        #Try to format the output path (inserting the plane)
        try:
            fig_out_path_formatted = fig_out_path.format(plane)
        except:
            #Extract the suffix from the path
            suffix = fig_out_path.split(".")[-1]
            fig_out_path_formatted = fig_out_path[:-len(suffix)]
            fig_out_path_formatted += plane
            fig_out_path_formatted += suffix
        print("Saving to: " + fig_out_path_formatted)
        plt.savefig(fig_out_path_formatted, dpi=300)
        plt.close()
def main():
    parser = argparse.ArgumentParser(description="Preprocesses the root files for the training of the STK track reconstruction")
    parser.add_argument("--file", required=True, type=str, help="Path to the root file")
    parser.add_argument("--tree", default="DmlNtup", type=str, help="Name of the tree in the root file")
    parser.add_argument("--display-id", default=-1, type=int, help="ID of the event to create and save processed image")
    parser.add_argument("--display-only", default=False, action="store_true", help="Only display the image and do not save it, do not generate a dataset")
    parser.add_argument("--output-dir", default="./", type=str, help="Directory (prefix to the file name) to save the output files")
    parser.add_argument("--debug", default=False,  action="store_true", help="Print debugging information")
    parser.add_argument("--compression", default="GZIP", type=str, choices=["NONE", "GZIP"], help="Compression algorithm to use")
    parser.add_argument("--overwrite", default=False, action="store_true", help="Overwrite the output file if it exists")
    parser.add_argument("--resolution", type=float, required=True, help="Resolution of the hough transform in mm/pixel")
    parser.add_argument("--clean-data", default=False, action="store_true", help="Remove events having in projection  an empty Hough image or the true track not being contained within the Hough image")
    args = parser.parse_args()
    DEBUG = args.debug
    resolution = args.resolution
    roi_window = 400*resolution/2 #mm

    print("The resolution is: ", resolution, "mm/pixel")
    print("The ROI window is: ", roi_window, "mm")
    
    #Open the root file
    #Check that the file exists and that the tree exists
    try:
        with open(args.file) as f:
            pass
    except IOError:
        print("File not found: ", args.file, file=sys.stderr)
        return
    with uproot.open(args.file) as file:
        try:
            tree = file[args.tree]
        except:
            print("Tree not found in the file", file=sys.stderr)
            return
    file_name = args.file.split("/")[-1].split(".")[0]
    #Check that the output directory exists and that it is writable
    if args.output_dir[-1] != "/":
        args.output_dir += "/"
    #Use os.path.isdir to check if the directory exists and print an error if it does not
    if not os.path.isdir(args.output_dir):
        print("Output directory does not exist: ", args.output_dir, file=sys.stderr)
        return
    if not os.access(args.output_dir, os.W_OK):
        print("Output directory is not writable: ", args.output_dir, file=sys.stderr)
        return
    
    
    def generate_hough_transforms(path_to_root_file, tree_name, display_id, output_dir, start_index=0, stop_index=-1):
        #Convert the arguments from tensorflow bytes to python strings
        if type(path_to_root_file) == bytes:
            path_to_root_file = path_to_root_file.decode("utf-8")
        if type(tree_name) == bytes:
            tree_name = tree_name.decode("utf-8")
        if type(output_dir) == bytes:
            output_dir = output_dir.decode("utf-8")
        #Open the root file
        if DEBUG: 
            print("Opening root file: ", path_to_root_file)
        tree = uproot.open(path_to_root_file)[tree_name]
        num_samples = len(tree["tt_bgoRecInterceptX"].array(library="np"))
        if DEBUG: 
            print("Number of samples:", num_samples)
            import psutil #To print RAM usage statistics

        

        #Loop over the events
        for id, (bgo_rec_intercept_x, bgo_rec_intercept_y, bgo_rec_slope_x, bgo_rec_slope_y, stk_cluster_x, stk_cluster_y, stk_cluster_z, t_XPos, t_YPos, t_ZPos, t_XPart, t_YPart, t_ZPart) in enumerate(iterable=zip(
                                                                                                tree["tt_bgoRecInterceptX"].array(library="np"), 
                                                                                                tree["tt_bgoRecInterceptY"].array(library="np"), 
                                                                                                tree["tt_bgoRecSlopeX"].array(library="np"),
                                                                                                tree["tt_bgoRecSlopeY"].array(library="np"),
                                                                                                tree["StkHit_XPos"].array(library="np"),
                                                                                                tree["StkHit_YPos"].array(library="np"),
                                                                                                tree["StkHit_ZPos"].array(library="np"),
                                                                                                tree["tt_mcprimary_XPos"].array(library="np"),
                                                                                                tree["tt_mcprimary_YPos"].array(library="np"),
                                                                                                tree["tt_mcprimary_ZPos"].array(library="np"),
                                                                                                tree["tt_mcprimary_XPart"].array(library="np"),
                                                                                                tree["tt_mcprimary_YPart"].array(library="np"),
                                                                                                tree["tt_mcprimary_ZPart"].array(library="np"))):
            if start_index > id:
                continue
            if id>=stop_index and stop_index != -1:
                return
            if DEBUG and id%5000 == 0:
                process = psutil.Process(os.getpid())
                #print the current time
                print("Completed", int(id/num_samples*100), "% of the events")
                print("RAM usage: ", round(process.memory_info().rss/1024/1024/1024, 3), "GB", "Virtual memory usage: ", round(process.memory_info().vms/1024/1024/1024,3), "GB")
            # Use the coarse model's prediction as inital guess instead of the bgo prediction
            X, hough_x_raster, hough_y_raster, roi_centre_top_x, roi_centre_bot_x, roi_centre_top_y, roi_centre_bot_y = preprocess_data(bgo_rec_intercept_x, bgo_rec_intercept_y, bgo_rec_slope_x, bgo_rec_slope_y, stk_cluster_x, stk_cluster_y, stk_cluster_z, as_sparse_tensor=True, resolution=resolution, roi_window=roi_window)
            label, t_x_top, t_x_bot, t_y_top, t_y_bot = preprocess_label(t_XPos, t_YPos, t_ZPos, t_XPart, t_YPart, t_ZPart, roi_centre_top_x, roi_centre_bot_x, roi_centre_top_y, roi_centre_bot_y, resolution=resolution)
            t_pixel_x_top, t_pixel_x_bot, t_pixel_y_top, t_pixel_y_bot = label
            #Clean data
            if args.clean_data:
                if np.sum(hough_x_raster) == 0 or np.sum(hough_y_raster) == 0:
                    continue
                if not check_strip_range(t_pixel_x_top, t_pixel_x_bot, t_pixel_y_top, t_pixel_y_bot):
                    continue
                #Check how many clusters there are in a certain window around the true track to decide whether the photon converted or not
                if not check_photon_conversion(stk_cluster_x, stk_cluster_y, stk_cluster_z, t_x_top, t_x_bot, t_y_top, t_y_bot):
                    continue

            #Display the images for debugging pruposes
            if display_id == id:
                fig_out_path = output_dir+tree_name+"_"+str(id)+"_hough_raster_{}.png"
                plot_hough_transform(hough_x_raster, hough_y_raster, t_pixel_x_top, t_pixel_x_bot, t_pixel_y_top, t_pixel_y_bot, resolution=resolution, roi_window=roi_window, fig_out_path=fig_out_path)
            yield X, label

    if args.display_only:
        func=generate_hough_transforms(path_to_root_file=args.file, tree_name=args.tree, display_id=args.display_id, output_dir=args.output_dir, start_index=args.display_id, stop_index=args.display_id+1)
        print("Display only, not saving the dataset")
        for id, (X, label) in enumerate(func):
            if id == args.display_id:
                break
        return
    
    if DEBUG: print("Creating dataset")
    #Generate the dataset from generator function
    tf_all = tf.data.Dataset.from_generator(
        generate_hough_transforms, 
        args=[args.file, args.tree, args.display_id, args.output_dir],
        output_signature=(
            tf.SparseTensorSpec(shape=(MODEL_X_SHAPE), dtype=tf.float16), 
            tf.TensorSpec(shape=(MODEL_LABEL_SHAPE), dtype=tf.float16)
        )
    )
    out_file_name = args.output_dir+file_name+DATASET_SUFFIX

    #Check if the dataset already exists
    if os.path.isdir(out_file_name) or os.path.isfile(out_file_name):
        if args.overwrite:
            print("Overwriting existing file: ", out_file_name)
            #remove the directory. Check that the directory name ends with DATASET_SUFFIX, is a directory and does contain "dataset_spec.pb"
            if out_file_name[-len(DATASET_SUFFIX):] == DATASET_SUFFIX and os.path.isdir(out_file_name) and "dataset_spec.pb" in os.listdir(out_file_name):
                print("Deleting dataset: ", out_file_name)
                for root, dirs, files in os.walk(out_file_name, topdown=False):
                    for name in files:
                        if DEBUG: print("Deleting: ", os.path.join(root, name))
                        os.remove(os.path.join(root, name))
                    for name in dirs:
                        os.rmdir(os.path.join(root, name))
                        if DEBUG: print("Deleting: ", os.path.join(root, name))
                if DEBUG: print("Deleting: ", out_file_name)
                os.rmdir(out_file_name)
            else:
                print("I almost deleted something I should not have. Exiting.", file=sys.stderr)
                return
        else:
            print("File already exists: ", out_file_name, file=sys.stderr)
            print("Use --overwrite to overwrite the file", file=sys.stderr)
            return
    #Save the dataset (will cause the generator function to be called)
    if DEBUG: print("Saving as tf.data.Dataset to: ", out_file_name)
    if args.clean_data:
        print("Data will be cleaned")
    tf_all.save(out_file_name, compression=args.compression)

    #Check the dataset for completeness
    if DEBUG: 
        print("Checking that the dataset is complete")
        num_samples = -1
        with uproot.open(args.file) as file:
            tree = file[args.tree]
            num_samples = len(tree["tt_bgoRecInterceptX"].array(library="np"))

        tf_dataset = tf.data.Dataset.load(args.output_dir+file_name+DATASET_SUFFIX)
        num_samples_tf = len(tf_dataset)
        if num_samples_tf != num_samples:
            print("The dataset is incomplete: {} != {}".format(num_samples_tf, num_samples), file=sys.stderr)
        else:
            print("The dataset is complete: {} == {}".format(num_samples_tf, num_samples)) 
    print("Done with parsing and preprocessing.")

if __name__ == "__main__":
    main()