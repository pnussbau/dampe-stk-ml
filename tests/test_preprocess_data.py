#pytest unit tests for dataParser.py
import pytest
from preprocess_data import get_roi_centre, get_bgo_intercept_and_slope
import numpy as np


BGO_BOTTOM_MOST_VERTICAL_POS = 58.5 #mm
BGO_TRACK_INTERCEPT_REFERENCE_Z = BGO_BOTTOM_MOST_VERTICAL_POS

@pytest.mark.parametrize("bgo_rec_intercept,bgo_rec_slope,z,expected_roi", [
    (0, 0, BGO_TRACK_INTERCEPT_REFERENCE_Z, 0),
    (0, 0, 100, 0),
    (100, 0, 100, 100),
    (100, 0, BGO_TRACK_INTERCEPT_REFERENCE_Z, 100),
    (0, 10, BGO_TRACK_INTERCEPT_REFERENCE_Z, 0),
    (0, 10, 100, (100-BGO_TRACK_INTERCEPT_REFERENCE_Z)*10),
    (10, 10, 100, (100-BGO_TRACK_INTERCEPT_REFERENCE_Z)*10 + 10)
])
def test_get_roi_centre(bgo_rec_intercept, bgo_rec_slope, z, expected_roi, projection='xy'):
    assert get_roi_centre(bgo_rec_intercept, bgo_rec_slope, z, projection) == expected_roi
@pytest.mark.parametrize("slope,intercept,ztop,zbot,projection", [
    (0.1, -10, 0, 100, "yz"),
    (2, -20, 0, 100, "xz"),
    (-0.1, 50, 1, 3, "yz"),
    (5, 50, 1, 3, "xz"),
    (np.array([0.1, 0.2]), np.array([-10, -20]), 0, 100, "yz")])
#Check that the function works also for a vector of intercepts and slopes
def test_get_bgo_intercept_and_slope(intercept, slope, ztop, zbot, projection):
    top = get_roi_centre(intercept, slope, ztop, projection)
    bot = get_roi_centre(intercept, slope, zbot, projection)
    intercept_f, slope_f = get_bgo_intercept_and_slope(top, ztop, bot, zbot, projection)
    assert np.all(np.isclose(intercept, intercept_f)), f"intercept {intercept} != {intercept_f}"
    assert np.all(np.isclose(slope, slope_f)), f"slope {slope} != {slope_f}"


