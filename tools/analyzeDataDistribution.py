import uproot
import awkward as ak
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import hist
from hist import Hist
import scipy.optimize as opt
#import zfit

def main():
    files = ["/home/parzival/EPFL/PDM/visualisation/visualisation/data/performance/DmlNtup_gamma_part124_MPhotonSelProj_TQ_SkimAllVar.root", "/home/parzival/EPFL/PDM/visualisation/visualisation/data/performance/DmlNtup_gamma_part124_MPhotonSelProj_TQ_SkimAllVar.root"]

    tree_name = "DmlNtup"
    #Read both root files and concat the "t_x", "t_y", "t_z", "t_energy", "t_px", "t_py", "t_pz" arrays
    
    all_x = np.empty(0)
    all_y = np.empty(0)
    all_z = np.empty(0)
    all_energy = np.empty(0)
    all_px = np.empty(0)
    all_py = np.empty(0)
    all_pz = np.empty(0)

    for file in files:
        with uproot.open(file) as f:
            tree = f[tree_name]
            t_x = tree["tt_mcprimary_XPos"].array(library="np")
            t_y = tree["tt_mcprimary_YPos"].array(library="np")
            t_z = tree["tt_mcprimary_ZPos"].array(library="np")
            t_energy = tree["tt_ekin"].array(library="np")
            t_px = tree["tt_mcprimary_XPart"].array(library="np")
            t_py = tree["tt_mcprimary_YPart"].array(library="np")
            t_pz = tree["tt_mcprimary_ZPart"].array(library="np")
            all_x = np.concatenate((all_x, t_x))
            all_y = np.concatenate((all_y, t_y))
            all_z = np.concatenate((all_z, t_z))
            all_energy = np.concatenate((all_energy, t_energy))
            all_px = np.concatenate((all_px, t_px))
            all_py = np.concatenate((all_py, t_py))
            all_pz = np.concatenate((all_pz, t_pz))

    print("Min energy: ", np.min(all_energy))
    print("Max energy: ", np.max(all_energy))
    print("Mean energy: ", np.mean(all_energy))
    
    all_energy = all_energy/1000 #Convert to GeV


    def power_law(x, a, b):
        return a * x**b
    
    def round_to_n(x,n):
        #if the first digit is 1 round to n+1 significant digits
        if(str(x)[0] == "1" or str(x)[0] == "-" and str(x)[1] == "1"):
            return round(x, -int(np.floor(np.log10(abs(x)))) + (n))
        else:
            return round(x, -int(np.floor(np.log10(abs(x)))) + (n - 1))
 
    def build_error_string(x, dx):  
        dx_rounded = round_to_n(dx, 1)
        #Round the value to the correct number of significant digits
        if x < 0:
            x_rounded = round_to_n(x, -int(np.floor(np.log10(abs(dx_rounded)))))
        else:
            x_rounded = round_to_n(x, int(np.floor(np.log10(abs(dx_rounded)))))
        x_string = "{:.1e}".format(x_rounded)
        dx_string = "{:.1e}".format(dx_rounded)
        return x_string + " $\pm$ " + dx_string


    hist_1 = hist.Hist.new.Regular(50, 1, 100, name="Truth Energy", label="Truth Energy [GeV]").Double().fill(all_energy)
    fig = plt.figure(figsize=(10, 8))
    hist_1.plot(histtype="step", yerr=True)
    # Fit the pdf to the histogram
    y = hist_1.values()
    x = hist_1.axes.centers[0]

    params, params_covariance = opt.curve_fit(power_law, x, y, p0=[1, 1])
    print(params)
    print(params_covariance)
    


    label_text = "$Fit: f(x) = a \cdot x^b$ \n $a$ = " + build_error_string(params[0], np.sqrt(params_covariance[0][0])) + " \n $b$ = " + build_error_string(params[1], np.sqrt(params_covariance[1][1])) 
    plt.plot(x, power_law(x, params[0], params[1]), label=label_text)
    plt.legend(fontsize=22)
    plt.xlabel("Energy [GeV]", fontsize=24)
    plt.ylabel("Events", fontsize=24)
    plt.tick_params(axis="y", which="major", labelsize=18)
    plt.tick_params(axis="x", which="major", labelsize=18)
    plt.grid(which="minor", alpha=0.2)
    plt.grid(which="major", alpha=0.5)
    plt.xscale("log")
    plt.yscale("log")

    plt.tight_layout()
    plt.savefig("truth_energy_histogram.pdf")
    plt.savefig("truth_energy_histogram.png")
    plt.close()


    # Calculate the true angle with respect to the z-axis
    angle_x = np.arctan2(all_px, all_pz)
    angle_y = np.arctan2(all_py, all_pz)
   
    # Create a histogram of the true angle
    hist_true_angle_x = hist.Hist.new.Reg(40, -np.pi/2, np.pi/2, name="True Angle", label="x plane").Double().fill(angle_x)
    hist_true_angle_y = hist.Hist.new.Reg(40, -np.pi/2, np.pi/2, name="True Angle", label="y plane").Double().fill(angle_y)


    # Plot the histogram
    fig, ax = plt.subplots()
    #Plot the histogram
    hist_true_angle_x.plot(ax=ax, label="xz plane")
    hist_true_angle_y.plot(ax=ax, label="yz plane")
    mpl.rcParams['hatch.linewidth'] = 0.5
    mpl.rcParams['hatch.color'] = "black"
    

    ax.set_xlabel("Angle [rad]", fontsize=18)
    ax.set_ylabel("Events", fontsize=18)
    #Set the axis font size to 14
  
    #Show the grid minor 
    ax.grid(which="minor", alpha=0.2)
    ax.grid(which="major", alpha=0.5)
    #Make the ticks bigger
    
    ax.set_xticks([-np.pi/2, -3*np.pi/8, -np.pi/4, -np.pi/8, 0, np.pi/8, np.pi/4, 3*np.pi/8, np.pi/2])
    ax.set_xticklabels(["$-\pi/2$", "$-3\pi/8$", "$-\pi/4$", "$-\pi/8$", "$0$", "$\pi/8$", "$\pi/4$", "$3\pi/8$", "$\pi/2$"])
    ax.tick_params(axis="y", which="major", labelsize=14)
    ax.tick_params(axis="x", which="major", labelsize=14)
    ax.legend(fontsize=14)
  
    plt.tight_layout()
    plt.savefig("truth_angle_histogram.pdf")
    plt.savefig("truth_angle_histogram.png")
    ax.set_title("Distribution of MC incident direction with respect to z-axis")
    plt.tight_layout()
    plt.savefig("truth_angle_histogram_title.pdf")
    plt.savefig("truth_angle_histogram_title.png")


if __name__ == "__main__":
    # Run the main function
    main()
