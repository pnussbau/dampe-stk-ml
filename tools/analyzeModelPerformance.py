import uproot
import awkward as ak
import numpy as np
import matplotlib.pyplot as plt
from hist import Hist
import os
from  visualizeEvent import PSD_STRIP_CENTRE_XZ_Z_POS, PSD_STRIP_CENTRE_YZ_Z_POS, STK_STRIP_XZ_Z_POS, STK_STRIP_YZ_Z_POS

BGO_TRACK_INTERCEPT_REFERENCE_XZ = 0
BGO_TRACK_INTERCEPT_REFERENCE_YZ = 0

PLOT = True
IS_MC = True
FIT_PEAK = False

BASE_TREE_NAME = "DmlNtup"
FRIEND_TREE_NAME = 'cnn_track;1'
BASE_DATA_PATH = "/home/parzival/EPFL/PDM/data/MC_Skim/"
#BASE_DATA_PATH = "/home/parzival/EPFL/PDM/data/MC_GammaSel/"
#BASE_DATA_PATH = "/home/parzival/EPFL/PDM/data/Data_GammaSel/"
FRIEND_DATA_PATH = "/home/parzival/EPFL/PDM/parsed/inference_mc/"
#FRIEND_DATA_PATH = "/home/parzival/EPFL/PDM/parsed/validation_mc/"
#FRIEND_DATA_PATH = "/home/parzival/EPFL/PDM/parsed/inference_flight/"
DATA_OUT_PATH = FRIEND_DATA_PATH 


#MODELS_TO_PLOT = [[800,800],[800,50], [800,100], [500, 500], [500, 100], [500, 50], [800], [500], [100,], [50,]]
MODELS_TO_PLOT= [[800,50]]

if FIT_PEAK:
    import zfit
    
def build_model_strings(resolution1, resolution2=None):
        model_strings = []
        
        if resolution2:
            model_strings.append("${}$ $\mu$m/pixel + ${}$ $\mu$m/pixel".format(resolution1, resolution2))
            if resolution2 == 50:
                resolution2 = "050"
            model_strings.append("{}um_{}um".format(resolution1, resolution2))
            model_strings.append("{}um-{}um".format(resolution1, resolution2))
        else:
            model_strings.append("${}$ $\mu$m/pixel".format(resolution1))
            if resolution1 == 50:
                resolution1 = "050"
            model_strings.append("{}um".format(resolution1))
            model_strings.append("{}um".format(resolution1))
        
        return model_strings
def main():
    keys = ["cnn_stk_track_x1", "cnn_stk_track_y1", "cnn_stk_track_x2", "cnn_stk_track_y2"]

    for resolutions in MODELS_TO_PLOT:
        base_root_files = []

        CNN_MODEL_NAME, file_name, folder_name = build_model_strings(*resolutions)

        #Get all the root files in the directory BASE_DATA_PATH/
        for root, dirs, files in os.walk(BASE_DATA_PATH):
            for file in files:
                if file.endswith(".root"):
                    base_root_files.append(os.path.join(root, file))

        friend_root_files = []
        #Get all the root files in the directory FRIEND_DATA_PATH/folder_name
        for root, dirs, files in os.walk(FRIEND_DATA_PATH + folder_name):
            for file in files:
                if file.endswith(".root"):
                    friend_root_files.append(os.path.join(root, file))
        #Sort the paths alphabetically
        base_root_files.sort()
        friend_root_files.sort()

        print(base_root_files)
        print(friend_root_files)

        #Only keep the first 1 root files
        # base_root_files = base_root_files[:1]
        # friend_root_files = friend_root_files[:1]

        # Print the number of root files
        print("Number of base root files: {}".format(len(base_root_files)))
        print("Number of friend root files: {}".format(len(friend_root_files)))


        assert len(base_root_files) == len(friend_root_files), "The number of base root files is not the same as the number of friend root files"
        # Initialize empty lists to store the variables
        t_x_list = []
        t_y_list = []
        t_z_list = []
        t_energy_list = []
        t_px_list = []
        t_py_list = []
        t_pz_list = []
        bgo_x_intercept_list = []
        bgo_y_intercept_list = []
        bgo_x_slope_list = []
        bgo_y_slope_list = []
        bgo_energy_list = []
        cnn_x1_list = []
        cnn_y1_list = []
        cnn_x2_list = []
        cnn_y2_list = []
        track_tq_list = []
        track_x1_list = []
        track_x2_list = []
        track_y1_list = []
        track_y2_list = []

        for base_root_file, friend_root_file in zip(base_root_files, friend_root_files):
            with uproot.open(base_root_file) as base, uproot.open(friend_root_file) as friend:
                base_tree = base[BASE_TREE_NAME]
                friend_tree = friend[FRIEND_TREE_NAME]

                # Read the variables from the root files
                if IS_MC:
                    t_x = base_tree["tt_mcprimary_XPos"].array(library="np")
                    t_y = base_tree["tt_mcprimary_YPos"].array(library="np")
                    t_z = base_tree["tt_mcprimary_ZPos"].array(library="np")
                    t_energy = base_tree["tt_ekin"].array(library="np")
                    t_px = base_tree["tt_mcprimary_XPart"].array(library="np")
                    t_py = base_tree["tt_mcprimary_YPart"].array(library="np")
                    t_pz = base_tree["tt_mcprimary_ZPart"].array(library="np")
                bgo_x_intercept = base_tree["tt_bgoRecInterceptX"].array(library="np")
                bgo_y_intercept = base_tree["tt_bgoRecInterceptY"].array(library="np")
                bgo_x_slope = base_tree["tt_bgoRecSlopeX"].array(library="np")
                bgo_y_slope = base_tree["tt_bgoRecSlopeY"].array(library="np")
                bgo_energy = base_tree["tt_bgoTotalE_GeV"].array(library="np")
                cnn_x1 = friend_tree[keys[0]].array(library="np")
                cnn_y1 = friend_tree[keys[1]].array(library="np")
                cnn_x2 = friend_tree[keys[2]].array(library="np")
                cnn_y2 = friend_tree[keys[3]].array(library="np")
                track_tq = base_tree["TQ_Corr"].array(library="ak")
                track_x1 = base_tree["X_Projection_X1"].array(library="ak")
                track_x2 = base_tree["X_Projection_X2"].array(library="ak")
                track_y1 = base_tree["Y_Projection_Y1"].array(library="ak")
                track_y2 = base_tree["Y_Projection_Y2"].array(library="ak")

                # Append the variables to the respective lists
                if IS_MC:
                    t_x_list.append(t_x)
                    t_y_list.append(t_y)
                    t_z_list.append(t_z)
                    t_energy_list.append(t_energy)
                    t_px_list.append(t_px)
                    t_py_list.append(t_py)
                    t_pz_list.append(t_pz)
                bgo_x_intercept_list.append(bgo_x_intercept)
                bgo_y_intercept_list.append(bgo_y_intercept)
                bgo_x_slope_list.append(bgo_x_slope)
                bgo_y_slope_list.append(bgo_y_slope)
                bgo_energy_list.append(bgo_energy)
                cnn_x1_list.append(cnn_x1)
                cnn_y1_list.append(cnn_y1)
                cnn_x2_list.append(cnn_x2)
                cnn_y2_list.append(cnn_y2)
                track_tq_list.append(track_tq)
                track_x1_list.append(track_x1)
                track_x2_list.append(track_x2)
                track_y1_list.append(track_y1)
                track_y2_list.append(track_y2)

        # Stack the variables along the first axis
        if IS_MC:
            t_x_stacked = np.concatenate(t_x_list)
            t_y_stacked = np.concatenate(t_y_list)
            t_z_stacked = np.concatenate(t_z_list)
            t_energy_stacked = np.concatenate(t_energy_list)
            t_px_stacked = np.concatenate(t_px_list)
            t_py_stacked = np.concatenate(t_py_list)
            t_pz_stacked = np.concatenate(t_pz_list)
        bgo_x_intercept_stacked = np.concatenate(bgo_x_intercept_list)
        bgo_y_intercept_stacked = np.concatenate(bgo_y_intercept_list)
        bgo_x_slope_stacked = np.concatenate(bgo_x_slope_list)
        bgo_y_slope_stacked = np.concatenate(bgo_y_slope_list)
        bgo_energy_stacked = np.concatenate(bgo_energy_list)
        cnn_x1_stacked = np.concatenate(cnn_x1_list)
        cnn_y1_stacked = np.concatenate(cnn_y1_list)
        cnn_x2_stacked = np.concatenate(cnn_x2_list)
        cnn_y2_stacked = np.concatenate(cnn_y2_list)
        track_tq_stacked = ak.concatenate(track_tq_list)
        track_x1_stacked = ak.concatenate(track_x1_list)
        track_x2_stacked = ak.concatenate(track_x2_list)
        track_y1_stacked = ak.concatenate(track_y1_list)
        track_y2_stacked = ak.concatenate(track_y2_list)

        # Print the stacked variables
        if IS_MC:
            print("Stacked t_x shape:", t_x_stacked.shape)
            print("Stacked t_y shape:", t_y_stacked.shape)
            print("Stacked t_z shape:", t_z_stacked.shape)
            print("Stacked t_energy shape:", t_energy_stacked.shape)
            print("Stacked t_px shape:", t_px_stacked.shape)
            print("Stacked t_py shape:", t_py_stacked.shape)
            print("Stacked t_pz shape:", t_pz_stacked.shape)
        print("Stacked bgo_x_intercept shape:", bgo_x_intercept_stacked.shape)
        print("Stacked bgo_y_intercept shape:", bgo_y_intercept_stacked.shape)
        print("Stacked bgo_x_slope shape:", bgo_x_slope_stacked.shape)
        print("Stacked bgo_y_slope shape:", bgo_y_slope_stacked.shape)
        print("Stacked cnn_x1 shape:", cnn_x1_stacked.shape)
        print("Stacked cnn_y1 shape:", cnn_y1_stacked.shape)
        print("Stacked cnn_x2 shape:", cnn_x2_stacked.shape)
        print("Stacked cnn_y2 shape:", cnn_y2_stacked.shape)

        cnn_x1 = cnn_x1_stacked
        cnn_x2 = cnn_x2_stacked
        cnn_y1 = cnn_y1_stacked
        cnn_y2 = cnn_y2_stacked
        if IS_MC:
            t_x = t_x_stacked
            t_y = t_y_stacked
            t_z = t_z_stacked
            t_energy = t_energy_stacked
            t_px = t_px_stacked
            t_py = t_py_stacked
            t_pz = t_pz_stacked
        bgo_x_slope = bgo_x_slope_stacked
        bgo_y_slope = bgo_y_slope_stacked
        bgo_x_intercept = bgo_x_intercept_stacked
        bgo_y_intercept = bgo_y_intercept_stacked
        bgo_energy = bgo_energy_stacked
        track_tq = track_tq_stacked
        track_x1 = track_x1_stacked
        track_x2 = track_x2_stacked
        track_y1 = track_y1_stacked
        track_y2 = track_y2_stacked



        #Remove any NaNs, -inf and inf from the CNN variables
        #Mask all the x1, x2, y1, y2 values that are NaN. If one of the values is NaN, all the values in the event are NaN
        mask_cnn_x1 = np.isnan(cnn_x1)
        mask_cnn_x2 = np.isnan(cnn_x2)
        mask_cnn_y1 = np.isnan(cnn_y1)
        mask_cnn_y2 = np.isnan(cnn_y2)  
        mask_cnn = mask_cnn_x1 | mask_cnn_x2 | mask_cnn_y1 | mask_cnn_y2

        #Mask all the events with x1, x2, y1, y2 values that are abs larger than 1000
        # mask_cnn_x1 = np.abs(cnn_x1) > 4000
        # mask_cnn_x2 = np.abs(cnn_x2) > 4000
        # mask_cnn_y1 = np.abs(cnn_y1) > 4000
        # mask_cnn_y2 = np.abs(cnn_y2) > 4000
        # mask_cnn = mask_cnn | mask_cnn_x1 | mask_cnn_x2 | mask_cnn_y1 | mask_cnn_y2

        # Mask all the events with x1, x2, y1, y2 values that are -inf or inf
        mask_cnn_x1 = np.isinf(cnn_x1)
        mask_cnn_x2 = np.isinf(cnn_x2)
        mask_cnn_y1 = np.isinf(cnn_y1)
        mask_cnn_y2 = np.isinf(cnn_y2)
        mask_cnn = mask_cnn | mask_cnn_x1 | mask_cnn_x2 | mask_cnn_y1 | mask_cnn_y2

        # Apply the mask to all the variables
        print("Number of events that will be removed: {}".format(np.sum(mask_cnn)))
        if IS_MC:
            t_x = t_x[~mask_cnn]
            t_y = t_y[~mask_cnn]
            t_z = t_z[~mask_cnn]
            t_energy = t_energy[~mask_cnn]
            t_px = t_px[~mask_cnn]
            t_py = t_py[~mask_cnn]
            t_pz = t_pz[~mask_cnn]
        cnn_x1 = cnn_x1[~mask_cnn]
        cnn_x2 = cnn_x2[~mask_cnn]
        cnn_y1 = cnn_y1[~mask_cnn]
        cnn_y2 = cnn_y2[~mask_cnn]
        track_tq = track_tq[~mask_cnn]
        track_x1 = track_x1[~mask_cnn]
        track_x2 = track_x2[~mask_cnn]
        track_y1 = track_y1[~mask_cnn]
        track_y2 = track_y2[~mask_cnn]
        bgo_x_slope = bgo_x_slope[~mask_cnn]
        bgo_y_slope = bgo_y_slope[~mask_cnn]
        bgo_x_intercept = bgo_x_intercept[~mask_cnn]
        bgo_y_intercept = bgo_y_intercept[~mask_cnn]
        bgo_energy = bgo_energy[~mask_cnn]



        # Get the length of dimension 0 of the ak array
        print("Length of track_x1: {}".format(len(track_x1)))
        print("TQ length: {}".format(len(track_tq)))
        print("Max of TQ: {}".format(np.max(track_tq)))
        print("Min of TQ: {}".format(np.min(track_tq)))

        #Get the track with the best TQ for each event
        #track_tq = ak.fill_none(track_tq, -np.inf)
        
        print("Length of TQ: {}".format(len(track_tq)))
        best_track_indices = ak.argmax(track_tq, axis=1, keepdims=True)
        #best_track_indices = ak.fill_none(best_track_indices, 0)
        #REplace empty arrays with an array with value np.nan
        # track_x1 = ak.where(ak.num(track_x1) == 0, ak.Array([-100]), track_x1)
        # track_x2 = ak.where(ak.num(track_x2) == 0, ak.Array([-100]), track_x2)
        # track_y1 = ak.where(ak.num(track_y1) == 0, ak.Array([-100]), track_y1)
        # track_y2 = ak.where(ak.num(track_y2) == 0, ak.Array([-100]), track_y2)
        #Using the best track indices along axis 1 get the best track's corresponding x1, x2, y1, y2
        best_track_x1 = ak.flatten(track_x1[best_track_indices])
        best_track_x2 = ak.flatten(track_x2[best_track_indices])
        best_track_y1 = ak.flatten(track_y1[best_track_indices])
        best_track_y2 = ak.flatten(track_y2[best_track_indices])



        #Print the shape of the best track
        best_track_x1 = ak.fill_none(best_track_x1, np.nan)
        best_track_x2 = ak.fill_none(best_track_x2, np.nan)
        best_track_y1 = ak.fill_none(best_track_y1, np.nan)
        best_track_y2 = ak.fill_none(best_track_y2, np.nan)
        #Replace the None values with NaN
        best_track_x1 = ak.to_numpy(best_track_x1)
        best_track_x2 = ak.to_numpy(best_track_x2)
        best_track_y1 = ak.to_numpy(best_track_y1)
        best_track_y2 = ak.to_numpy(best_track_y2)


        #Truth 
        if IS_MC:
            slope_truth_x = t_px / t_pz
            slope_truth_y = t_py / t_pz
            angle_truth_x = np.arctan(slope_truth_x) / np.pi * 180
            angle_truth_y = np.arctan(slope_truth_y) / np.pi * 180


    

        # #Remove all events with an truth angle greater than 45 degrees
        # mask_indices_smaller_45 = (np.abs(angle_truth_x) < 10) & (np.abs(angle_truth_y) < 10)
        # t_x = t_x[mask_indices_smaller_45]
        # t_y = t_y[mask_indices_smaller_45]
        # t_z = t_z[mask_indices_smaller_45]
        # t_energy = t_energy[mask_indices_smaller_45]
        # t_px = t_px[mask_indices_smaller_45]
        # t_py = t_py[mask_indices_smaller_45]
        # t_pz = t_pz[mask_indices_smaller_45]
        # slope_truth_x = slope_truth_x[mask_indices_smaller_45]
        # slope_truth_y = slope_truth_y[mask_indices_smaller_45]
        # angle_truth_x = angle_truth_x[mask_indices_smaller_45]
        # angle_truth_y = angle_truth_y[mask_indices_smaller_45]
        # cnn_x1 = cnn_x1[mask_indices_smaller_45]
        # cnn_x2 = cnn_x2[mask_indices_smaller_45]
        # cnn_y1 = cnn_y1[mask_indices_smaller_45]
        # cnn_y2 = cnn_y2[mask_indices_smaller_45]

        # track_tq = track_tq[mask_indices_smaller_45]
        # track_x1 = track_x1[mask_indices_smaller_45]
        # track_x2 = track_x2[mask_indices_smaller_45]
        # track_y1 = track_y1[mask_indices_smaller_45]
        # track_y2 = track_y2[mask_indices_smaller_45]

        # best_track_x1 = best_track_x1[mask_indices_smaller_45]
        # best_track_x2 = best_track_x2[mask_indices_smaller_45]
        # best_track_y1 = best_track_y1[mask_indices_smaller_45]
        # best_track_y2 = best_track_y2[mask_indices_smaller_45]




        #Remove all events with an angle larger than 10 degrees between truth and BGO
        bgo_angle_x = np.arctan(bgo_x_slope) / np.pi * 180
        bgo_angle_y = np.arctan(bgo_y_slope) / np.pi * 180



        if IS_MC:
            mask_indices_smaller_10 = (np.abs(bgo_angle_x - angle_truth_x) < 1) & (np.abs(bgo_angle_y - angle_truth_y) < 1)
            # t_x = t_x[mask_indices_smaller_10]
            # t_y = t_y[mask_indices_smaller_10]
            # t_z = t_z[mask_indices_smaller_10]
            # t_energy = t_energy[mask_indices_smaller_10]
            # t_px = t_px[mask_indices_smaller_10]
            # t_py = t_py[mask_indices_smaller_10]
            # t_pz = t_pz[mask_indices_smaller_10]
            # bgo_x_intercept = bgo_x_intercept[mask_indices_smaller_10]
            # bgo_y_intercept = bgo_y_intercept[mask_indices_smaller_10]
            # bgo_angle_x = bgo_angle_x[mask_indices_smaller_10]
            # bgo_x_slope = bgo_x_slope[mask_indices_smaller_10]
            # bgo_angle_y = bgo_angle_y[mask_indices_smaller_10]
            # bgo_y_slope = bgo_y_slope[mask_indices_smaller_10]
            # slope_truth_x = slope_truth_x[mask_indices_smaller_10]
            # slope_truth_y = slope_truth_y[mask_indices_smaller_10]
            # angle_truth_x = angle_truth_x[mask_indices_smaller_10]
            # angle_truth_y = angle_truth_y[mask_indices_smaller_10]
            # cnn_x1 = cnn_x1[mask_indices_smaller_10]
            # cnn_x2 = cnn_x2[mask_indices_smaller_10]
            # cnn_y1 = cnn_y1[mask_indices_smaller_10]
            # cnn_y2 = cnn_y2[mask_indices_smaller_10]
            # track_tq = track_tq[mask_indices_smaller_10]
            # track_x1 = track_x1[mask_indices_smaller_10]
            # track_x2 = track_x2[mask_indices_smaller_10]
            # track_y1 = track_y1[mask_indices_smaller_10]
            # track_y2 = track_y2[mask_indices_smaller_10]
            # best_track_x1 = best_track_x1[mask_indices_smaller_10]
            # best_track_x2 = best_track_x2[mask_indices_smaller_10]
            # best_track_y1 = best_track_y1[mask_indices_smaller_10]
            # best_track_y2 = best_track_y2[mask_indices_smaller_10]


        #Keep only events with a track with a TQ larger 10 along axis 1 
        # mask_indices_tq_larger_10 = ak.any(track_tq > 1, axis=1)


        #Best Track
        slope_best_track_x = (best_track_x2 - best_track_x1) / (PSD_STRIP_CENTRE_XZ_Z_POS[0] - PSD_STRIP_CENTRE_XZ_Z_POS[-1])
        slope_best_track_y = (best_track_y2 - best_track_y1) / (PSD_STRIP_CENTRE_YZ_Z_POS[0] - PSD_STRIP_CENTRE_YZ_Z_POS[-1])
    

        #Find the mask for the events where the value is not None
        mask_best_track_exists = ~np.isnan(best_track_x1) & ~np.isnan(best_track_x2) & ~np.isnan(best_track_y1) & ~np.isnan(best_track_y2)
        print("Number of events with a best track: {}".format(np.sum(mask_best_track_exists)))


        # slope_diff_best_track_truth = np.sqrt(slope_best_track_x_diff_truth**2 + slope_best_track_y_diff_truth**2)
        if IS_MC:
            angle_best_track_x_diff_truth = (np.arctan(slope_best_track_x[mask_best_track_exists]) - np.arctan(slope_truth_x[mask_best_track_exists])) * 180 / np.pi
            angle_best_track_y_diff_truth = (np.arctan(slope_best_track_y[mask_best_track_exists]) - np.arctan(slope_truth_y[mask_best_track_exists])) * 180 / np.pi

            truth_x1 = t_x + slope_truth_x * (PSD_STRIP_CENTRE_XZ_Z_POS[-1] - t_z)
            truth_x2 = t_x + slope_truth_x * (PSD_STRIP_CENTRE_XZ_Z_POS[0] - t_z)
            truth_y1 = t_y + slope_truth_y * (PSD_STRIP_CENTRE_YZ_Z_POS[-1] - t_z)
            truth_y2 = t_y + slope_truth_y * (PSD_STRIP_CENTRE_YZ_Z_POS[0] - t_z)
        
            best_track_offset_x1 = best_track_x1[mask_best_track_exists] - truth_x1[mask_best_track_exists]
            best_track_offset_y1 = best_track_y1[mask_best_track_exists] - truth_y1[mask_best_track_exists]



        #BGO
        #Angle
        if IS_MC:
            slope_bgo_x_diff_truth = bgo_x_slope - slope_truth_x
            slope_bgo_y_diff_truth = bgo_y_slope - slope_truth_y
            angle_bgo_x_diff_truth = bgo_angle_x - angle_truth_x
            angle_bgo_y_diff_truth = bgo_angle_y - angle_truth_y
        #Offset
        if IS_MC:
            bgo_offset_x1 = bgo_x_intercept + bgo_x_slope * (PSD_STRIP_CENTRE_XZ_Z_POS[-1] - BGO_TRACK_INTERCEPT_REFERENCE_XZ) - truth_x1
            bgo_offset_y1 = bgo_y_intercept + bgo_y_slope * (PSD_STRIP_CENTRE_YZ_Z_POS[-1] - BGO_TRACK_INTERCEPT_REFERENCE_YZ) - truth_y1
            angle_best_track_diff_truth = np.sqrt(angle_best_track_x_diff_truth**2 + angle_best_track_y_diff_truth**2)
        best_track_x1 = best_track_x1[mask_best_track_exists]
        best_track_x2 = best_track_x2[mask_best_track_exists]
        best_track_y1 = best_track_y1[mask_best_track_exists]
        best_track_y2 = best_track_y2[mask_best_track_exists]
        if IS_MC:
            t_energy_best_track = t_energy[mask_best_track_exists]
            slope_truth_x = slope_truth_x[mask_best_track_exists]
            slope_truth_y = slope_truth_y[mask_best_track_exists]
            angle_truth_x = angle_truth_x[mask_best_track_exists]
            angle_truth_y = angle_truth_y[mask_best_track_exists]
        cnn_x1 = cnn_x1[mask_best_track_exists]
        cnn_x2 = cnn_x2[mask_best_track_exists]
        cnn_y1 = cnn_y1[mask_best_track_exists]
        cnn_y2 = cnn_y2[mask_best_track_exists]
        if IS_MC:
            t_energy = t_energy[mask_best_track_exists]
            t_x = t_x[mask_best_track_exists]
            t_y = t_y[mask_best_track_exists]
            t_z = t_z[mask_best_track_exists]
            t_px = t_px[mask_best_track_exists]
            t_py = t_py[mask_best_track_exists]
            t_pz = t_pz[mask_best_track_exists]
            truth_x1 = truth_x1[mask_best_track_exists]
            truth_x2 = truth_x2[mask_best_track_exists]
            truth_y1 = truth_y1[mask_best_track_exists]
            truth_y2 = truth_y2[mask_best_track_exists]
        bgo_angle_x = bgo_angle_x[mask_best_track_exists]
        bgo_x_slope = bgo_x_slope[mask_best_track_exists]
        bgo_angle_y = bgo_angle_y[mask_best_track_exists]
        bgo_y_slope = bgo_y_slope[mask_best_track_exists]
        bgo_energy = bgo_energy[mask_best_track_exists]
        #bgo_offset_x1 = bgo_offset_x1[mask_best_track_exists]
        #bgo_offset_y1 = bgo_offset_y1[mask_best_track_exists]
        slope_best_track_x = slope_best_track_x[mask_best_track_exists]
        slope_best_track_y = slope_best_track_y[mask_best_track_exists]

        if IS_MC:
            assert len(slope_truth_x) == len(slope_truth_y) == len(cnn_x1) == len(cnn_x2) == len(cnn_y1) == len(cnn_y2) == len(t_energy) == len(t_px) == len(t_py) == len(t_pz), "The number of events is not the same"
        else:
            assert len(cnn_x1) == len(cnn_x2) == len(cnn_y1) == len(cnn_y2), "The number of events is not the same"
        
        #Compute the solpe in x and y with respect to z. x1 and y1 are measured at PSD_STRIP_CENTRE_XZ_Z_POS[-1] and PSD_STRIP_CENTRE_YZ_Z_POS[-1] respectively
        # x2 and y2 are measured at PSD_STRIP_CENTRE_XZ_Z_POS[0] and PSD_STRIP_CENTRE_YZ_Z_POS[0] respectively
        if USE_PSD_REFERENCE:
            slope_cnn_x = (cnn_x2 - cnn_x1) * (PSD_STRIP_CENTRE_XZ_Z_POS[0] - PSD_STRIP_CENTRE_XZ_Z_POS[-1])**-1
            slope_cnn_y = (cnn_y2 - cnn_y1) * (PSD_STRIP_CENTRE_YZ_Z_POS[0] - PSD_STRIP_CENTRE_YZ_Z_POS[-1])**-1
        else :
            slope_cnn_x = (cnn_x2 - cnn_x1) * (STK_STRIP_XZ_Z_POS[0] - STK_STRIP_XZ_Z_POS[-1])**-1
            slope_cnn_y = (cnn_y2 - cnn_y1) * (STK_STRIP_YZ_Z_POS[0] - STK_STRIP_YZ_Z_POS[-1])**-1
        angle_cnn_x = np.arctan(slope_cnn_x) / np.pi * 180
        angle_cnn_y = np.arctan(slope_cnn_y) / np.pi * 180
        if IS_MC:
            slope_cnn_x_diff_truth = slope_cnn_x - slope_truth_x
            slope_cnn_y_diff_truth = slope_cnn_y - slope_truth_y
            slope_diff_cnn_truth = np.sqrt(slope_cnn_x_diff_truth**2 + slope_cnn_y_diff_truth**2)



        if IS_MC:
            assert len(slope_truth_x) == len(slope_truth_y) == len(cnn_x1) == len(cnn_x2) == len(cnn_y1) == len(cnn_y2) == len(t_energy) == len(t_px) == len(t_py) == len(t_pz), "The number of events is not the same"
        else:
            assert len(cnn_x1) == len(cnn_x2) == len(cnn_y1) == len(cnn_y2), "The number of events is not the same"
        #Compute the angle using arctan
        if IS_MC:
            angle_cnn_x_diff_truth = (np.arctan(slope_cnn_x) - np.arctan(slope_truth_x)) * 180 / np.pi
            angle_cnn_y_diff_truth = (np.arctan(slope_cnn_y) - np.arctan(slope_truth_y)) * 180 / np.pi

        #Offset for cnn
        if IS_MC:
            cnn_offset_x1 = cnn_x1 - truth_x1
            cnn_offset_y1 = cnn_x2 - truth_x2


        #Store data for validation differences between CNN and truth
        angle_cnn_x_diff_best_track = (np.arctan(slope_cnn_x) - np.arctan(slope_best_track_x)) * 180 / np.pi
        angle_cnn_y_diff_best_track = (np.arctan(slope_cnn_y) - np.arctan(slope_best_track_y)) * 180 / np.pi
        
        cnn_offset_x1_best_track = cnn_x1 - best_track_x1
        cnn_offset_y1_best_track = cnn_y1 - best_track_y1
    
        #Save the data to a npz file  store athe following variables to #FRIEND_DATA_PATH/folder_name/angle_offset_energy.npz
        #angle_cnn_x_diff_truth, angle_bgo_x_diff_truth, angle_best_track_x_diff_truth
        #angle_cnn_y_diff_truth, angle_bgo_y_diff_truth, angle_best_track_y_diff_truth
        #cnn_offset_x1, bgo_offset_x1, best_track_offset_x1
        #cnn_offset_y1, bgo_offset_y1, best_track_offset_y1
        #t_energy, t_energy_best_track
        print("Saving data to {}".format(DATA_OUT_PATH + folder_name + "_angle_offset_energy.npz"))

        if IS_MC:
            np.savez(DATA_OUT_PATH + folder_name + "_angle_offset_energy.npz", cnn_model_name=folder_name, cnn_model_name_display=CNN_MODEL_NAME, angle_cnn_x_diff_truth=angle_cnn_x_diff_truth, angle_bgo_x_diff_truth=angle_bgo_x_diff_truth, angle_best_track_x_diff_truth=angle_best_track_x_diff_truth, angle_cnn_y_diff_truth=angle_cnn_y_diff_truth, angle_bgo_y_diff_truth=angle_bgo_y_diff_truth, angle_best_track_y_diff_truth=angle_best_track_y_diff_truth, cnn_offset_x1=cnn_offset_x1, bgo_offset_x1=bgo_offset_x1, best_track_offset_x1=best_track_offset_x1, cnn_offset_y1=cnn_offset_y1, bgo_offset_y1=bgo_offset_y1, best_track_offset_y1=best_track_offset_y1, t_energy=t_energy, t_energy_best_track=t_energy_best_track, angle_cnn_x_diff_best_track=angle_cnn_x_diff_best_track, angle_cnn_y_diff_best_track=angle_cnn_y_diff_best_track, cnn_offset_x1_best_track=cnn_offset_x1_best_track, cnn_offset_y1_best_track=cnn_offset_y1_best_track, bgo_energy=bgo_energy)
        else:
            np.savez(DATA_OUT_PATH + folder_name + "_angle_offset_energy.npz", cnn_model_name=folder_name, cnn_model_name_display=CNN_MODEL_NAME, angle_cnn_x_diff_best_track=angle_cnn_x_diff_best_track, angle_cnn_y_diff_best_track=angle_cnn_y_diff_best_track, cnn_offset_x1_best_track=cnn_offset_x1_best_track, cnn_offset_y1_best_track=cnn_offset_y1_best_track, bgo_energy=bgo_energy)

        if not PLOT:
            continue
        ## Plotting ##
        print("Plotting")

        fig, axs = plt.subplots(2, 2, figsize=(10, 10))

        N_BINS = 50
        LOWER_BOUND = -3
        UPPER_BOUND = 3
        LABEL_BEST = "Std reco"
        LABEL_CNN = "$500$ $\mu$m/pixel \n+ $80$ $\mu$m/pixel"
        LABEL_BGO = "BGO only reco"
        LS_BEST = "-"
        LS_CNN = "--"
        LS_BGO = "-."
        COLOR_BEST = "red"
        COLOR_CNN = "black"
        COLOR_BGO = "green"
        AXIS_LABEL_SIZE = 14
        SET_Y_LOG = True




        def perform_fit(histogram, obs_name, lower_bound, upper_bound, ax):
            binned_data = zfit.data.BinnedData.from_hist(histogram).to_unbinned()
            obs = zfit.Space(obs_name, limits=(lower_bound, upper_bound))

            mu = zfit.Parameter("mu"+obs_name, 0, -10, 10)
            sigma1 = zfit.Parameter("sigma1"+obs_name, 1, 0, 1000)
            alpha1 = zfit.Parameter("alpha1"+obs_name, 1, 0, 1000)
            n1 = zfit.Parameter("n1"+obs_name, 1, 0, 10000000)
            cb1 = zfit.pdf.CrystalBall(obs=obs, mu=mu, sigma=sigma1, alpha=alpha1, n=n1)

            #sigma2 = zfit.Parameter("sigma2"+obs_name, 1, 0, 1000000)
            alpha2 = zfit.Parameter("alpha2"+obs_name, 1, 0,100)
            n2 = zfit.Parameter("n2"+obs_name, 1, 0, 100)

            model = zfit.pdf.DoubleCB(obs=obs, mu=mu, sigma=sigma1, alphal=alpha1, nl=n1, alphar=alpha2, nr=n2)

            nll = zfit.loss.UnbinnedNLL(model=model, data=binned_data)
            minimizer = zfit.minimize.Minuit(tol=1e-6)
            result = minimizer.minimize(nll)

            print(result)

            x = np.linspace(lower_bound, upper_bound, 1000)
            y = model.pdf(x)

            val_mu = mu.numpy()
            val_sigma = sigma1.numpy()
            val_alpha_l = alpha1.numpy()
            val_n_l = n1.numpy()
            val_alpha_r = alpha2.numpy()
            val_n_r = n2.numpy()
            #Add to textbox the fitted values
            #Check if the string contains the word Std
            if "Std" in obs_name:
                textstr = '\n'.join((
                    r"Standard:",
                    r'$\mu=%.2e$' % (val_mu, ),
                    r'$\sigma=%.2e$' % (val_sigma, ),
                    r'$\alpha_l=%.2e$' % (val_alpha_l, ),
                    r'$n_l=%.2e$' % (val_n_l, ),
                    r'$\alpha_r=%.2e$' % (val_alpha_r, ),
                    r'$n_r=%.2e$' % (val_n_r, )))
                ax.text(0.05, 1, textstr, color="red", fontsize=14, transform=ax.transAxes)
            else:
                textstr = '\n'.join((
                    r'CNN:',
                    r'$\mu=%.2e$' % (val_mu, ),
                    r'$\sigma=%.2e$' % (val_sigma, ),
                    r'$\alpha_l=%.2e$' % (val_alpha_l, ),
                    r'$n_l=%.2e$' % (val_n_l, ),
                    r'$\alpha_r=%.2e$' % (val_alpha_r, ),
                    r'$n_r=%.2e$' % (val_n_r, )))
                ax.text(0.75, 1, textstr, color="black", fontsize=14, transform=ax.transAxes)
            
        
            return x, y, val_sigma
        
        # Histogram for x-coordinate
        h_x = Hist.new.Reg(N_BINS, LOWER_BOUND, UPPER_BOUND, name="CNN-Truth X").Double()
        h_x.fill(angle_cnn_x_diff_truth)#, weight=1/len(angle_cnn_x_diff_truth))
        if FIT_PEAK:
            x,y,sigma = perform_fit(h_x, "CNN-Truth X", LOWER_BOUND, UPPER_BOUND, axs[0, 0])
            axs[0, 0].plot(x, y, label="Double CB $\sigma=$"+str(np.round(sigma,2)), color="cyan", linestyle="--", linewidth=2)


        h_x.plot(ax=axs[0, 0], label=LABEL_CNN, color=COLOR_CNN, linestyle=LS_CNN, density=True)
        h_x_bgo = Hist.new.Reg(N_BINS, LOWER_BOUND, UPPER_BOUND, name="BGO-Truth X").Double()
        h_x_bgo.fill(angle_bgo_x_diff_truth, weight=1/len(angle_bgo_x_diff_truth))
        h_x_bgo.plot(ax=axs[0, 0], label=LABEL_BGO, color=COLOR_BGO, linestyle=LS_BGO, density=False)
        h_x_best_track = Hist.new.Reg(N_BINS, LOWER_BOUND, UPPER_BOUND, name="Std-Truth X").Double()
        h_x_best_track.fill(angle_best_track_x_diff_truth)

        if FIT_PEAK:
            x,y,sigma,  = perform_fit(h_x_best_track, "Std-Truth X", LOWER_BOUND, UPPER_BOUND, axs[0, 0])
            axs[0, 0].plot(x, y, label="Double CB $\sigma=$"+str(np.round(sigma,2)), color="orange", linestyle="--", linewidth=2)
        h_x_best_track.plot(ax=axs[0, 0], label=LABEL_BEST, color=COLOR_BEST, linestyle=LS_BEST, density=True)
        axs[0, 0].grid(True)
        axs[0, 0].legend(loc="lower right")
        axs[0, 0].set_xlabel("$\Delta \Theta_x [°]$", fontsize=AXIS_LABEL_SIZE)
        axs[0, 0].set_ylabel("Density", fontsize=AXIS_LABEL_SIZE)


        # Histogram for y-coordinate
        h_y = Hist.new.Reg(N_BINS, LOWER_BOUND, UPPER_BOUND, name="CNN-Truth Y").Double()
        h_y.fill(angle_cnn_y_diff_truth)
        h_y.plot(ax=axs[0, 1], label=LABEL_CNN, color=COLOR_CNN, linestyle=LS_CNN, density=True)
        
        if FIT_PEAK:
            x,y,sigma = perform_fit(h_y, "CNN-Truth Y", LOWER_BOUND, UPPER_BOUND, axs[0, 1])
            axs[0, 1].plot(x, y, label="Double CB $\sigma=$"+str(np.round(sigma,2)), color="cyan", linestyle="--", linewidth=2)

        h_y_bgo = Hist.new.Reg(N_BINS, LOWER_BOUND, UPPER_BOUND, name="BGO-Truth Y").Double()
        h_y_bgo.fill(angle_bgo_y_diff_truth, weight=1/len(angle_bgo_y_diff_truth))
        h_y_bgo.plot(ax=axs[0, 1], label=LABEL_BGO, color=COLOR_BGO, linestyle=LS_BGO, density=False)
        h_y_best_track = Hist.new.Reg(N_BINS, LOWER_BOUND, UPPER_BOUND, name="Std-Truth Y").Double()
        h_y_best_track.fill(angle_best_track_y_diff_truth)
        h_y_best_track.plot(ax=axs[0, 1], label=LABEL_BEST, color=COLOR_BEST, linestyle=LS_BEST, density=True)

        if FIT_PEAK:
            x,y,sigma = perform_fit(h_y_best_track, "Std-Truth Y", LOWER_BOUND, UPPER_BOUND, axs[0, 1])
            axs[0, 1].plot(x, y, label="Double CB $\sigma=$"+str(np.round(sigma,2)), color="orange", linestyle="--", linewidth=2)
        axs[0, 1].grid(True)
        axs[0, 1].legend(loc="lower right")
        axs[0, 1].set_xlabel("$\Delta \Theta_y [°]$", fontsize=AXIS_LABEL_SIZE)
        axs[0, 1].set_ylabel("Density", fontsize=AXIS_LABEL_SIZE)

        N_BINS = 50
        LOWER_BOUND = -5
        UPPER_BOUND = 5

        h_1_x = Hist.new.Reg(N_BINS, LOWER_BOUND, UPPER_BOUND, name="CNN-Truth X1").Double()
        h_1_x.fill(cnn_offset_x1)
        h_1_x.plot(ax=axs[1, 0], label=LABEL_CNN, color=COLOR_CNN, linestyle=LS_CNN, density=True, histtype='step')
        if FIT_PEAK:
            x,y,sigma = perform_fit(h_1_x, "CNN-Truth X1", LOWER_BOUND, UPPER_BOUND, axs[1, 0])
            axs[1, 0].plot(x, y, label="Double CB $\sigma=$"+str(np.round(sigma,2)), color="cyan", linestyle="--", linewidth=2)

        h_1_x_bgo = Hist.new.Reg(N_BINS, LOWER_BOUND, UPPER_BOUND, name="BGO-Truth X1").Double()
        h_1_x_bgo.fill(bgo_offset_x1, weight=1/len(bgo_offset_x1))
        h_1_x_bgo.plot(ax=axs[1, 0], label=LABEL_BGO, color=COLOR_BGO, linestyle=LS_BGO, density=False, histtype='step')
        h_1_x_best_track = Hist.new.Reg(N_BINS, LOWER_BOUND, UPPER_BOUND, name="Std-Truth X1").Double()
        h_1_x_best_track.fill(best_track_offset_x1, weight=1/len(best_track_offset_x1))
        h_1_x_best_track.plot(ax=axs[1, 0], label=LABEL_BEST, color=COLOR_BEST, linestyle=LS_BEST, density=True, histtype='step')
        if FIT_PEAK:
            x,y,sigma = perform_fit(h_1_x_best_track, "Std-Truth X1", LOWER_BOUND, UPPER_BOUND, axs[1, 0])
            axs[1, 0].plot(x, y, label="Double CB $\sigma=$"+str(np.round(sigma,2)), color="orange", linestyle="--", linewidth=2)

        axs[1, 0].grid(True)
        axs[1, 0].legend(loc="lower right")
        axs[1, 0].set_xlabel("$\Delta x_{\mathrm{PSD-top}} $[mm]", fontsize=AXIS_LABEL_SIZE)
        axs[1, 0].set_ylabel("Density", fontsize=AXIS_LABEL_SIZE)
        

        h_1_y = Hist.new.Reg(N_BINS, LOWER_BOUND, UPPER_BOUND, name="CNN-Truth Y1").Double()
        h_1_y.fill(cnn_offset_y1)
        h_1_y.plot(ax=axs[1, 1], label=LABEL_CNN, color=COLOR_CNN, linestyle=LS_CNN, density=True, histtype='step')
        if FIT_PEAK:
            x,y,sigma = perform_fit(h_1_y, "CNN-Truth Y1", LOWER_BOUND, UPPER_BOUND, axs[1, 1])
            axs[1, 1].plot(x, y, label="Double CB $\sigma=$"+str(np.round(sigma,2)), color="cyan", linestyle="--", linewidth=2)
        h_1_y_bgo = Hist.new.Reg(N_BINS, LOWER_BOUND, UPPER_BOUND, name="BGO-Truth Y1").Double()
        h_1_y_bgo.fill(bgo_offset_y1, weight=1/len(bgo_offset_y1))
        h_1_y_bgo.plot(ax=axs[1, 1], label=LABEL_BGO, color=COLOR_BGO, linestyle=LS_BGO, density=False, histtype='step')
        h_1_y_best_track = Hist.new.Reg(N_BINS, LOWER_BOUND, UPPER_BOUND, name="Std-Truth Y1").Double()
        h_1_y_best_track.fill(best_track_offset_y1)
        h_1_y_best_track.plot(ax=axs[1, 1], label=LABEL_BEST, color=COLOR_BEST, linestyle=LS_BEST, density=True, histtype='step')
        if FIT_PEAK:
            x,y,sigma = perform_fit(h_1_y_best_track, "Std-Truth Y1", LOWER_BOUND, UPPER_BOUND, axs[1, 1])
            axs[1, 1].plot(x, y, label="Double CB $\sigma=$"+str(np.round(sigma,2)), color="orange", linestyle="--", linewidth=2)
        axs[1, 1].grid(True)
        axs[1, 1].legend(loc='lower right')
        axs[1, 1].set_xlabel("$\Delta y_{\mathrm{PSD-top}} $[mm]", fontsize=AXIS_LABEL_SIZE)
        axs[1, 1].set_ylabel("Density", fontsize=AXIS_LABEL_SIZE)

        if SET_Y_LOG:
            axs[0, 0].set_yscale("log")
            axs[0, 1].set_yscale("log")
            axs[1, 0].set_yscale("log")
            axs[1, 1].set_yscale("log")
            axs[0, 0].set_xlim(-3, 3)
            axs[0, 1].set_xlim(-3, 3)
            axs[1, 0].set_xlim(LOWER_BOUND, UPPER_BOUND)
            axs[1, 1].set_xlim(LOWER_BOUND, UPPER_BOUND)
        #fig.suptitle(CNN_MODEL_NAME, fontsize=16)
        plt.tight_layout()
        print("Wrinting to file: {}".format(file_name + "_residuals.png"))
        plt.savefig(file_name + "_residuals.png")
        plt.savefig(file_name + "_residuals.pdf")
        plt.close()


        if FIT_PEAK:
            exit()
        ## Analysis as function of the energy ##
        ## Compute the 68% percentils of the containments 
        LABEL_BEST = "Std reco"
        #LABEL_CNN = "CNN"
        LABEL_BGO = "BGO only reco"
        COLOR_BEST = "red"
        #COLOR_CNN = "red"
        COLOR_BGO = "green"
        AXIS_LABEL_SIZE = 14
        SET_Y_LOG = True

        # Get quantiles of true energy
        quantiles_true_energy = np.nanquantile(t_energy, [0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3 ,0.35, 0.4, 0.45, 0.5, 0.55, 0.6 ,0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1])
        binned_cnn_diff = []
        binned_track_diff = []
        for i in range(len(quantiles_true_energy)-1):
            indices = np.where((t_energy >= quantiles_true_energy[i]) & (t_energy < quantiles_true_energy[i+1]))
            binned_cnn_diff.append(np.sqrt(angle_cnn_x_diff_truth[indices]**2 + angle_cnn_y_diff_truth[indices]**2))
            indices = np.where((t_energy_best_track >= quantiles_true_energy[i]) & (t_energy_best_track < quantiles_true_energy[i+1]))
            binned_track_diff.append(np.sqrt(angle_best_track_x_diff_truth[indices]**2 + angle_best_track_y_diff_truth[indices]**2))
        # Find the 68% containment and 96% containment for each bin
        binned_cnn_68 = []
        binned_cnn_96 = []
        binned_track_68 = []
        binned_track_96 = []
        for i in range(len(binned_cnn_diff)):
            binned_cnn_68.append(np.nanquantile(binned_cnn_diff[i], [0.68]))
            binned_cnn_96.append(np.nanquantile(binned_cnn_diff[i], [0.96]))
            binned_track_68.append(np.nanquantile(binned_track_diff[i], [0.68]))
            binned_track_96.append(np.nanquantile(binned_track_diff[i], [0.96]))
        # Plot the 68% and 96% containment as a function of the true energy
        fig, ax1 = plt.subplots(1, 1, figsize=(10, 5))
        ax1.plot(quantiles_true_energy[:-1], binned_cnn_68, label=LABEL_CNN + " 68% containment", color=COLOR_CNN, ls="-.")
        ax1.plot(quantiles_true_energy[:-1], binned_track_68, label=LABEL_BEST + " 68% containment", color=COLOR_BEST, ls="-.")
        ax1.plot(quantiles_true_energy[:-1], binned_cnn_96, label=LABEL_CNN + " 96% containment", color=COLOR_CNN, ls="-")
        ax1.plot(quantiles_true_energy[:-1], binned_track_96, label=LABEL_BEST + " 96% containment", color=COLOR_BEST, ls="-")
        ax1.set_xscale("log")
        ax1.set_xlabel("$E_{\mathrm{true}}$ [MeV]", fontsize=AXIS_LABEL_SIZE)
        ax1.set_ylabel("Containment [°]", fontsize=AXIS_LABEL_SIZE)
        ax1.grid(True)
        ax1.grid(which='minor', linestyle=':', linewidth='0.5', color='black')
        ax1.legend(loc="lower right")
        if SET_Y_LOG:
            ax1.set_yscale("log")
        fig.suptitle(CNN_MODEL_NAME, fontsize=16)
        plt.tight_layout()
        fig.savefig(file_name + "_angle_containment_vs_energy.png")
        fig.savefig(file_name + "_angle_containment_vs_energy.pdf")    
        plt.close()

        #Make the same plot but of the containment of the x1 and y1 coordinates
        binned_cnn_diff_1 = []
        binned_track_diff_1 = []
        for i in range(len(quantiles_true_energy)-1):
            indices = np.where((t_energy >= quantiles_true_energy[i]) & (t_energy < quantiles_true_energy[i+1]))
            binned_cnn_diff_1.append(np.sqrt((cnn_offset_x1[indices])**2 + (cnn_offset_y1[indices])**2))
            indices = np.where((t_energy_best_track >= quantiles_true_energy[i]) & (t_energy_best_track < quantiles_true_energy[i+1]))
            binned_track_diff_1.append(np.sqrt((best_track_offset_x1[indices])**2 + (best_track_offset_y1[indices])**2))
        # Find the 68% containment and 96% containment for each bin
        binned_cnn_68_1 = []
        binned_cnn_96_1 = []
        binned_track_68_1 = []
        binned_track_96_1 = []
        for i in range(len(binned_cnn_diff_1)):
            binned_cnn_68_1.append(np.nanquantile(binned_cnn_diff_1[i], [0.68]))
            binned_cnn_96_1.append(np.nanquantile(binned_cnn_diff_1[i], [0.96]))
            binned_track_68_1.append(np.nanquantile(binned_track_diff_1[i], [0.68]))
            binned_track_96_1.append(np.nanquantile(binned_track_diff_1[i], [0.96]))
        # Plot the 68% and 96% containment as a function of the true energy
        fig, ax1 = plt.subplots(1, 1, figsize=(10, 5))
        ax1.plot(quantiles_true_energy[:-1], binned_cnn_68_1, label=LABEL_CNN + " 68% containment", color=COLOR_CNN, ls="-.")
        ax1.plot(quantiles_true_energy[:-1], binned_track_68_1, label=LABEL_BEST + " 68% containment", color=COLOR_BEST, ls="-.")
        ax1.plot(quantiles_true_energy[:-1], binned_cnn_96_1, label=LABEL_CNN + " 96% containment", color=COLOR_CNN, ls="-")
        ax1.plot(quantiles_true_energy[:-1], binned_track_96_1, label=LABEL_BEST + " 96% containment", color=COLOR_BEST, ls="-")
        ax1.set_xscale("log")
        ax1.set_xlabel("$E_{\mathrm{true}}$ [MeV]", fontsize=AXIS_LABEL_SIZE)
        ax1.set_ylabel("Containment [mm]", fontsize=AXIS_LABEL_SIZE)
        ax1.grid(True)
        ax1.grid(which='minor', linestyle=':', linewidth='0.5', color='black')
        ax1.legend(loc="lower right")
        if SET_Y_LOG:
            ax1.set_yscale("log")
        fig.suptitle(CNN_MODEL_NAME, fontsize=16)
        plt.tight_layout()
        fig.savefig(file_name + "_offset_containment_vs_energy.png")

        
if __name__ == "__main__":
    main()