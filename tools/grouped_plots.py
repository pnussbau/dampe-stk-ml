import numpy as np
import matplotlib.pyplot as plt
from hist import Hist
import os



DATA_INPUT_PATH = "/home/parzival/EPFL/PDM/parsed/inference_mc/"
#Output path for the plots 

def main():
    #np.savez(DATA_OUT_PATH + folder_name + "_angle_offset_energy.npz", cnn_model_name=folder_name, cnn_model_name_display=CNN_MODEL_NAME, angle_cnn_x_diff_truth=angle_cnn_x_diff_truth, angle_bgo_x_diff_truth=angle_bgo_x_diff_truth, angle_best_track_x_diff_truth=angle_best_track_x_diff_truth, angle_cnn_y_diff_truth=angle_cnn_y_diff_truth, angle_bgo_y_diff_truth=angle_bgo_y_diff_truth, angle_best_track_y_diff_truth=angle_best_track_y_diff_truth, cnn_offset_x1=cnn_offset_x1, bgo_offset_x1=bgo_offset_x1, best_track_offset_x1=best_track_offset_x1, cnn_offset_y1=cnn_offset_y1, bgo_offset_y1=bgo_offset_y1, best_track_offset_y1=best_track_offset_y1, t_energy=t_energy, t_energy_best_track=t_energy_best_track, angle_cnn_x_diff_best_track=angle_cnn_x_diff_best_track, angle_cnn_y_diff_best_track=angle_cnn_y_diff_best_track, cnn_offset_x1_best_track=cnn_offset_x1_best_track, cnn_offset_y1_best_track=cnn_offset_y1_best_track)
    relevant_files = ["800um", "500um", "100um", "050um"]# "500um-500um", "500um-100um", "500um-050um"]
    #relevant_files = relevant_files[::-1]
    #relevant_files = ["800um", "800um-050um", "500um", "500um-050um"]
    COMBINED = False
    PLOT_96_PERCENTILE = False

    OUT_FILE_NAME="gfx2/combined"
    if not COMBINED:
        OUT_FILE_NAME="gfx2/standalone"
    files = []
    #Read all the files in the directory, keep all with "_angle_offset_energy.npz"
    for file in os.listdir(DATA_INPUT_PATH):
        if file.endswith("_angle_offset_energy.npz"):
            files.append(file)
    print(files)
            
    #Read the data store it in a dictionary using cnn_model_name as key
    data = {}
    for file in files:
        #Check if cnn_model_name is in the relevant_files
        for relevant_file in relevant_files:
            if relevant_file + "_angle_offset_energy.npz" == os.path.basename(file):
                print("Loading file: " + file)
                data[relevant_file] = np.load(DATA_INPUT_PATH + file)
                break
    #Get from the first dictionary the data for bgo and best track as they are the same for all
    angle_bgo_x_diff_truth = data[relevant_files[0]]["angle_bgo_x_diff_truth"]
    angle_bgo_y_diff_truth = data[relevant_files[0]]["angle_bgo_y_diff_truth"]
    angle_best_track_x_diff_truth = data[relevant_files[0]]["angle_best_track_x_diff_truth"]
    angle_best_track_y_diff_truth = data[relevant_files[0]]["angle_best_track_y_diff_truth"]
    bgo_offset_x1 = data[relevant_files[0]]["bgo_offset_x1"]
    bgo_offset_y1 = data[relevant_files[0]]["bgo_offset_y1"]
    best_track_offset_x1 = data[relevant_files[0]]["best_track_offset_x1"]
    best_track_offset_y1 = data[relevant_files[0]]["best_track_offset_y1"]
    t_energy = data[relevant_files[0]]["t_energy"]
    t_energy_best_track = data[relevant_files[0]]["t_energy_best_track"]

    #Sort the dictionary so that first the lowest resolution is plotted, then lowest + second model
    data = dict(sorted(data.items(), key=lambda item: item[0]))
 

    #Plottin
    color_list = ["gray", "pink", "black", "cyan", "olive", "orange"]
    marker_list = ["o", "v", "s", "D", "P", "X", "d", "p", "x", "h"]
    if COMBINED:
        color_list = color_list[::-1]
        marker_list = marker_list[::-1]
    COLORS={}
    MARKERS={}
    COLORS["800um"] = "gray"
    MARKERS["800um"] = "x"
    COLORS["500um"] = "pink"
    MARKERS["500um"] = "+"
    COLORS["800um-050um"] = "black"
    MARKERS["800um-050um"] = '1'
    COLORS["500um-050um"] = "orange"
    MARKERS["500um-050um"] = '2'
    COLORS["100um"] = "olive"
    MARKERS["100um"] = "P"
    COLORS["050um"] = "cyan"
    MARKERS["050um"] = "X"

    fig, axs = plt.subplots(2, 2, figsize=(10, 10))

    N_BINS_ANGLE = 50
    LOWER_BOUND_ANGLE = -10
    UPPER_BOUND_ANGLE = 10
    N_BINS_DISTANCE = 50
    LOWER_BOUND_DISTANCE = -20
    UPPER_BOUND_DISTANCE = 20
    LABEL_BEST = "Std reco"
    LABEL_CNN = "CNN"
    LABEL_BGO = "BGO only reco"
    LS_BEST = "-"
    TICK_SIZE = 13
    #LS_CNN = "--"
    LS_BGO = "--"
    COLOR_BEST = "red"
    #COLOR_CNN = "red"
    COLOR_BGO = "blue"
    AXIS_LABEL_SIZE = 15
    SET_Y_LOG = True

    # Histogram for x-coordinate
    #h_x = Hist.new.Reg(N_BINS, LOWER_BOUND, UPPER_BOUND, name="CNN-Truth X").Double()
    #h_x.fill(angle_cnn_x_diff_truth, weight=1/len(angle_cnn_x_diff_truth))
    #h_x.plot(ax=axs[0, 0], label="CNN-Truth", color=COLOR_CNN, linestyle=LS_CNN, density=False)
    h_x_bgo = Hist.new.Reg(N_BINS_ANGLE, LOWER_BOUND_ANGLE, UPPER_BOUND_ANGLE, name="BGO-Truth X").Double()
    h_x_bgo.fill(angle_bgo_x_diff_truth, weight=1/len(angle_bgo_x_diff_truth))
    h_x_best_track = Hist.new.Reg(N_BINS_ANGLE, LOWER_BOUND_ANGLE, UPPER_BOUND_ANGLE, name="Std-Truth X").Double()
    h_x_best_track.fill(angle_best_track_x_diff_truth, weight=1/len(angle_best_track_x_diff_truth),)
    h_x_best_track.plot(ax=axs[0, 0], label=LABEL_BEST, color=COLOR_BEST, linestyle=LS_BEST, density=False )
    h_x_bgo.plot(ax=axs[0, 0], label=LABEL_BGO, color=COLOR_BGO, linestyle=LS_BGO, density=False )
    axs[0, 0].grid(True)
    #axs[0, 0].legend()
    axs[0, 0].set_xlabel("$\Delta \Theta_x [°]$", fontsize=AXIS_LABEL_SIZE)
    axs[0, 0].set_ylabel("Fraction of Events", fontsize=AXIS_LABEL_SIZE)
    #axs[0, 0].text(0.05, 1.1, "CNN Entries: {} Mean: {:.2f} Std: {:.2f} \n Best: Entries: {} Mean: {:.2f} Std: {:.2f}".format(np.sum(~np.isnan(angle_cnn_x_diff_truth)), np.mean(angle_cnn_x_diff_truth), np.std(angle_cnn_x_diff_truth), np.sum(~np.isnan(angle_best_track_x_diff_truth)), np.mean(angle_best_track_x_diff_truth), np.std(angle_best_track_x_diff_truth)), transform=axs[0, 0].transAxes, fontsize=10, verticalalignment='top')

    # Histogram for y-coordinate
    #h_y = Hist.new.Reg(N_BINS, LOWER_BOUND, UPPER_BOUND, name="LABEL_CNN Y").Double()
    #h_y.fill(angle_cnn_y_diff_truth, weight=1/len(angle_cnn_y_diff_truth))
    #h_y.plot(ax=axs[0, 1], label=LABEL_CNN, color=COLOR_CNN, linestyle=LS_CNN, density=False)
    h_y_bgo = Hist.new.Reg(N_BINS_ANGLE, LOWER_BOUND_ANGLE, UPPER_BOUND_ANGLE, name="BGO-Truth Y").Double()
    h_y_bgo.fill(angle_bgo_y_diff_truth, weight=1/len(angle_bgo_y_diff_truth))
    h_y_best_track = Hist.new.Reg(N_BINS_ANGLE, LOWER_BOUND_ANGLE, UPPER_BOUND_ANGLE, name="Std-Truth Y").Double()
    h_y_best_track.fill(angle_best_track_y_diff_truth, weight=1/len(angle_best_track_y_diff_truth))
    h_y_best_track.plot(ax=axs[0, 1], label=LABEL_BEST, color=COLOR_BEST, linestyle=LS_BEST, density=False )
    h_y_bgo.plot(ax=axs[0, 1], label=LABEL_BGO, color=COLOR_BGO, linestyle=LS_BGO, density=False )

    axs[0, 1].grid(True)
    axs[0, 1].set_xlabel("$\Delta \Theta_y [°]$", fontsize=AXIS_LABEL_SIZE)
    axs[0, 1].set_ylabel("Fraction of Events", fontsize=AXIS_LABEL_SIZE)
    #axs[0, 1].text(0.05, 1.1, "CNN Entries: {} Mean: {:.2f} Std: {:.2f} \n Best: Entries: {} Mean: {:.2f} Std: {:.2f}".format(np.sum(~np.isnan(angle_cnn_y_diff_truth)), np.mean(angle_cnn_y_diff_truth), np.std(angle_cnn_y_diff_truth), np.sum(~np.isnan(angle_best_track_y_diff_truth)), np.mean(angle_best_track_y_diff_truth), np.std(angle_best_track_y_diff_truth)), transform=axs[0, 1].transAxes, fontsize=10, verticalalignment='top')

    for model_name, model_data in data.items():
        h_x_model = Hist.new.Reg(N_BINS_ANGLE, LOWER_BOUND_ANGLE, UPPER_BOUND_ANGLE, name=model_name).Double()
        h_x_model.fill(model_data["angle_cnn_x_diff_truth"], weight=1/len(model_data["angle_cnn_x_diff_truth"]))
        h_x_model.plot(ax=axs[0, 0], label=str(model_data["cnn_model_name_display"]).replace("+", "\n+"), color=COLORS[model_name], density=False)
        h_y_model = Hist.new.Reg(N_BINS_ANGLE, LOWER_BOUND_ANGLE, UPPER_BOUND_ANGLE, name=model_name).Double()
        h_y_model.fill(model_data["angle_cnn_y_diff_truth"], weight=1/len(model_data["angle_cnn_y_diff_truth"]))
        h_y_model.plot(ax=axs[0, 1], label=str(model_data["cnn_model_name_display"]).replace("+", "\n+"), color=COLORS[model_name], density=False)
    #axs[0, 0].legend(loc='upper center', bbox_to_anchor=(0.5, -0.2), ncol=4, fontsize=12)

    # Histogram for offset x-coordinate
    #h_offset_x = Hist.new.Reg(N_BINS, LOWER_BOUND, UPPER_BOUND, name="Offset X").Double()
    #h_offset_x.fill(cnn_offset_x1, weight=1/len(cnn_offset_x1))
    #h_offset_x.plot(ax=axs[1, 0], label=LABEL_CNN, color=COLOR_CNN, linestyle=LS_CNN, density=False, histtype='step')
    h_offset_x_bgo = Hist.new.Reg(N_BINS_DISTANCE, LOWER_BOUND_DISTANCE, UPPER_BOUND_DISTANCE, name="Offset BGO").Double()
    h_offset_x_bgo.fill(bgo_offset_x1, weight=1/len(bgo_offset_x1))
    h_offset_x_best_track = Hist.new.Reg(N_BINS_DISTANCE, LOWER_BOUND_DISTANCE, UPPER_BOUND_DISTANCE, name="Offset Std").Double()
    h_offset_x_best_track.fill(best_track_offset_x1, weight=1/len(best_track_offset_x1))
    h_offset_x_best_track.plot(ax=axs[1, 0], label=LABEL_BEST, color=COLOR_BEST, linestyle=LS_BEST, density=False, histtype='step')
    h_offset_x_bgo.plot(ax=axs[1, 0], label=LABEL_BGO, color=COLOR_BGO, linestyle=LS_BGO, density=False, histtype='step')

    for model_name, model_data in data.items():
        h_offset_x_model = Hist.new.Reg(N_BINS_DISTANCE, LOWER_BOUND_DISTANCE, UPPER_BOUND_DISTANCE, name=model_name).Double()
        h_offset_x_model.fill(model_data["cnn_offset_x1"], weight=1/len(model_data["cnn_offset_x1"]))
        h_offset_x_model.plot(ax=axs[1, 0], label=str(model_data["cnn_model_name_display"]).replace("+", "\n+"), color=COLORS[model_name], density=False, histtype='step')
    axs[1, 0].grid(True)
    #axs[1, 0].legend()
    axs[1, 0].set_xlabel("$\Delta x_{\mathrm{PSD\ top}} $[mm]", fontsize=AXIS_LABEL_SIZE)
    axs[1, 0].set_ylabel("Fraction of Events", fontsize=AXIS_LABEL_SIZE)

    # Histogram for offset y-coordinate
    #h_offset_y = Hist.new.Reg(N_BINS, LOWER_BOUND, UPPER_BOUND, name="Offset Y").Double()
    #h_offset_y.fill(cnn_offset_y1, weight=1/len(cnn_offset_y1))
    #h_offset_y.plot(ax=axs[1, 1], label=LABEL_CNN, color=COLOR_CNN, linestyle=LS_CNN, density=False, histtype='step')
    h_offset_y_bgo = Hist.new.Reg(N_BINS_DISTANCE, LOWER_BOUND_DISTANCE, UPPER_BOUND_DISTANCE, name="Offset BGO").Double()
    h_offset_y_bgo.fill(bgo_offset_y1, weight=1/len(bgo_offset_y1))
    h_offset_y_best_track = Hist.new.Reg(N_BINS_DISTANCE, LOWER_BOUND_DISTANCE, UPPER_BOUND_DISTANCE, name="Offset Std").Double()
    h_offset_y_best_track.fill(best_track_offset_y1, weight=1/len(best_track_offset_y1))
    h_offset_y_best_track.plot(ax=axs[1, 1], label=LABEL_BEST, color=COLOR_BEST, linestyle=LS_BEST, density=False, histtype='step')
    h_offset_y_bgo.plot(ax=axs[1, 1], label=LABEL_BGO, color=COLOR_BGO, linestyle=LS_BGO, density=False, histtype='step')
    for model_name, model_data in data.items():
        h_offset_y_model = Hist.new.Reg(N_BINS_DISTANCE, LOWER_BOUND_DISTANCE, UPPER_BOUND_DISTANCE, name=model_name).Double()
        h_offset_y_model.fill(model_data["cnn_offset_y1"], weight=1/len(model_data["cnn_offset_y1"]))
        h_offset_y_model.plot(ax=axs[1, 1], label=str(model_data["cnn_model_name_display"]).replace("+", "\n+"), color=COLORS[model_name], density=False, histtype='step')
    axs[1, 1].grid(True)
    #axs[1, 1].legend(fontsize=TICK_SIZE)
    axs[1, 1].set_xlabel("$\Delta y_{\mathrm{PSD\ top}} $[mm]", fontsize=AXIS_LABEL_SIZE)
    axs[1, 1].set_ylabel("Fraction of Events", fontsize=AXIS_LABEL_SIZE)

    if SET_Y_LOG:
        axs[0, 0].set_yscale("log")
        axs[0, 1].set_yscale("log")
        axs[1, 0].set_yscale("log")
        axs[1, 1].set_yscale("log")

    # Set Tick size
    axs[0, 0].tick_params(axis='both', which='major', labelsize=TICK_SIZE)
    axs[0, 1].tick_params(axis='both', which='major', labelsize=TICK_SIZE)
    axs[1, 0].tick_params(axis='both', which='major', labelsize=TICK_SIZE)
    axs[1, 1].tick_params(axis='both', which='major', labelsize=TICK_SIZE)

    #Set the ranges so that the overfill is not shown
    axs[0, 0].set_xlim(LOWER_BOUND_ANGLE, UPPER_BOUND_ANGLE)
    axs[0, 1].set_xlim(LOWER_BOUND_ANGLE, UPPER_BOUND_ANGLE)
    axs[1, 0].set_xlim(LOWER_BOUND_DISTANCE, UPPER_BOUND_DISTANCE)
    axs[1, 1].set_xlim(LOWER_BOUND_DISTANCE, UPPER_BOUND_DISTANCE)
    

    # Set legend using handles from one of the subfigures
    #handles, labels = axs[1, 1].get_legend_handles_labels()
    #Position the legend outside the plot
    axs[1,1].legend(loc='center left', bbox_to_anchor=(.75, 0.75), fontsize=TICK_SIZE)

    
    plt.tight_layout()
    print("Wrinting to file: {}".format(OUT_FILE_NAME + "_residuals.png"))
    plt.savefig(OUT_FILE_NAME + "_residuals.png")
    plt.savefig(OUT_FILE_NAME + "_residuals.pdf")
    plt.close()

  
    
    ## Analysis as function of the energy ##
    ## Compute the 68% percentils of the containments 
    LABEL_BEST = "Std reco"
    LABEL_CNN = "CNN"
    LABEL_BGO = "BGO only reco"
    COLOR_BEST = "red"
    COLOR_CNN = "green"
    COLOR_BGO = "blue"
    SET_Y_LOG = False

    # Get quantiles of true energy
    quantiles_true_energy = np.nanquantile(t_energy, [0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3 ,0.35, 0.4, 0.45, 0.5, 0.55, 0.6 ,0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1])
    binned_cnn_diff =  {}
    binned_track_diff = []
    for name in relevant_files:
        binned_cnn_diff[name] = []

    for i in range(len(quantiles_true_energy)-1):
        indices = np.where((t_energy >= quantiles_true_energy[i]) & (t_energy < quantiles_true_energy[i+1]))
        for model_name, model_data in data.items():
            binned_cnn_diff[model_name].append(np.sqrt(model_data["angle_cnn_x_diff_truth"][indices]**2 + model_data["angle_cnn_y_diff_truth"][indices]**2))
        indices = np.where((t_energy_best_track >= quantiles_true_energy[i]) & (t_energy_best_track < quantiles_true_energy[i+1]))
        binned_track_diff.append(np.sqrt(angle_best_track_x_diff_truth[indices]**2 + angle_best_track_y_diff_truth[indices]**2))
    # Find the 68% containment and 96% containment for each bin
    binned_cnn_68 = {}
    binned_cnn_96 = {}
    for name in relevant_files:
        binned_cnn_68[name] = []
        binned_cnn_96[name] = []
    binned_track_68 = []
    binned_track_96 = []
    for i in range(len(binned_track_diff)):
        for name in relevant_files:
            binned_cnn_68[name].append(np.nanquantile(binned_cnn_diff[name][i], [0.68]))
            binned_cnn_96[name].append(np.nanquantile(binned_cnn_diff[name][i], [0.96]))
        #binned_cnn_68.append(np.nanquantile(binned_cnn_diff[i], [0.68]))
        #binned_cnn_96.append(np.nanquantile(binned_cnn_diff[i], [0.96]))
        binned_track_68.append(np.nanquantile(binned_track_diff[i], [0.68]))
        binned_track_96.append(np.nanquantile(binned_track_diff[i], [0.96]))
    # Plot the 68% and 96% containment as a function of the true energy
    fig, ax1 = plt.subplots(1, 1, figsize=(10, 5))
    for model_name, model_data in data.items():
        base_label = str(model_data["cnn_model_name_display"]).replace("+", "\n+")
        if PLOT_96_PERCENTILE:
            ax1.plot(quantiles_true_energy[:-1], binned_cnn_68[model_name], label=base_label+ " 68% ", color=COLORS[model_name], marker=MARKERS[model_name], ls="-")
            ax1.plot(quantiles_true_energy[:-1], binned_cnn_96[model_name], label=base_label+ " 96% ", color=COLORS[model_name], marker=MARKERS[model_name], ls="-.")
        else:
            ax1.plot(quantiles_true_energy[:-1], binned_cnn_68[model_name], label=base_label, color=COLORS[model_name], marker=MARKERS[model_name], ls="-")
    if PLOT_96_PERCENTILE:
        ax1.plot(quantiles_true_energy[:-1], binned_track_68, label=LABEL_BEST + " 68%", color=COLOR_BEST, ls="-", marker="X")
        ax1.plot(quantiles_true_energy[:-1], binned_track_96, label=LABEL_BEST + " 96%", color=COLOR_BEST, ls="-.", marker="X")
    else:
        ax1.plot(quantiles_true_energy[:-1], binned_track_68, label=LABEL_BEST, color=COLOR_BEST, ls="-", marker="X")
    ax1.set_xscale("log")
    ax1.set_xlabel("$E_{\mathrm{true}}$ [MeV]", fontsize=AXIS_LABEL_SIZE)
    if PLOT_96_PERCENTILE:
        ax1.set_ylabel("Containment $\sqrt{\Delta \Theta_x^2 + \Delta \Theta_y^2 }$ [°]", fontsize=AXIS_LABEL_SIZE)
    else:
        ax1.set_ylabel("68% Containment $\sqrt{\Delta \Theta_x^2 + \Delta \Theta_y^2 }$ [°]", fontsize=AXIS_LABEL_SIZE)
    ax1.grid(True)
    ax1.grid(which='minor', linestyle=':', linewidth='0.5', color='black')
    #Set the legend to the right of the plot
    ax1.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    if SET_Y_LOG:
        ax1.set_yscale("log")
    ax1.tick_params(axis='both', which='major', labelsize=TICK_SIZE)
    #fig.suptitle(CNN_MODEL_NAME, fontsize=16)
    plt.tight_layout()
    print("Wrinting to file: {}".format(OUT_FILE_NAME + "_angle_containment_vs_energy.png"))
    fig.savefig(OUT_FILE_NAME + "_angle_containment_vs_energy.png")
    fig.savefig(OUT_FILE_NAME + "_angle_containment_vs_energy.pdf")
    plt.close()


    #Make the same plot but of the containment of the x1 and y1 coordinates
    binned_cnn_diff_1 = {}
    binned_track_diff_1 = []
    for name in relevant_files:
        binned_cnn_diff_1[name] = []

    for i in range(len(quantiles_true_energy)-1):
        indices = np.where((t_energy >= quantiles_true_energy[i]) & (t_energy < quantiles_true_energy[i+1]))
        for model_name, model_data in data.items():
            binned_cnn_diff_1[model_name].append(np.sqrt(model_data["cnn_offset_x1"][indices]**2 + model_data["cnn_offset_y1"][indices]**2))
        indices = np.where((t_energy_best_track >= quantiles_true_energy[i]) & (t_energy_best_track < quantiles_true_energy[i+1]))
        binned_track_diff_1.append(np.sqrt(best_track_offset_x1[indices]**2 + best_track_offset_y1[indices]**2))
    # Find the 68% containment and 96% containment for each bin
    binned_cnn_68_1 = {}
    binned_cnn_96_1 = {}
    for name in relevant_files:
        binned_cnn_68_1[name] = []
        binned_cnn_96_1[name] = []
    binned_track_68_1 = []
    binned_track_96_1 = []
    for i in range(len(binned_track_diff_1)):
        for name in relevant_files:
            binned_cnn_68_1[name].append(np.nanquantile(binned_cnn_diff_1[name][i], [0.68]))
            binned_cnn_96_1[name].append(np.nanquantile(binned_cnn_diff_1[name][i], [0.96]))
        binned_track_68_1.append(np.nanquantile(binned_track_diff_1[i], [0.68]))
        binned_track_96_1.append(np.nanquantile(binned_track_diff_1[i], [0.96]))
    # Plot the 68% and 96% containment as a function of the true energy
    fig, ax1 = plt.subplots(1, 1, figsize=(10, 5))
    for model_name, model_data in data.items():
        base_label = str(model_data["cnn_model_name_display"]).replace("+", "\n+")
        if PLOT_96_PERCENTILE:
            ax1.plot(quantiles_true_energy[:-1], binned_cnn_68_1[model_name], label=base_label+ " 68% ", color=COLORS[model_name], marker=MARKERS[model_name], ls="-")
            ax1.plot(quantiles_true_energy[:-1], binned_cnn_96_1[model_name], label=base_label + " 96%", color=COLORS[model_name],  marker=MARKERS[model_name], ls="-.")
        else:
            ax1.plot(quantiles_true_energy[:-1], binned_cnn_68_1[model_name], label=base_label, color=COLORS[model_name], marker=MARKERS[model_name], ls="-")
    if PLOT_96_PERCENTILE:
        ax1.plot(quantiles_true_energy[:-1], binned_track_68_1, label=LABEL_BEST + " 68% ", color=COLOR_BEST, ls="-", marker="X")
        ax1.plot(quantiles_true_energy[:-1], binned_track_96_1, label=LABEL_BEST + " 96% ", color=COLOR_BEST, ls="-.", marker="X")
    else:
        ax1.plot(quantiles_true_energy[:-1], binned_track_68_1, label=LABEL_BEST, color=COLOR_BEST, ls="-", marker="X")
    ax1.set_xscale("log")
    ax1.set_xlabel("$E_{\mathrm{true}}$ [MeV]", fontsize=AXIS_LABEL_SIZE)
    if PLOT_96_PERCENTILE:
        ax1.set_ylabel("Containment $\sqrt{\Delta x_{\mathrm{PSD}}^2 + \Delta y_{\mathrm{PSD}}^2 }$ [mm]", fontsize=AXIS_LABEL_SIZE)
    else:
        ax1.set_ylabel("68% Containment $\sqrt{\Delta x_{\mathrm{PSD}}^2 + \Delta y_{\mathrm{PSD}}^2 }$ [mm]", fontsize=AXIS_LABEL_SIZE)
    ax1.grid(True)
    ax1.grid(which='minor', linestyle=':', linewidth='0.5', color='black')
    ax1.tick_params(axis='both', which='major', labelsize=TICK_SIZE)
    #Set the legend to the right of the plot
    ax1.legend(loc='center left', bbox_to_anchor=(1, 0.5), fontsize=TICK_SIZE)
    if SET_Y_LOG:
        ax1.set_yscale("log")
    #fig.suptitle(CNN_MODEL_NAME, fontsize=16)
    plt.tight_layout()
    print("Wrinting to file: {}".format(OUT_FILE_NAME + "_offset_containment_vs_energy.png"))
    fig.savefig(OUT_FILE_NAME + "_offset_containment_vs_energy.png")
    fig.savefig(OUT_FILE_NAME + "_offset_containment_vs_energy.pdf")

    




if "__main__" == __name__:
    main()