import numpy as np
import matplotlib.pyplot as plt
from hist import Hist
import os



DATA_FILE = "/home/parzival/EPFL/PDM/parsed/inference_flight/800um-050um_angle_offset_energy.npz"
MC_FILE = "/home/parzival/EPFL/PDM/parsed/validation_mc/800um-050um_angle_offset_energy.npz"
OUT_FILE_NAME = "/home/parzival/EPFL/PDM/parsed"
def main():
    #Both files contain np.savez(DATA_OUT_PATH + folder_name + "_angle_offset_energy.npz", cnn_model_name=folder_name, cnn_model_name_display=CNN_MODEL_NAME, angle_cnn_x_diff_best_track=angle_cnn_x_diff_best_track, angle_cnn_y_diff_best_track=angle_cnn_y_diff_best_track, cnn_offset_x1_best_track=cnn_offset_x1_best_track, cnn_offset_y1_best_track=cnn_offset_y1_best_track, bgo_energy=bgo_energy)
    data = np.load(DATA_FILE)
    mc = np.load(MC_FILE)


     #Plottin
    color_list = ["green","brown", "gray", "olive", "pink", "purple", "gray", "cyan", "black", "orange"]
 

    fig, axs = plt.subplots(2, 2, figsize=(10, 10))

    N_BINS = 100
    LOWER_BOUND = -3
    UPPER_BOUND = 3
    LABEL_DATA = "Data CNN-Best Track"
    LABEL_MC = "MC CNN-Best Track"
    LS_BEST = "-"
    LS_BGO = "--"
    MARKER_DATA = "x"

    MARKER_MC = "+"
    COLOR_DATA = "red"
    COLOR_MC = "blue"
    AXIS_LABEL_SIZE = 15
    TICK_SIZE = 13

    SET_Y_LOG = False

    # Histogram for x-coordinate
    h_x_data = Hist.new.Reg(N_BINS, LOWER_BOUND, UPPER_BOUND, name="Data angle").Double()
    h_x_data.fill(data["angle_cnn_x_diff_best_track"], weight=1/len(data["angle_cnn_x_diff_best_track"]))
    h_x_mc = Hist.new.Reg(N_BINS, LOWER_BOUND, UPPER_BOUND, name="Data mc").Double()
    h_x_mc.fill(mc["angle_cnn_x_diff_best_track"], weight=1/len(mc["angle_cnn_x_diff_best_track"]))
    h_x_data.plot(ax=axs[0, 0], label=LABEL_DATA, color=COLOR_DATA, linestyle=LS_BEST, density=False)
    h_x_mc.plot(ax=axs[0, 0], label=LABEL_MC, color=COLOR_MC, linestyle=LS_BGO, density=False)

    axs[0, 0].grid(True)
    #axs[0, 0].legend()
    axs[0, 0].set_xlabel("$\Delta \Theta_x [°]$", fontsize=AXIS_LABEL_SIZE)
    axs[0, 0].set_ylabel("Fraction of Events", fontsize=AXIS_LABEL_SIZE)

    h_y_data = Hist.new.Reg(N_BINS, LOWER_BOUND, UPPER_BOUND, name="Data angle").Double()
    h_y_data.fill(data["angle_cnn_y_diff_best_track"], weight=1/len(data["angle_cnn_y_diff_best_track"]))
    h_y_mc = Hist.new.Reg(N_BINS, LOWER_BOUND, UPPER_BOUND, name="Data mc").Double()
    h_y_mc.fill(mc["angle_cnn_y_diff_best_track"], weight=1/len(mc["angle_cnn_y_diff_best_track"]))
    h_y_data.plot(ax=axs[0, 1], label=LABEL_DATA, color=COLOR_DATA, linestyle=LS_BEST, density=False)
    h_y_mc.plot(ax=axs[0, 1], label=LABEL_MC, color=COLOR_MC, linestyle=LS_BGO, density=False)
    axs[0, 1].grid(True)
    #axs[0, 1].legend()
    axs[0, 1].set_xlabel("$\Delta \Theta_y [°]$", fontsize=AXIS_LABEL_SIZE)
    axs[0, 1].set_ylabel("Fraction of Events", fontsize=AXIS_LABEL_SIZE)

    h_offset_x_data = Hist.new.Reg(N_BINS, LOWER_BOUND, UPPER_BOUND, name="Data offset").Double()
    h_offset_x_data.fill(data["cnn_offset_x1_best_track"], weight=1/len(data["cnn_offset_x1_best_track"]))
    h_offset_x_mc = Hist.new.Reg(N_BINS, LOWER_BOUND, UPPER_BOUND, name="Data mc").Double()
    h_offset_x_mc.fill(mc["cnn_offset_x1_best_track"], weight=1/len(mc["cnn_offset_x1_best_track"]))
    h_offset_x_data.plot(ax=axs[1, 0], label=LABEL_DATA, color=COLOR_DATA, linestyle=LS_BEST, density=False, histtype='step')
    h_offset_x_mc.plot(ax=axs[1, 0], label=LABEL_MC, color=COLOR_MC, linestyle=LS_BGO, density=False, histtype='step')
    axs[1, 0].grid(True)
    #axs[1, 0].legend()
    axs[1, 0].set_xlabel("$\Delta x_{\mathrm{PSD\ top}} $[mm]", fontsize=AXIS_LABEL_SIZE)
    axs[1, 0].set_ylabel("Fraction of Events", fontsize=AXIS_LABEL_SIZE)

    h_offset_y_data = Hist.new.Reg(N_BINS, LOWER_BOUND, UPPER_BOUND, name="Data offset").Double()
    h_offset_y_data.fill(data["cnn_offset_y1_best_track"], weight=1/len(data["cnn_offset_y1_best_track"]))
    h_offset_y_mc = Hist.new.Reg(N_BINS, LOWER_BOUND, UPPER_BOUND, name="Data mc").Double()
    h_offset_y_mc.fill(mc["cnn_offset_y1_best_track"], weight=1/len(mc["cnn_offset_y1_best_track"]))
    h_offset_y_data.plot(ax=axs[1, 1], label=LABEL_DATA, color=COLOR_DATA, linestyle=LS_BEST, density=False, histtype='step')
    h_offset_y_mc.plot(ax=axs[1, 1], label=LABEL_MC, color=COLOR_MC, linestyle=LS_BGO, density=False, histtype='step')
    axs[1, 1].grid(True)
    axs[1, 1].legend(fontsize=TICK_SIZE, loc='upper center', bbox_to_anchor=(0.7, 1))
    axs[1, 1].set_xlabel("$\Delta y_{\mathrm{PSD\ top}} $[mm]", fontsize=AXIS_LABEL_SIZE)
    axs[1, 1].set_ylabel("Fraction of Events", fontsize=AXIS_LABEL_SIZE)

    if SET_Y_LOG:
        axs[0, 0].set_yscale("log")
        axs[0, 1].set_yscale("log")
        axs[1, 0].set_yscale("log")
        axs[1, 1].set_yscale("log")

    axs[0, 0].tick_params(axis='both', which='major', labelsize=TICK_SIZE)
    axs[0, 1].tick_params(axis='both', which='major', labelsize=TICK_SIZE)
    axs[1, 0].tick_params(axis='both', which='major', labelsize=TICK_SIZE)
    axs[1, 1].tick_params(axis='both', which='major', labelsize=TICK_SIZE)

    plt.tight_layout()
    print("Writing to file: {}".format(OUT_FILE_NAME + "_residuals.png"))
    plt.savefig(OUT_FILE_NAME + "_residuals.png")
    plt.savefig(OUT_FILE_NAME + "_residuals.pdf")
    plt.close()

  
    
    ## Analysis as function of the energy ##
    ## Compute the 68% percentils of the containments 
    SET_Y_LOG = False

    # Get quantiles of true energy
    quantiles_mc_energy= np.nanquantile(mc["bgo_energy"], [0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3 ,0.35, 0.4, 0.45, 0.5, 0.55, 0.6 ,0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1])
    quantiles_data_energy = np.nanquantile(data["bgo_energy"], [0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3 ,0.35, 0.4, 0.45, 0.5, 0.55, 0.6 ,0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95 ,1])
 
    binned_data_diff = []
    binned_mc_diff = []

    for i in range(len(quantiles_mc_energy)-1):
        indices = np.where((mc["bgo_energy"] >= quantiles_mc_energy[i]) & (mc["bgo_energy"] < quantiles_mc_energy[i+1]))
        binned_mc_diff.append(mc["angle_cnn_x_diff_best_track"][indices])
    for i in range(len(quantiles_data_energy)-1):
        indices = np.where((data["bgo_energy"] >= quantiles_data_energy[i]) & (data["bgo_energy"] < quantiles_data_energy[i+1]))
        binned_data_diff.append(data["angle_cnn_x_diff_best_track"][indices])
    
    # Find the 68% containment and 96% containment for each bin
    binned_data_68 = []
    binned_data_96 = []
    binned_mc_68 = []
    binned_mc_96 = []
    for i in range(len(binned_data_diff)):
        binned_data_68.append(np.nanquantile(binned_data_diff[i], [0.68]))
        binned_data_96.append(np.nanquantile(binned_data_diff[i], [0.96]))
    for i in range(len(binned_mc_diff)):
        binned_mc_68.append(np.nanquantile(binned_mc_diff[i], [0.68]))
        binned_mc_96.append(np.nanquantile(binned_mc_diff[i], [0.96]))


    # Plot the 68% and 96% containment as a function of the true energy
    fig, ax1 = plt.subplots(1, 1, figsize=(10, 5))
   

    ax1.plot(quantiles_mc_energy[:-1], binned_mc_68, label=LABEL_MC + " 68% ", color=COLOR_MC, ls="-", marker=MARKER_MC)
    ax1.plot(quantiles_mc_energy[:-1], binned_mc_96, label=LABEL_MC + " 96%", color=COLOR_MC, ls="-.", marker=MARKER_MC)
    ax1.plot(quantiles_data_energy[:-1], binned_data_68, label=LABEL_DATA + " 68% ", color=COLOR_DATA, ls="-", marker=MARKER_DATA)
    ax1.plot(quantiles_data_energy[:-1], binned_data_96, label=LABEL_DATA + " 96% ", color=COLOR_DATA, ls="-.", marker=MARKER_DATA)
    ax1.set_xscale("log")
    ax1.set_xlabel("$E_{\mathrm{BGO\ total}}$ [GeV]", fontsize=AXIS_LABEL_SIZE)
    ax1.set_ylabel("Containment $\sqrt{\Delta \Theta_x^2 + \Delta \Theta_y^2 }$ [°]", fontsize=AXIS_LABEL_SIZE)
    ax1.grid(True)
    ax1.grid(which='minor', linestyle=':', linewidth='0.5', color='black')
    ax1.legend(fontsize=TICK_SIZE)
    ax1.tick_params(axis='both', which='major', labelsize=TICK_SIZE)
    if SET_Y_LOG:
        ax1.set_yscale("log")
    #fig.suptitle(CNN_MODEL_NAME, fontsize=16)
    plt.tight_layout()
    print("Wrinting to file: {}".format(OUT_FILE_NAME + "_angle_containment_vs_energy.png"))
    fig.savefig(OUT_FILE_NAME + "_angle_containment_vs_energy.png")
    fig.savefig(OUT_FILE_NAME + "_angle_containment_vs_energy.pdf")
    plt.close()


    #Make the same plot but of the containment of the x1 and y1 coordinates
    binned_data_diff_1 =  []
    binned_mc_diff_1 = []
   
    for i in range(len(quantiles_mc_energy)-1):
        indices = np.where((mc["bgo_energy"] >= quantiles_mc_energy[i]) & (mc["bgo_energy"] < quantiles_mc_energy[i+1]))
        binned_mc_diff_1.append(np.sqrt(mc["cnn_offset_x1_best_track"][indices]**2 + mc["cnn_offset_y1_best_track"][indices]**2))
    for i in range(len(quantiles_data_energy)-1):
        indices = np.where((data["bgo_energy"] >= quantiles_data_energy[i]) & (data["bgo_energy"] < quantiles_data_energy[i+1]))
        binned_data_diff_1.append(np.sqrt(data["cnn_offset_x1_best_track"][indices]**2 + data["cnn_offset_y1_best_track"][indices]**2))
    
    binned_data_68_1 = []
    binned_data_96_1 = []
    binned_mc_68_1 = []
    binned_mc_96_1 = []
    
    for i in range(len(binned_data_diff_1)):
        binned_data_68_1.append(np.nanquantile(binned_data_diff_1[i], [0.68]))
        binned_data_96_1.append(np.nanquantile(binned_data_diff_1[i], [0.96]))
    for i in range(len(binned_mc_diff_1)):
        binned_mc_68_1.append(np.nanquantile(binned_mc_diff_1[i], [0.68]))
        binned_mc_96_1.append(np.nanquantile(binned_mc_diff_1[i], [0.96]))
    

    fig, ax1 = plt.subplots(1, 1, figsize=(10, 5))
    ax1.plot(quantiles_mc_energy[:-1], binned_mc_68_1, label=LABEL_MC + " 68% ", color=COLOR_MC, ls="-", marker=MARKER_MC)
    ax1.plot(quantiles_mc_energy[:-1], binned_mc_96_1, label=LABEL_MC + " 96%", color=COLOR_MC, ls="-.", marker=MARKER_MC)
    ax1.plot(quantiles_data_energy[:-1], binned_data_68_1, label=LABEL_DATA + " 68% ", color=COLOR_DATA, ls="-" , marker=MARKER_DATA)
    ax1.plot(quantiles_data_energy[:-1], binned_data_96_1, label=LABEL_DATA + " 96% ", color=COLOR_DATA, ls="-.", marker=MARKER_DATA)
    ax1.set_xscale("log")
    ax1.set_xlabel("$E_{\mathrm{BGO\ total}}$ [GeV]", fontsize=AXIS_LABEL_SIZE)
    ax1.set_ylabel("Containment $\sqrt{\Delta x_{\mathrm{PSD}}^2 + \Delta y_{\mathrm{PSD}}^2 }$ [mm]", fontsize=AXIS_LABEL_SIZE)
    ax1.grid(True)
    ax1.grid(which='minor', linestyle=':', linewidth='0.5', color='black')
    ax1.legend(fontsize=TICK_SIZE)
    ax1.tick_params(axis='both', which='major', labelsize=TICK_SIZE)
    if SET_Y_LOG:
        ax1.set_yscale("log")
    #fig.suptitle(CNN_MODEL_NAME, fontsize=16)
    plt.tight_layout()
    print("Wrinting to file: {}".format(OUT_FILE_NAME + "_offset_containment_vs_energy.png"))
    fig.savefig(OUT_FILE_NAME + "_offset_containment_vs_energy.png")
    fig.savefig(OUT_FILE_NAME + "_offset_containment_vs_energy.pdf")




if "__main__" == __name__:
    main()