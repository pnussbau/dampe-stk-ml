import uproot
import matplotlib as mpl
mpl.use('Agg') # Save figures to disk instead of displaying them
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np
from enum import Enum
import argparse
import sys
import os
import multiprocessing as mp
from concurrent.futures import ThreadPoolExecutor, as_completed

#Extracted the following values of the detector geometry from the root file
#BGO
BGO_BAR_WIDTH = 25 #mm
BGO_BAR_VERTICAL_SPACING = 4 #mm
BGO_NUMBER_OF_VERTICAL_LAYER = 14
BGO_NUMBER_OF_HORIZONTAL_LAYER = 22
BGO_BAR_HORIZONTAL_SPACING = 2.5
BGO_LEFT_MOST_HORIZONTAL_POS = -288.75 #mm
BGO_BOTTOM_MOST_VERTICAL_POS_Y = 58.5 #mm
BGO_BOTTOM_MOST_VERTICAL_POS_X = BGO_BOTTOM_MOST_VERTICAL_POS_Y + BGO_BAR_WIDTH + BGO_BAR_VERTICAL_SPACING#mm
BGO_TRACK_INTERCEPT_REFERENCE_XZ = 0
BGO_TRACK_INTERCEPT_REFERENCE_YZ = 0

#STK
STK_STRIP_XZ_Z_POS = [-209.58199763, -176.78199706, -143.98199706, -111.1819942,   -79.18199573, -47.18195758] #mm
STK_STRIP_YZ_Z_POS = [-206.41799808, -173.61799665, -140.81799712, -108.01799655,  -76.01799274, -44.0179584 ] #mm
STK_STRIP_X_AND_Y_RANGE = [-400,400] #mm
#PSD
PSD_STRIP_CENTRE_XZ_Z_POS = [-298.5, -284.5] #mm
PSD_STRIP_CENTRE_YZ_Z_POS = [-324.7, -310.7] #mm
#Append the two layers
PSD_STRIP_CENTRE_Z_POS = PSD_STRIP_CENTRE_YZ_Z_POS + PSD_STRIP_CENTRE_XZ_Z_POS
PSD_STRIP_CENTRE_HORZINTAL = [[-380.0, -340.0, -300.0, -260.0, -220.0, -180.0, -140.0, -100.0, -60.0, -20.0, 20.0, 60.0, 100.0, 140.0, 180.0, 220.0, 260.0, 300.0, 340.0, 380.0],[-398.5, -360, -320, -280, -240, -200, -160, -120, -80, -40, 0, 40, 80, 120, 160, 200, 240, 280, 320, 360, 398.5],[-380.0, -340.0, -300.0, -260.0, -220.0, -180.0, -140.0, -100.0, -60.0, -20.0, 20.0, 60.0, 100.0, 140.0, 180.0, 220.0, 260.0, 300.0, 340.0, 380.0],[-398.5, -360, -320, -280, -240, -200, -160, -120, -80, -40, 0, 40, 80, 120, 160, 200, 240, 280, 320, 360, 398.5]] #mm
PSD_LAYER_X = 1
PSD_LAYER_Y = 0
PSD_STRIP_WIDTH = 28 #mm
PSD_STRIP_HEIGHT = 10 #mm

#Define an Enum to represent the two possible projections
class Projection(Enum):
    XZ = 1
    YZ = 2
    BOTH = 3


class EventVisualizer:
    """
    A class for visualizing events from the DAMPE. It can be used to visualize events from both Monte Carlo and real data.
    """
    def __init__(self, file_path, tree_name="DmlNtup", is_mc=False, verbose=False, cnn_friend_path=None, friend_tree_name="cnn_track;1"):
        """
        Initialize the VisualizeEvent class.

        Args:
        - file_path (str): Path to the input ROOT file.
        - tree_name (str): Name of the TTree in the ROOT file.
        - is_mc (bool): Whether the input file contains Monte Carlo data.
        - verbose (bool): Whether to print verbose output.

        Returns:
        None
        """
        self.file_suffix_projections = {Projection.XZ: "XZ", Projection.YZ: "YZ", Projection.BOTH: "BOTH"}


        self._file_path = file_path
        self._tree_name = tree_name
        self.verbose = verbose
        self.is_mc = is_mc
        self._friend_path = cnn_friend_path
        self._friend_tree_name = friend_tree_name if cnn_friend_path is not None else None

        # Check that the file exists and print to stderr if it doesn't
        try:
            open(self._file_path)
        except IOError:
            print("File not found: ", self._file_path, file=sys.stderr)
            return     
        self._file = uproot.open(self._file_path)
        self._tree = self._file[self._tree_name]
        self._n_events = len(self._tree["BgoHit_layerID"].array())

        self._friend_tree = None
        self._friend_file = None
        if self._friend_path is not None:
            try:
                open(self._friend_path)
            except IOError:
                print("File not found: ", self._friend_path, file=sys.stderr)
                return
            self._friend_file = uproot.open(self._friend_path)
            self._friend_tree = self._friend_file[self._friend_tree_name]
            friend_n_events = len(self._friend_tree["cnn_stk_track_x1"].array())
            assert friend_n_events == self._n_events, "The number of events in the friend tree must be the same as in the main tree"

        
        #Get the binning ready for plotting
        self._prepare_plotting()
        
    def _prepare_plotting(self):
        #Binning for the BGO bars
        bins_vertical = [BGO_BOTTOM_MOST_VERTICAL_POS_Y-BGO_BAR_WIDTH/2]
        for i in range(1, 2*BGO_NUMBER_OF_VERTICAL_LAYER+2):
            if i % 2 == 1:
                bins_vertical.append(bins_vertical[i - 1] + BGO_BAR_WIDTH)
            else:
                bins_vertical.append(bins_vertical[i - 1] + BGO_BAR_VERTICAL_SPACING)
                
        bins_horizontal = [BGO_LEFT_MOST_HORIZONTAL_POS-BGO_BAR_WIDTH/2]
        for i in range(1, 2*BGO_NUMBER_OF_HORIZONTAL_LAYER+2):
            if i % 2 == 1:
                bins_horizontal.append(bins_horizontal[i - 1] + BGO_BAR_WIDTH)
            else:
                bins_horizontal.append(bins_horizontal[i - 1] + BGO_BAR_HORIZONTAL_SPACING)

        self.BGO_VERTICAL_BINS = np.array(bins_vertical)
        self.BGO_HORIZONTAL_BINS = bins_horizontal

        #PSD
        bins_horizontal = []
        bins_vertical = []
        for sub_layer, strip_z_pos in enumerate(PSD_STRIP_CENTRE_Z_POS):
            bins_vertical_in_layer = np.array([strip_z_pos-PSD_STRIP_HEIGHT/2, strip_z_pos+PSD_STRIP_HEIGHT/2])
            bins_vertical.append(bins_vertical_in_layer)
            bins_horizontal_in_layer = []
            for horizontal_pos in PSD_STRIP_CENTRE_HORZINTAL[sub_layer]:
                bins_horizontal_in_layer.append(horizontal_pos-PSD_STRIP_WIDTH/2)
                bins_horizontal_in_layer.append(horizontal_pos+PSD_STRIP_WIDTH/2)
            bins_horizontal.append(np.array(bins_horizontal_in_layer))
        self.PSD_VERTICAL_BINS = np.array(bins_vertical)
        self.PSD_HORIZONTAL_BINS = np.array(bins_horizontal, dtype=object)
    
    def _draw_bgo(self, ax, fig, x, y, z, layer, energy, projection):
        LINE_WIDTH = 0.15
        BGO_BAR_COLOR = "black"
        LABEL_COLORBAR = "Energy BGO [MeV]"

        #Fix the color scale (energy) globally for both projections
        min_energy = energy.min()
        max_energy = energy.max()
        #Mask the bary which are not oriented into the plain of the detector
        if projection == Projection.XZ:
            layer_mask = layer % 2 == 1
        elif projection == Projection.YZ:
            layer_mask = layer % 2 == 0
        
        #Select on the hits in the corresponding projection plane
        z = z[layer_mask]
        energy = energy[layer_mask]
        if projection == Projection.XZ:
            horizontal = x[layer_mask]
        elif projection == Projection.YZ:
            horizontal = y[layer_mask]

        # Draw the BGO detector bars
        for i in range(int(BGO_NUMBER_OF_VERTICAL_LAYER/2)):
            for j in range(BGO_NUMBER_OF_HORIZONTAL_LAYER):
                if projection == Projection.XZ:
                    ax.add_patch(Rectangle((self.BGO_HORIZONTAL_BINS[2*j], self.BGO_VERTICAL_BINS[2+4*i]), 
                                            BGO_BAR_WIDTH, BGO_BAR_WIDTH, 
                                            linewidth=LINE_WIDTH, 
                                            edgecolor=BGO_BAR_COLOR, 
                                            facecolor='none'))
                elif projection == Projection.YZ:
                    ax.add_patch(Rectangle((self.BGO_HORIZONTAL_BINS[2*j], self.BGO_VERTICAL_BINS[4*i]), 
                                            BGO_BAR_WIDTH, BGO_BAR_WIDTH, 
                                            linewidth=LINE_WIDTH, 
                                            edgecolor=BGO_BAR_COLOR, 
                                            facecolor='none'))
        # Draw the BGO hits
        h1 = ax.hist2d(horizontal, z, 
                bins=(self.BGO_HORIZONTAL_BINS,self.BGO_VERTICAL_BINS), 
                weights=energy, 
                norm=mpl.colors.LogNorm(vmin=min_energy,vmax=max_energy), 
                cmap="jet", cmin=1e-10)
        #Place the colorbar in the top right corner
        c1 = fig.colorbar(h1[3], ax=ax, label=LABEL_COLORBAR)

        return c1
    
    def _draw_bgo_predicted_track(self, ax, bgo_rec_slope_x, bgo_rec_intercept_x, bgo_rec_slope_y, bgo_rec_intercept_y, projection):
        LINE_STYLE = "-."
        BGO_TRACK_COLOR = "lime"
        LABEL = "Reconstructed BGO track"

        # Draw the reconstructed "seed track" from the BGO data
        if bgo_rec_slope_x is not None and projection == Projection.XZ:
            ax.axline([bgo_rec_intercept_x, BGO_TRACK_INTERCEPT_REFERENCE_XZ], 
                      [bgo_rec_intercept_x + (100 - BGO_TRACK_INTERCEPT_REFERENCE_XZ) * bgo_rec_slope_x, 100], 
                      color=BGO_TRACK_COLOR, linestyle=LINE_STYLE, label=LABEL)
        elif bgo_rec_slope_y is not None and projection == Projection.YZ:
            ax.axline([bgo_rec_intercept_y, BGO_TRACK_INTERCEPT_REFERENCE_YZ], 
                      [bgo_rec_intercept_y + (100 -  BGO_TRACK_INTERCEPT_REFERENCE_YZ) * bgo_rec_slope_y, 100], 
                      color=BGO_TRACK_COLOR, linestyle=LINE_STYLE, label=LABEL) 


    def _draw_stk(self, ax, stk_cluster_x, stk_cluster_y, stk_cluster_z, projection):
        MARKER_SIZE = 5
        MARKER_COLOR = "black"
        STK_DETECTOR_LINE_STYLE = "-"
        STK_DETECTOR_COLOR = "black"
        LABEL = "STK cluster"

        # Only selec the hits in the corresponding projection plane. By construction the coordinate of hits in the wrong plane are set to -9000 
        if projection == Projection.XZ:
            stk_horizontal = stk_cluster_x[stk_cluster_x > -9000]
            stk_cluster_z = stk_cluster_z[stk_cluster_x > -9000]
            STK_STRIP_Z_POS = STK_STRIP_XZ_Z_POS
        elif projection == Projection.YZ:
            stk_horizontal = stk_cluster_y
            stk_horizontal = stk_cluster_y[stk_cluster_y > -9000]
            stk_cluster_z = stk_cluster_z[stk_cluster_y > -9000]
            STK_STRIP_Z_POS = STK_STRIP_YZ_Z_POS
            
        # Draw the STK detector strips
        for strip_z_pos in STK_STRIP_Z_POS:
            ax.plot(STK_STRIP_X_AND_Y_RANGE, [strip_z_pos, strip_z_pos], color=STK_DETECTOR_COLOR, linestyle=STK_DETECTOR_LINE_STYLE)
    
        # Draw the STK detector hits
        ax.plot(stk_horizontal, stk_cluster_z, marker="x", markersize=MARKER_SIZE, linestyle="", color=MARKER_COLOR, label=LABEL)

        
    def _draw_stk_tracks(self, ax, stk_track_x_x1, stk_track_x_x2, stk_track_y_y1, stk_track_y_y2, stk_tq, number_of_best_tracks_to_plot, projection):
        LINE_STYLE = "--"
        TRACK_COLOR = "orange"
        BEST_TRACK_COLOR = "red"
        LINE_WIDTH = 0.5
        BEST_LINE_WIDTH = 2
        BEST_TRACK_LABEL = "Best STK track"
        TRACK_LABEL = "STK track"

        stk_n_tracks = len(stk_tq)
        get_n_tracks = min(number_of_best_tracks_to_plot, stk_n_tracks)
        if len(stk_tq) != len(stk_track_x_x1) or len(stk_tq) != len(stk_track_x_x2) or len(stk_tq) != len(stk_track_y_y1) or len(stk_tq) != len(stk_track_y_y2):
            raise ValueError("The arrays for the STK data must have the same dimension")
        sorted_indices = np.argsort(stk_tq)
        stk_track_x_x1 = stk_track_x_x1[sorted_indices][-get_n_tracks:]
        stk_track_x_x2 = stk_track_x_x2[sorted_indices][-get_n_tracks:]
        stk_track_y_y1 = stk_track_y_y1[sorted_indices][-get_n_tracks:]
        stk_track_y_y2 = stk_track_y_y2[sorted_indices][-get_n_tracks:]
        stk_tq = stk_tq[sorted_indices][-get_n_tracks:]
        # Plot the best one in red
        colors = []
        lws = []
        labels = []
        if get_n_tracks > 0:
            colors = [TRACK_COLOR]*(get_n_tracks-1)
            colors.append(BEST_TRACK_COLOR)
            lws = [LINE_WIDTH]*(get_n_tracks-1)
            lws.append(BEST_LINE_WIDTH)
            labels = [TRACK_LABEL]*(get_n_tracks-1)
            labels.append(BEST_TRACK_LABEL)
        # Draw the STK detector tracks
        for x1, x2, y1, y2, color, lw, l in zip(stk_track_x_x1, stk_track_x_x2, stk_track_y_y1, stk_track_y_y2, colors, lws, labels):
            if projection == Projection.XZ:
                ax.axline((x1, PSD_STRIP_CENTRE_XZ_Z_POS[-1]), (x2, PSD_STRIP_CENTRE_XZ_Z_POS[0]), color=color, lw=lw, linestyle=LINE_STYLE, label=l)
            elif projection == Projection.YZ:
                ax.axline([y1, PSD_STRIP_CENTRE_YZ_Z_POS[-1]], [y2, PSD_STRIP_CENTRE_YZ_Z_POS[0]], color=color, lw=lw, linestyle=LINE_STYLE, label=l)
    def _draw_cnn_track(self, ax, cnn_track_x1, cnn_track_x2, cnn_track_y1, cnn_track_y2, projection):
        LINE_STYLE = "--"
        TRACK_COLOR = "blue"
        LINE_WIDTH = 2
        TRACK_LABEL = "CNN track"

        if projection == Projection.XZ:
            ax.axline((cnn_track_x1, PSD_STRIP_CENTRE_XZ_Z_POS[-1]), (cnn_track_x2, PSD_STRIP_CENTRE_XZ_Z_POS[0]), color=TRACK_COLOR, lw=LINE_WIDTH, linestyle=LINE_STYLE, label=TRACK_LABEL)
            #ax.scatter(cnn_track_x1, PSD_STRIP_CENTRE_XZ_Z_POS[-1], marker="*", color=TRACK_COLOR)
            #ax.scatter(cnn_track_x2, PSD_STRIP_CENTRE_XZ_Z_POS[0], marker="^", color=TRACK_COLOR)
            #ax.scatter(cnn_track_y1, PSD_STRIP_CENTRE_XZ_Z_POS[-1], marker="*", color="red")
            #ax.scatter(cnn_track_y2, PSD_STRIP_CENTRE_XZ_Z_POS[0], marker="^", color="red")
        elif projection == Projection.YZ:
            ax.axline([cnn_track_y1, PSD_STRIP_CENTRE_YZ_Z_POS[-1]], [cnn_track_y2, PSD_STRIP_CENTRE_YZ_Z_POS[0]], color=TRACK_COLOR, lw=LINE_WIDTH, linestyle=LINE_STYLE, label=TRACK_LABEL)
            #ax.scatter(cnn_track_y1, PSD_STRIP_CENTRE_YZ_Z_POS[-1], marker="*", color=TRACK_COLOR)
            #ax.scatter(cnn_track_y2, PSD_STRIP_CENTRE_YZ_Z_POS[0], marker="^", color=TRACK_COLOR)
            #ax.scatter(cnn_track_x1, PSD_STRIP_CENTRE_YZ_Z_POS[-1], marker="*", color="red")
            #ax.scatter(cnn_track_x2, PSD_STRIP_CENTRE_YZ_Z_POS[0], marker="^", color="red")
    def _draw_best_stk_hits(self, ax, stk_best_track_clusters_x, stk_best_track_clusters_y, stk_best_track_clusters_z, projection):
        MARKER_SIZE = 8
        MARKER_COLOR = "red"
        MARKER = "x"
        LABEL = "Best STK track cluster"

        if projection == Projection.XZ:
            stk_horizontal = stk_best_track_clusters_x
        elif projection == Projection.YZ:
            stk_horizontal = stk_best_track_clusters_y
        # Draw the STK detector hits
        stk_mask = stk_horizontal > -9000
        ax.plot(stk_horizontal[stk_mask], stk_best_track_clusters_z[stk_mask], marker=MARKER, markersize=MARKER_SIZE, linestyle="", color=MARKER_COLOR, label=LABEL)

    def _draw_psd(self, ax, fig, x_psd, y_psd, z_psd, id_psd, energy_psd, projection, log_scale=False):
        # Define constants
        PSD_DETECTOR_LINE_WIDTH = 0.15
        PSD_DETECTOR_COLOR = "black"
        LABEL_COLORBAR = "Energy PSD [MeV]"
        
        # Set the color scale (energy) globally for both projections
        if len(energy_psd) > 0:
            min_energy = energy_psd.min()
            max_energy = energy_psd.max()

        # Select the hits in the corresponding projection plane
        if projection == Projection.XZ:
            psd_strip_sublayer_mask = [False, False, True, True]
            horizontal_pos_PSD_hits = x_psd[id_psd == PSD_LAYER_X]
            z_psd = z_psd[id_psd == PSD_LAYER_X]
            energy_psd = energy_psd[id_psd == PSD_LAYER_X]
        elif projection == Projection.YZ:
            psd_strip_sublayer_mask = [True, True, False, False]
            horizontal_pos_PSD_hits = y_psd[id_psd == PSD_LAYER_Y]
            z_psd = z_psd[id_psd == PSD_LAYER_Y]
            energy_psd = energy_psd[id_psd == PSD_LAYER_Y]

        #Draw PSD strips
        h2_s = []
        for sub_layer, strip_z_pos in enumerate(np.array(PSD_STRIP_CENTRE_Z_POS)[psd_strip_sublayer_mask]):
            for horizontal_pos in PSD_STRIP_CENTRE_HORZINTAL[sub_layer]:
                ax.add_patch(Rectangle((horizontal_pos-PSD_STRIP_WIDTH/2, strip_z_pos-PSD_STRIP_HEIGHT/2), 
                                        PSD_STRIP_WIDTH, PSD_STRIP_HEIGHT, 
                                        linewidth=PSD_DETECTOR_LINE_WIDTH, 
                                        edgecolor=PSD_DETECTOR_COLOR, 
                                        facecolor='none'))
        if len(energy_psd) != 0:
            for sub_layer, strip_z_pos in enumerate(np.array(PSD_STRIP_CENTRE_Z_POS)[psd_strip_sublayer_mask]):
                if log_scale:
                    norm = mpl.colors.LogNorm(vmin=min_energy, vmax=energy_psd.max_energy())
                else:
                    norm = mpl.colors.Normalize(vmin=min_energy, vmax=max_energy)
                h2 = ax.hist2d(horizontal_pos_PSD_hits, z_psd, 
                            bins=(self.PSD_HORIZONTAL_BINS[psd_strip_sublayer_mask][sub_layer],
                                self.PSD_VERTICAL_BINS[psd_strip_sublayer_mask][sub_layer]), 
                            weights=energy_psd, 
                            norm=norm, 
                            cmap="jet", 
                            cmin=1e-10)
                #Print the number of points in the histogram
                h2_s.append(h2) 
        if len(energy_psd) == 0:
            return None
        else:
            #Place the colorbar at the bottom right corner. Since all colorbars have the same min and max, we can use the first one
            c2 = fig.colorbar(h2_s[0][3], ax=ax, label=LABEL_COLORBAR)
            return c2
    
    def _draw_mc_truth(self, ax, x, y, z, p_x, p_y, p_z, projection):
        MC_TRACK_STYLE = "--"
        MC_TRACK_COLOR = "black"
        ARROW_COLOR = "black"
        ARROW_WIDTH = 30
        ARROW_STARTS_AT_ZERO = True
        LABEL = "True track"

        # Draw the track of the true MC particle
        # Normalize the direction of the MC particle
        t_direction_norm = np.sqrt(p_x ** 2 + p_y ** 2 + p_z ** 2)
        t_vec_normalized = 30 * 1 / t_direction_norm * np.array([p_x, p_y, p_z])
        t_z_pos_arrow = -60

        if x is not None and projection == Projection.XZ:
            ax.axline([x, z], [x + p_x, z + p_z], color=MC_TRACK_COLOR, linestyle=MC_TRACK_STYLE, label=LABEL)
            t_x_pos_arrow = x + (t_z_pos_arrow - z) * p_x / p_z
            # Draw an arrow indicating the normaized direction of the MC particle
            ax.arrow(t_x_pos_arrow, t_z_pos_arrow, t_vec_normalized[0], t_vec_normalized[2], color=ARROW_COLOR, head_width=ARROW_WIDTH, head_starts_at_zero=ARROW_STARTS_AT_ZERO)
        elif y is not None and projection == Projection.YZ:
            ax.axline([y, z], [y + p_y, z + p_z], color=MC_TRACK_COLOR, linestyle=MC_TRACK_STYLE, label=LABEL)
            # Draw an arrow indicating the normaized direction of the MC particle
            t_y_pos_arrow = y + (t_z_pos_arrow - z) * p_y / p_z
            ax.arrow(t_y_pos_arrow, t_z_pos_arrow, t_vec_normalized[1], t_vec_normalized[2], color=ARROW_COLOR, head_width=ARROW_WIDTH, head_starts_at_zero=ARROW_STARTS_AT_ZERO)
           

    def _draw_event_display_figure(self, energy, t_energy, t_x, t_y, t_z, t_px, t_py, t_pz, 
                        bgo_x_slope, bgo_x_intercept, bgo_y_slope, bgo_y_intercept, stk_track_tq, 
                        stk_track_x1, stk_track_x2, stk_track_y1, stk_track_y2, stk_best_track_x, 
                        stk_best_track_y, stk_best_track_z, bgo_x, bgo_y, bgo_z, bgo_layer_id, 
                        bgo_e, stk_x, stk_y, stk_z, psd_x, psd_y, psd_z, psd_layer_id, psd_energy,
                        cnn_track_x1, cnn_track_x2, cnn_track_y1, cnn_track_y2,
                        projection, draw_legend=True):
        """
        Draws the event display for the given event.

        Parameters:
        energy (float): Measured energy of the event.
        t_energy (float): True energy of the event in MeV.
        t_x (float): True x position of the event.
        t_y (float): True y position of the event.
        t_z (float): True z position of the event.
        t_px (float): True x momentum of the event.
        t_py (float): True y momentum of the event.
        t_pz (float): True z momentum of the event.
        bgo_x_slope (float): Slope of the predicted BGO track in x direction.
        bgo_x_intercept (float): Intercept at the bottom of the BGO (closest to the STK) of the predicted BGO track in x direction.
        bgo_y_slope (float): Slope of the predicted BGO track in y direction.
        bgo_y_intercept (float): Intercept at the bottom of the BGO (closest to the STK)of the predicted BGO track in y direction.
        stk_track_tq (list): List of track qualities for STK tracks.
        stk_track_x1 (list): List of x positions of the first point of STK tracks.
        stk_track_x2 (list): List of x positions of the second point of STK tracks.
        stk_track_y1 (list): List of y positions of the first point of STK tracks.
        stk_track_y2 (list): List of y positions of the second point of STK tracks.
        stk_best_track_x (list): List of x positions of the best STK track.
        stk_best_track_y (list): List of y positions of the best STK track.
        stk_best_track_z (list): List of z positions of the best STK track.
        bgo_x (list): List of x positions of BGO hits.
        bgo_y (list): List of y positions of BGO hits.
        bgo_z (list): List of z positions of BGO hits.
        bgo_layer_id (list): List of layer IDs of BGO hits.
        bgo_e (list): List of energies of BGO hits in GeV.
        stk_x (list): List of x positions of STK hits.
        stk_y (list): List of y positions of STK hits.
        stk_z (list): List of z positions of STK hits.
        psd_x (list): List of x positions of PSD hits.
        psd_y (list): List of y positions of PSD hits.
        psd_z (list): List of z positions of PSD hits.
        psd_layer_id (list): List of layer IDs of PSD hits.
        psd_energy (list): List of energies of PSD hits.
        cnn_track_x1 (float): x position of the first point of the CNN track.
        cnn_track_x2 (float): x position of the second point of the CNN track.
        cnn_track_y1 (float): y position of the first point of the CNN track.
        cnn_track_y2 (float): y position of the second point of the CNN track.
        projection (Projection): Projection of the event display.
        draw_legend (bool): Whether to draw the legend or not.

        Returns:
        fig (matplotlib.figure.Figure): The figure object of the event display.
        """
        assert(projection == Projection.XZ or projection == Projection.YZ) 
        X_AXIS_LIMITS = (-450, 450)
        Y_AXIS_LIMITS = (-400, 550)
        X_LABEL_X = "X [mm]"
        X_LABEL_Y = "Y [mm]"
        Y_LABEL = "Z [mm]"
        TEXTBOX_FONT_SIZE = 8
        TICKS_FONT_SIZE = 10
        TEXTBOX_POS = (0.5, 0.94)
        AX_POS = [0.0, 0.1, 0.8, 0.85]

        # Number of best STK tracks to plot
        N_BEST_TRACKS = 5

        fig, ax = plt.subplots()
        ax.autoscale(enable=False)
        # Draw the different parts of the detector and the respective hits
        c1 = self._draw_bgo(ax, fig, bgo_x, bgo_y, bgo_z, bgo_layer_id, bgo_e, projection)
        if bgo_x_intercept is not None and bgo_y_intercept is not None and bgo_x_slope is not None and bgo_y_slope is not None:
            self._draw_bgo_predicted_track(ax, bgo_x_slope, bgo_x_intercept, bgo_y_slope, bgo_y_intercept, projection)
        elif self.verbose:
            print("Did not find BGO track to plot", file=sys.stderr)
        self._draw_stk(ax, stk_x, stk_y, stk_z, projection)
        if stk_track_x1 is not None and stk_track_x2 is not None and stk_track_y1 is not None and stk_track_y2 is not None and stk_track_tq is not None:
           self._draw_stk_tracks(ax, stk_track_x1, stk_track_x2, stk_track_y1, stk_track_y2, stk_track_tq, N_BEST_TRACKS, projection)
        elif self.verbose:
           print("Did not find STK tracks to plot", file=sys.stderr)
        if cnn_track_x1 is not None and cnn_track_x2 is not None and cnn_track_y1 is not None and cnn_track_y2 is not None:
            self._draw_cnn_track(ax, cnn_track_x1, cnn_track_x2, cnn_track_y1, cnn_track_y2, projection)
        elif self.verbose:
            print("Did not find CNN track to plot", file=sys.stderr)
        if stk_best_track_x is not None and stk_best_track_y is not None and stk_best_track_z is not None:
            self._draw_best_stk_hits(ax, stk_best_track_x, stk_best_track_y, stk_best_track_z, projection)
        elif self.verbose:
            print("Did not find Best STK hits to plot", file=sys.stderr)
        c2 = self._draw_psd(ax, fig, psd_x, psd_y, psd_z, psd_layer_id, psd_energy, projection)
        if t_x is not None and t_y is not None and t_z is not None and t_px is not None and t_py is not None and t_pz is not None and t_energy is not None:
            self._draw_mc_truth(ax, t_x, t_y, t_z, t_px, t_py, t_pz, projection)
        #Position the colorbars
        c1.ax.set_position([AX_POS[0]+AX_POS[2], 0.35, AX_POS[0]+AX_POS[2]-0.15, 0.6])
        if c2 != None:
            c2.ax.set_position([AX_POS[0]+AX_POS[2], 0.1, AX_POS[0]+AX_POS[2]-0.15, 0.20])        
        ax.set_position(AX_POS)

        # Setting properties of the plot
        if projection == Projection.XZ:
            ax.set_xlabel(X_LABEL_X)
        elif projection == Projection.YZ:
            ax.set_xlabel(X_LABEL_Y)
        ax.set_ylabel(Y_LABEL)

        # Set the x and y limits of the plot
        ax.set_xlim(*X_AXIS_LIMITS)
        ax.set_ylim(*Y_AXIS_LIMITS)
        ax.set_aspect('equal', adjustable='box')
        ax.xaxis.set_major_locator(plt.MultipleLocator(100))
        ax.yaxis.set_major_locator(plt.MultipleLocator(100))
        ax.tick_params(axis='both', which='major', labelsize=TICKS_FONT_SIZE)
        ax.minorticks_on()
        
        # Include event information in the plot
        energy_text = "Reco energy: " + str(round(energy, 1)) + " GeV"
        if t_energy is not None:
            energy_text += "\n True energy: " + str(round(t_energy / 1000, 1)) + " GeV"
        energy_text += "\n STK tracks: " + str(len(stk_track_tq))
        props = dict(boxstyle='square', facecolor='white', edgecolor='black', alpha=0.8)
        ax.text(TEXTBOX_POS[0], TEXTBOX_POS[1], energy_text, horizontalalignment='center', verticalalignment='center', transform=ax.transAxes, bbox=props, fontsize=TEXTBOX_FONT_SIZE)

        # Draw the legend
        if draw_legend:
            handles, labels = ax.get_legend_handles_labels()
            by_label = dict(zip(labels, handles))
            #Position the legend over the plot 
            ax.legend(by_label.values(), by_label.keys(), loc='upper center', bbox_to_anchor=(0.5, 1.07), ncol=3, fancybox=True,  prop={'size': 6})

        return fig
           
    def draw_event_using_data_from_file(self, event_id, projection, draw_legend=False):
        # Read the reconstructed energy of the event
        energy = self._tree["tt_bgoTotalEcorr_GeV"].array(library="np")[event_id]  # energy in Gev
        # Read the MC information tt_mcprimaries_XPos, tt_mcprimaries_YPos, tt_mcprimaries_ZPos, tt_mcprimaries_XPart, tt_mcprimaries_YPart et tt_mcprimaries_ZPar
        t_energy, t_x, t_y, t_z, t_px, t_py, t_pz = None, None, None, None, None, None, None
        if self.is_mc:
            try:
                # Read the the true MC information on the event
                t_energy = self._tree["tt_ekin"].array(library="np")[event_id]
                # Read the trajectiory of the MC primary particle
                t_x = self._tree["tt_mcprimary_XPos"].array(library="np")[event_id]
                t_y = self._tree["tt_mcprimary_YPos"].array(library="np")[event_id]
                t_z = self._tree["tt_mcprimary_ZPos"].array(library="np")[event_id]
                t_px = self._tree["tt_mcprimary_XPart"].array(library="np")[event_id]
                t_py = self._tree["tt_mcprimary_YPart"].array(library="np")[event_id]
                t_pz = self._tree["tt_mcprimary_ZPart"].array(library="np")[event_id]

            except KeyError:
                print("Warning: The option --mc expects a MC file", file=sys.stderr)
                return
        
        # Store BGO hit information as numpy arrays
        bgo_layer_id = self._tree["BgoHit_layerID"].array(library="np")[event_id]
        bgo_x = self._tree["BgoHit_XPos"].array(library="np")[event_id]
        bgo_y = self._tree["BgoHit_YPos"].array(library="np")[event_id]
        bgo_z = self._tree["BgoHit_ZPos"].array(library="np")[event_id]
        bgo_e = self._tree["BgoHit_E"].array(library="np")[event_id]

        # Store STK cluster position as numpy arrays
        stk_x = self._tree["StkHit_XPos"].array(library="np")[event_id]
        stk_y = self._tree["StkHit_YPos"].array(library="np")[event_id]
        stk_z = self._tree["StkHit_ZPos"].array(library="np")[event_id]

        # Store the PSD git information as numpy arrays
        psd_x = self._tree["PsdHit_XPos"].array(library="np")[event_id]
        psd_y = self._tree["PsdHit_YPos"].array(library="np")[event_id]
        psd_z = self._tree["PsdHit_ZPos"].array(library="np")[event_id]
        psd_layer_id = self._tree["PsdHit_layerID"].array(library="np")[event_id]
        psd_energy = self._tree["PsdHit_E"].array(library="np")[event_id]

        # Load the BGO reconstructed track
        bgo_x_intercept = self._tree["tt_bgoRecInterceptX"].array(library="np")[event_id]
        bgo_y_intercept = self._tree["tt_bgoRecInterceptY"].array(library="np")[event_id]
        bgo_x_slope = self._tree["tt_bgoRecSlopeX"].array(library="np")[event_id]
        bgo_y_slope = self._tree["tt_bgoRecSlopeY"].array(library="np")[event_id]

        # Load the tracks reconstructed by the STK
        stk_track_tq = self._tree["TQ_Corr"].array(library="np")[event_id]
        stk_track_x1 = self._tree["X_Projection_X1"].array(library="np")[event_id]
        stk_track_x2 = self._tree["X_Projection_X2"].array(library="np")[event_id]
        stk_track_y1 = self._tree["Y_Projection_Y1"].array(library="np")[event_id]
        stk_track_y2 = self._tree["Y_Projection_Y2"].array(library="np")[event_id]
        
        # Load the STK clusters associated to the the best track (with the highest track quality)
        # Note this does not correspond to the best track according to the TQ variable (as defined by Jennifier Frieden)
        # stk_best_track_x = self._tree["StkHitTrack_XPos"].array(library="np")[event_id]
        # stk_best_track_y = self._tree["StkHitTrack_YPos"].array(library="np")[event_id]
        # stk_best_track_z = self._tree["StkHitTrack_ZPos"].array(library="np")[event_id]

        # Get the cluster of the best track using the track ID
        stk_best_track_clusters_x = None
        stk_best_track_clusters_y = None
        stk_best_track_clusters_z = None

        # Load the friend tree
        cnn_track_x1, cnn_track_x2, cnn_track_y1, cnn_track_y2 = None, None, None, None
        if self._friend_tree is not None:
            # Load the STK clusters associated to the the best track (with the highest track quality)
            cnn_track_x1 = self._friend_tree["cnn_stk_track_x1"].array(library="np")[event_id]  # x_top y_top x_bot y_bot to x
            cnn_track_x2 = self._friend_tree["cnn_stk_track_x2"].array(library="np")[event_id]
            cnn_track_y1 = self._friend_tree["cnn_stk_track_y1"].array(library="np")[event_id]
            cnn_track_y2 = self._friend_tree["cnn_stk_track_y2"].array(library="np")[event_id]

        
        fig = self._draw_event_display_figure(energy=energy, t_energy=t_energy, t_x=t_x, t_y=t_y, t_z=t_z, t_px=t_px, t_py=t_py, t_pz=t_pz,
                                 bgo_x_slope=bgo_x_slope, bgo_x_intercept=bgo_x_intercept, bgo_y_slope=bgo_y_slope, bgo_y_intercept=bgo_y_intercept, stk_track_tq=stk_track_tq,
                                stk_track_x1=stk_track_x1, stk_track_x2=stk_track_x2, stk_track_y1=stk_track_y1, stk_track_y2=stk_track_y2, stk_best_track_x=stk_best_track_clusters_x,
                                stk_best_track_y=stk_best_track_clusters_y, stk_best_track_z=stk_best_track_clusters_z, bgo_x=bgo_x, bgo_y=bgo_y, bgo_z=bgo_z, bgo_layer_id=bgo_layer_id,
                                bgo_e=bgo_e, stk_x=stk_x, stk_y=stk_y, stk_z=stk_z, psd_x=psd_x, psd_y=psd_y, psd_z=psd_z, psd_layer_id=psd_layer_id, psd_energy=psd_energy,
                                cnn_track_x1=cnn_track_x1, cnn_track_x2=cnn_track_x2, cnn_track_y1=cnn_track_y1, cnn_track_y2=cnn_track_y2,
                                 projection=projection, draw_legend=draw_legend)
        return fig
        
    def _check_output_dir(self, output_dir):
        if not os.path.isdir(output_dir):
            print("Output directory not found: ", output_dir, file=sys.stderr)
            return False
        if not os.access(output_dir, os.W_OK):
            print("Output directory not writable: ", output_dir, file=sys.stderr)
            return False
        return True
    def _check_suffix(self, suffix):
        if not (suffix.endswith(".png") or suffix.endswith(".jpg") or suffix.endswith(".pdf") or suffix.endswith(".svg") or suffix.endswith(".eps") or suffix.endswith(".pdf") or suffix.endswith(".gif")):
            print("Invalid suffix: ", suffix, file=sys.stderr)
            print("The suffix must end with one of the following: .png, .jpg, .pdf, .svg, .eps, .gif", file=sys.stderr)
            return False
        else:
            return True
    
    def visualize_event(self, event_id, projection, output_dir, suffix, dpi=500, draw_legend=False):
        """
        Visualize a single event using data from a file.

        Args:
            event_id (int): The ID of the event to visualize.
            projection (Projection or list[Projection]): The projection(s) to draw.
            output_dir (str): The directory to save the output file(s) to.
            suffix (str): The suffix to add to the output file name(s).
            dpi (int, optional): The resolution of the output file(s). Defaults to 500.
            draw_legend (bool, optional): Whether to draw a legend on the plot. Defaults to False.
        """
        if self._check_output_dir(output_dir) == False:
            return
        if self._check_suffix(suffix) == False:
            return
        if event_id >= self._n_events:
            print("Event ID out of range. The maximum event ID is ", self._n_events - 1, file=sys.stderr)
            return
       
        if not output_dir.endswith("/"):
            output_dir += "/"
        
        if projection == Projection.BOTH:
            projections_to_draw = [Projection.XZ, Projection.YZ]
        else:
            projections_to_draw = [projection]

        for p in projections_to_draw:
            file_suffix_projections = {Projection.XZ: "XZ", Projection.YZ: "YZ"}
            output_file_path = output_dir + self._tree_name + "_" + str(event_id) + "_" + file_suffix_projections[p] 
            if suffix[0] == "." or suffix[0] == "_":
                output_file_path += suffix
            else:
                output_file_path += "_" + suffix
            fig = self.draw_event_using_data_from_file(event_id, projection=p, draw_legend=draw_legend)
             # Save the figure
            if self.verbose:
                print("Saving figure to ", output_file_path)
            fig.savefig(output_file_path, dpi=dpi)
            fig.clf()

    def _worker_draw_event_one_pdf(self, event_id, projection, draw_legend):
        fig =  self.draw_event_using_data_from_file(event_id, projection, draw_legend)
        if self.verbose:
            print("Done drawing event ", event_id)
        return event_id, projection, fig
    
    def _worker_draw_event(self, event_id, projection, draw_legend, output_dir, suffix, dpi):
        fig = self.draw_event_using_data_from_file(event_id, projection, draw_legend)
        output_file_path = output_dir + self._tree_name + "_" + str(event_id) + "_" + self.file_suffix_projections[projection] 
        if suffix[0] == "." or suffix[0] == "_":
                output_file_path += suffix
        else:
            output_file_path += "_" + suffix
        if self.verbose:
            print("Saving figure of event {} projection {} to {}".format(event_id, self.file_suffix_projections[projection], output_file_path))
        fig.savefig(output_file_path, dpi=dpi)
        plt.close(fig)
    
    def visualize_events_in_energy_range(self, projection, output_dir, suffix=None, store_as_single_pdf=False, energy_min=-np.inf, energy_max=np.inf, max_n_events=10, use_mc_energy=False, must_contain_stk_track=False, dpi=500, draw_legend=False, n_workers=4):
        """
        Visualize events in a given energy range.

        Args:
            projection (str): The projection to use for the visualization.
            output_dir (str): The output directory where the visualization files will be saved.
            suffix (str, optional): A suffix to add to the output file names. Defaults to None.
            store_as_single_pdf (bool, optional): Whether to store all the visualizations in a single PDF file. Defaults to False.
            energy_min (float, optional): The minimum energy of the events to visualize. In GeV. Defaults to -np.inf.
            energy_max (float, optional): The maximum energy of the events to visualize. In GeV. Defaults to np.inf.
            max_n_events (int, optional): The maximum number of events to visualize. Defaults to 10.
            use_mc_energy (bool, optional): Whether to use the true particle0s energy instead of the measured BGO energy. This can only be set to True if the data is Monte Carlo. Defaults to False.
            must_contain_stk_track (bool, optional): Whether to only visualize events that contain at least one STK track. Defaults to False.
            dpi (int, optional): The resolution of the output file(s). Defaults to 500.
            draw_legend (bool, optional): Whether to draw a legend on the plot. Defaults to False.

        """
        PDF_ID_FONT_SIZE = 12

        if self._check_output_dir(output_dir) == False:
            return
        if suffix is not None and store_as_single_pdf == True:
            print("Only either suffix or store_as_single_pdf can be set", file=sys.stderr)
            return
        if suffix is not None and self._check_suffix(suffix) == False:
            return   
        if use_mc_energy and not self.is_mc:
            print("use_mc_energy can only be set if is_mc is True. This means that the file has to contain the MC data", file=sys.stderr)
            return
        if  store_as_single_pdf is False and suffix is None:
            print("One of the arguments suffix or store_as_single_pdf must be set", file=sys.stderr)
            return
        
        if use_mc_energy:
            #Covert to GeV since the MC energy is in MeV
            event_energies = self._tree["tt_ekin"].array(library="np")/1000
        else:
            event_energies = self._tree["tt_bgoTotalE_GeV"].array(library="np")
        if must_contain_stk_track:
            stk_track_x1 = self._tree["X_Projection_X1"].array(library="np")
        
        if not output_dir.endswith("/"):
            output_dir += "/"

        ids_to_visualize = []
        matches = 0
        for id, energy in enumerate(event_energies):
            if energy_min <= energy and energy <= energy_max:
                if must_contain_stk_track and stk_track_x1[id].size > 0 or not must_contain_stk_track:
                    matches += 1
                    ids_to_visualize.append(id)
            if matches >= max_n_events:
                if self.verbose:
                    print("Reached the maximum number of events to visualize: " + str(max_n_events) + ". Stopping here.")
                break
        if self.verbose:       
            print("Found ", len(ids_to_visualize), " events that match the criteria:")
            print(ids_to_visualize)

        drawing_tasks = []
        for id in ids_to_visualize:
            if projection == Projection.XZ or projection == Projection.BOTH:
                drawing_tasks.append([id, Projection.XZ, draw_legend])
            if projection == Projection.YZ or projection == Projection.BOTH:
                drawing_tasks.append([id, Projection.YZ, draw_legend])
        #Check the number of CPU cores
        if n_workers > mp.cpu_count():
            print("Warning: The number of workers is greater than the number of CPU cores. Consider setting the number of workers to a value less than or equal to {}".format(mp.cpu_count()), file=sys.stderr)
        if not store_as_single_pdf:
            #Add the missing options
            for task in drawing_tasks:
                task.append(output_dir)
                task.append(suffix)
                task.append(dpi)
            with mp.Pool(n_workers) as pool:
                pool.starmap(self._worker_draw_event, drawing_tasks)
            if self.verbose:
                print("Done saving all the figures!")
        if store_as_single_pdf:
            # Save all the figures in one PDF
            with ThreadPoolExecutor(max_workers=n_workers) as executor:
                futures = [executor.submit(self._worker_draw_event_one_pdf, id, projection, draw_legend) for id, projection, draw_legend in drawing_tasks]
                output_file_path = output_dir + self._tree_name + "_" + self.file_suffix_projections[projection] + "_" + str(energy_min) + "-" + str(energy_max) + "_" + str(must_contain_stk_track) + ".pdf"
                if self.verbose:
                    print("Saving all the figures as one PDF:", output_file_path)
                with PdfPages(output_file_path, metadata={'Title': 'DAMPE event display'}) as pdf:
                    for future in futures:
                        id, projection, fig = future.result()
                        title_str = "Event ID: " + str(id)
                        # Set the location of the supertitle to be on the left and oriented in the vertical direction
                        fig.suptitle(title_str, x=0.05, y=0.5, rotation="vertical", fontsize=PDF_ID_FONT_SIZE)
                        pdf.savefig(fig, dpi=dpi)
                        if self.verbose:
                            print("Saved event ID", id)
                        plt.close(fig)

def main():
    # Define arguments using argparse
    parser = argparse.ArgumentParser(description='Visualize DAMPE event data. Either a single event ("--id") or a range of events can be visualized. The latter uses the options --e-range --must-contain-stk-track to select the events to visualize.')
    parser.add_argument('--tree-name', type=str, default='DmlNtup', help='Name of the tree in the ROOT file')
    parser.add_argument('--file-path', type=str, required=True, help='Path to the ROOT file')
    parser.add_argument('--projection', type=str, choices=["both", "XZ", "YZ"], default='both', help='Projection to draw')
    parser.add_argument('--id', type=int, default=None, help='ID of the event to visualize')
    parser.add_argument('--verbose', action='store_true', default=False, help='Enable verbose output')
    parser.add_argument('--output-dir', type=str, default='./', help='Directory to save the output file')
    parser.add_argument('--suffix', type=str, default='visualization.png', help='Suffix of the output file. This can be a file extension (e.g. .png, .pdf, .jpg, .svg, .eps) or a custom suffix (e.g. my_visualization.png)')
    parser.add_argument('--mc', action='store_true', default=False, help='Display the MC truth information')
    parser.add_argument('--dpi', type=int, default=300, help='DPI of the output file')
    parser.add_argument('--legend', action='store_true', default=False, help='Draw a legend on the plot.')
    parser.add_argument('--e-range', type=str, default=None, help='Reco energy (corrected) range in GeV of the events to visualize. Format: min_energy-max_energy (e.g. 10-100)')
    parser.add_argument('--max-n-events', type=int, default=None, help='Maximum number of events to visualize')
    parser.add_argument('--use-mc-energy', action='store_true', default=False, help='Use the true MC energy instead of the measured BGO energy')
    parser.add_argument('--must-contain-stk-track', action='store_true', default=False, help='Only visualize events that contain at least one STK track')
    parser.add_argument('--single-pdf', action='store_true', default=False, help='Store all the visualizations in a single PDF file. Warning this can take a lot of space if many events are visualized. This option may not be used together with --suffix')
    parser.add_argument('--cnn-file', type=str, default=None, help='Root file containing the friend tree with the CNN prediction')
    parser.add_argument('--n-workers', type=int, default=4, help='Number of workers to use for multiprocessing')

    #Be strict about the arguments
    args = parser.parse_args()    
    if args.projection == "both":
        projection = Projection.BOTH
    elif args.projection == "XZ":
        projection = Projection.XZ
    elif args.projection == "YZ":
        projection = Projection.YZ

    if args.dpi is not None and args.dpi <= 0:
        print("--dpi must be a positive integer", file=sys.stderr)
        return
    
    single_event_display = True
    e_range = [0, np.inf]

    if args.id is None and args.e_range is None and args.max_n_events is None and args.single_pdf is False:
        print("One of the following options must be set: --id, --e-range, --max-n-events or --single_pdf", file=sys.stderr)
        return
    if args.id is not None and (args.e_range is not None or args.max_n_events is not None or args.single_pdf is True):
        print("Only --id or one of the following options may be set: --e-range, --max-n-events or --single_pdf", file=sys.stderr)
        return
    if args.single_pdf:
        if args.verbose:
            print("Warning: The --single-pdf option overwrites --suffix")
        args.suffix = None
    if args.use_mc_energy and not args.mc:
        print("--use-mc-energy can only be set if --mc is set", file=sys.stderr)
        return
    if args.id is not None and args.id<0:
        print("--id must be a positive integer", file=sys.stderr)
        return
    if args.max_n_events is not None:
        single_event_display = False
    if args.e_range is not None:
        single_event_display = False
        #Parse the energy range
        #Split the string along the last dash
        e_range_str = args.e_range.split("-")
        if len(e_range_str) != 2:
            print("Invalid energy range: ", args.e_range, file=sys.stderr)
            return
        try:
            e_range[0] = float(e_range_str[0])
            e_range[1] = float(e_range_str[1])
        except ValueError:
            print("Invalid energy range: ", args.e_range, file=sys.stderr)
            return
        if e_range[0] > e_range[1]:
            print("Invalid energy range: ", args.e_range, file=sys.stderr)
            return
    if args.cnn_file is not None:
        if not os.path.isfile(args.cnn_file):
            print("CNN file not found: ", args.cnn_file, file=sys.stderr)
            return
    
    event_visualizer = EventVisualizer(file_path=args.file_path, tree_name=args.tree_name, is_mc=args.mc, verbose=args.verbose, cnn_friend_path=args.cnn_file)
    if single_event_display:
        if args.verbose: print("Visualizing event with ID: ", args.id, file=sys.stderr)
        event_visualizer.visualize_event(args.id, projection=projection, output_dir=args.output_dir, suffix=args.suffix, dpi=args.dpi, draw_legend=args.legend)
    else:
        if args.verbose: 
            print("Visualizing events in energy range: ", e_range, file=sys.stderr)
            print("Maximum number of events to visualize: ", args.max_n_events, file=sys.stderr)
            print("Must contain STK track: ", args.must_contain_stk_track, file=sys.stderr)
            print ("Use MC energy: ", args.use_mc_energy, file=sys.stderr)
            print("Single PDF: ", args.single_pdf, file=sys.stderr)
        event_visualizer.visualize_events_in_energy_range(
            projection=projection, output_dir=args.output_dir,
            suffix=args.suffix, store_as_single_pdf=args.single_pdf,
            energy_min=e_range[0], energy_max=e_range[1],
            max_n_events=args.max_n_events, use_mc_energy=args.use_mc_energy,
            must_contain_stk_track=args.must_contain_stk_track, dpi=args.dpi,
            draw_legend=args.legend,
            n_workers=args.n_workers
        )

if __name__ == "__main__":
    main()