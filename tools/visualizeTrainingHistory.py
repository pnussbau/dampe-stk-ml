import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
import os
import re
import seaborn as sns
BASE_DIR = "models/"
MODEL = "800um"

TRAIN_EXPRESSION = "run-.*_train-tag-batch_mean_absolute_error\.csv"
VALIDATION_EXPRESSION = "run-.*_validation-tag.*\.csv"



data_train = []
data_val = []


for file in os.listdir(os.path.join(BASE_DIR, MODEL)):
    if re.match(TRAIN_EXPRESSION, file):
        data_train.append(pd.read_csv(os.path.join(BASE_DIR, MODEL, file)))
    if re.match(VALIDATION_EXPRESSION, file):
        data_val.append(pd.read_csv(os.path.join(BASE_DIR, MODEL, file)))
print("Found {} training files and {} validation files".format(len(data_train), len(data_val)))


#Data contains Walltime, Step, Value 

df_train = pd.concat(data_train)
df_val = pd.concat(data_val)

df_train['Wall time'] = df_train['Wall time'].astype(float)
df_train['Step'] = df_train['Step'].astype(int)
df_train['Value'] = df_train['Value'].astype(float)

#Check for NaNs
print("Number of NaNs in training data: {}".format(df_train.isna().sum().sum()))
print("Number of NaNs in validation data: {}".format(df_val.isna().sum().sum()))


#Compute the relative time from the earliest wall time. There might be gaps. If so, glue the curves together.
df_train['Wall time'] = df_train['Wall time'] - df_train['Wall time'].min()
df_val['Wall time'] = df_val['Wall time'] - df_val['Wall time'].min()
#Sort the dataframes by wall time
df_train.sort_values(by=['Wall time'], inplace=True)
df_val.sort_values(by=['Wall time'], inplace=True)
#Update the index
df_train.reset_index(drop=True, inplace=True)
df_val.reset_index(drop=True, inplace=True)


#Compute the median value for the difference in wall time between two training steps and two validation steps
median_dt_train = np.median(df_train['Wall time'].diff()[1:])
median_dt_val = np.median(df_val['Wall time'].diff()[1:])

print("Median time between two training steps: {}".format(median_dt_train))
print("Median time between two validation steps: {}".format(median_dt_val))





for i in range(len(df_val)-1):
    if np.abs(df_val['Wall time'].iloc[i+1] - df_val['Wall time'].iloc[i]) > 1.2 * median_dt_val : 
        #print("Found a gap in the validation data at index {}".format(i))
        #df_val['Step'].iloc[i+1:]= df_val['Step'].iloc[i+1:] + df_val['Step'].iloc[i]
        previous_step = df_val['Step'].iloc[i]
        #print("Previous step: {}".format(previous_step))
        j = i+1
        df_val.iloc[j, df_val.columns.get_loc('Step')] = df_val.iloc[j, df_val.columns.get_loc('Step')] + previous_step + 1

        while j+1 < len(df_val) and np.abs(df_val['Wall time'].iloc[j+1] - df_val['Wall time'].iloc[j]) < 1.2 * median_dt_val:
            df_val.iloc[j+1, df_val.columns.get_loc('Step')] = df_val.iloc[j+1, df_val.columns.get_loc('Step')] + previous_step + 1
            j += 1

#Go through the data and change the Wall time to stich the curves together
for i in range(len(df_train)-1):
    if np.abs(df_train['Wall time'].iloc[i+1] - df_train['Wall time'].iloc[i]) > 1.2 * median_dt_train :
        #print("Found a gap in the training data at index {}".format(i))
        #df_train['Step'].iloc[i+1:]= df_train['Step'].iloc[i+1:] + df_train['Step'].iloc[i]
        previous_step = df_train['Step'].iloc[i]
        #print("Previous step: {}".format(previous_step))
        j = i+1
        df_train.iloc[j, df_train.columns.get_loc('Step')] = df_train.iloc[j, df_train.columns.get_loc('Step')] + previous_step + 1

        while j+1 < len(df_train) and np.abs(df_train['Wall time'].iloc[j+1] - df_train['Wall time'].iloc[j]) < 1.2 * median_dt_train:
            df_train.iloc[j+1, df_train.columns.get_loc('Step')] = df_train.iloc[j+1, df_train.columns.get_loc('Step')] + previous_step + 1
            j += 1      

df_val['Epoch'] = df_val['Step'] + 1
#Convert the steps of the train data to epochs 
df_train['Epoch'] = np.interp(df_train['Wall time'], df_val['Wall time'], df_val['Epoch']) -1 

fontsize = 20

plt.figure(figsize=(10, 5))
sns.lineplot(data=df_train, x='Epoch', y='Value', label='Training loss')
sns.lineplot(data=df_val, x='Epoch', y='Value', label='Test loss')
plt.ylabel('MAE [pixel]', fontsize=fontsize+4)
plt.xlabel('Epoch', fontsize=fontsize+4)
plt.legend(fontsize=fontsize)
plt.xticks(fontsize=fontsize)
plt.yticks(fontsize=fontsize)
if MODEL == "500um":
    plt.title("$500$ $\mu$m/pixel model", fontsize=fontsize)
elif MODEL == "050um":
    plt.title("$50$ $\mu$m/pixel model", fontsize=fontsize)
elif MODEL == "100um":
    plt.title("$100$ $\mu$m/pixel model", fontsize=fontsize)
elif MODEL == "800um":
    plt.title("$800$ $\mu$m/pixel model", fontsize=fontsize)
#add minor gridlines
plt.grid(which="minor", alpha=0.2)
plt.grid(which="major", alpha=0.5)

plt.tight_layout()
plt.savefig(os.path.join(BASE_DIR, MODEL, "training_history.png"), dpi=300)
plt.savefig(os.path.join(BASE_DIR, MODEL, "training_history.pdf"))
plt.savefig(os.path.join(BASE_DIR, MODEL, "training_history.eps"))
plt.close()