import io
import tensorflow as tf
from tensorflow import keras
import numpy as np
import argparse
import os
import sys
import time
import keras_tuner as kt
from tensorboard.plugins.hparams import api as tb_hp_api
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from models.models import *

DATA_SET_SUFFIX = "_dataset"

class MyHyperModel(kt.HyperModel):
    _batch_size = None
    def __init__(self, batch_size, model_name):
        self._batch_size = batch_size
        self._model_name = model_name

        super().__init__()

    def build(self, hyper_params):
        lr = hyper_params.Float('lr', 0.0001, 1.0, sampling='log')
        optimizer = hyper_params.Choice('optimizer', ['adam', 'sgd', 'rmsprop'])
        conv_kernel_initializer = hyper_params.Choice('conv_kernel_initializer', ['glorot_uniform', 'glorot_normal', 'he_uniform', 'he_normal', 'None'])  
        dense_kernel_initializer = hyper_params.Choice('dense_kernel_initializer', ['glorot_uniform', 'glorot_normal', 'he_uniform', 'he_normal', 'None'])
        cnn_activation = hyper_params.Choice('cnn_activation', ['relu', 'tanh', 'sigmoid', 'leakyrelu'])
        dense_activation = hyper_params.Choice('dense_activation', ['relu', 'tanh', 'sigmoid', 'leakyrelu'])   
        num_fc_layers = hyper_params.Int('num_fc_layers', 1, 5)
        fc_width = hyper_params.Int('fc_width', 1, 100)
        dropout = hyper_params.Float('dropout', 0.0, 0.2)
        batch_size = hyper_params.Choice('batch_size', [self._batch_size])

        hyper_params={
            'lr' : lr,
            'optimizer' : optimizer,
            'conv_kernel_initializer' : conv_kernel_initializer,
            'dense_kernel_initializer' : dense_kernel_initializer,
            'cnn_activation' : cnn_activation,
            'dense_activation' : dense_activation,
            'num_fc_layers' : num_fc_layers,
            'fc_width' : fc_width,
            'dropout' : dropout,
        }
        if self._model_name == ModelType.SIMPLIFIED.value:
            model = get_cnn_stk_simplified(hyper_params=hyper_params)
        elif self._model_name == ModelType.SIMPLIFIED_MAX_POOLING.value:
            model = get_cnn_stk_simplified_max_pooling(hyper_params=hyper_params)
        elif self._model_name == ModelType.ANDRII_SIMPLIFIED.value:
            model = get_cnn_stk_andrii_simplified(hyper_params=hyper_params)
        else:
            print("No model was selected - this should never happen", file=sys.stderr)
            return 1
        return model
    # def run_trial(self, trial, *fit_args, **fit_kwargs):
    #     # Batch the data according to the hyper-parameters
    #     batch_size = trial.hyperparameters.get('batch_size')
    #     print("Batch size: {}".format(batch_size))
    #     time.sleep(5)
    #     fit_kwargs['x'] = fit_kwargs['x'].batch(batch_size)
    #     fit_kwargs['validation_data'] = fit_kwargs['validation_data'].batch(batch_size)
    #     # Run the trial withe the batched data
    #     super().run_trial(trial, *fit_args, **fit_kwargs)

    # def fit(self, hyper_parameters, model, *args, **kwargs):
    #     #Batch the tf,dataset passes as 'x' in the kwargs
    #     dataset = kwargs.pop('x').batch(hyper_parameters.get('batch_size'))
    #     valdiadtion_dataset = kwargs.pop('validation_data').batch(hyper_parameters.get('batch_size'))
    #     kwargs['x'] = dataset
    #     kwargs['validation_data'] = valdiadtion_dataset
    #     #Run the fit with the batched data
    #     return super().fit(hyper_parameters, model, *args, **kwargs )


# Convert data from tf.sparse to tf.dense
def dense_conversion(x, y):
    return tf.sparse.to_dense(x), y

def split_dataset_into_train_val_test(data_set, train_ratio, val_ratio, test_ratio):
    #Split the data into training, validation and test data
    total_samples = data_set.cardinality().numpy()
    #Check that the cardinality is known
    if total_samples == tf.data.UNKNOWN_CARDINALITY:
        raise ValueError("The cardinality of the dataset is unknown")
    print("The dataset contains {} samples".format(total_samples))
    print("The ratios are: train = {}, validation = {}, test = {}".format(train_ratio, val_ratio, test_ratio))
    train_samples = int(total_samples * train_ratio)
    val_samples = int(total_samples * val_ratio)
    test_samples = int(total_samples * test_ratio)
    print("This corresponds to {} training samples, {} validation samples and {} test samples".format(train_samples, val_samples, test_samples))
    train_data = data_set.take(train_samples)
    remaining_data = data_set.skip(train_samples)
    validation_data = remaining_data.take(val_samples)
    test_data = remaining_data.skip(test_samples)

    return train_data, validation_data, test_data


def load_datasets_from_folder(datasets_in_folder, compression):
    dataset = None
    for path in datasets_in_folder:
        try:
            if compression == "None" or compression is None:
                next_data_set = tf.data.Dataset.load(path)
            elif compression == "GZIP":
                next_data_set = tf.data.Dataset.load(path, compression=compression)
            else:
                raise ValueError("Unknown compression algorithm {}".format(compression))
            
        except (FileNotFoundError, ValueError):
            print("Failed to load the dataset {}".format(path), file=sys.stderr)
            print("Skipping this dataset")
            continue
        if dataset is None:
            dataset = next_data_set
        else:
            dataset = dataset.concatenate(next_data_set)
    if dataset is None:
        print("No dataset was loaded", file=sys.stderr)
    

    print("Loaded dataset with {} samples".format(dataset.cardinality().numpy()))
    dataset = dataset.prefetch(buffer_size=tf.data.AUTOTUNE)

    #Convert from tf.sparse to tf.dense
    dataset = dataset.map(dense_conversion, num_parallel_calls=tf.data.AUTOTUNE)

    return dataset


class LR_TensorBoard(keras.callbacks.TensorBoard):
    def __init__(self, log_dir, prediction_data, prediction_frequency, stk_writer, **kwargs):
        self._data = prediction_data
        self._log_dir = log_dir
        self._frequency = prediction_frequency
        self._stk_writer = stk_writer
        super().__init__(log_dir=log_dir, **kwargs)

    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}
        logs.update({'lr': keras.backend.eval(self.model.optimizer.lr)})
        super().on_epoch_end(epoch, logs)

    # def on_train_batch_end(self, batch, logs=None):
    #     if batch % self._frequency == 0:
    #         self._log_stk_prediction(batch, logs)    
    #     super().on_train_batch_end(batch, logs)

    def _log_stk_prediction(self, step, logs):
        #Just use the first batch of the validation data
        batch = self._data
        # Enumerate over the batch
        print("Creating the figures for tensorboard")
        batch_prediction = self.model.predict(batch)
        with self._stk_writer.as_default():
            for i, ((x, y), prediction) in enumerate(zip(batch.unbatch(), batch_prediction)):
                fig = visualize_data(x, y, prediction)
                # Save figure to the log directory
                #tf.summary.image("visualization_per_epoch_"+str(i), plot_to_tf_image(fig), step=epoch)
                tf.summary.image("visualization_per_batch_"+str(i), plot_to_tf_image(fig), step=step)
                del fig
                del x
                del y
                del prediction
    

def visualize_data(image, label, prediction=None):
    image = image.numpy()
    label = label.numpy()
    image_xz = image[0]
    image_yz = image[1]
    label_xz = label[:2]
    label_yz = label[2:]
    if prediction is not None:
        prediction_xz = prediction[:2]
        prediction_yz = prediction[2:]
    else:
        prediction_xz = None
        prediction_yz = None
    # Create a subplot with 2 rows and 1 column
    fig, (ax1, ax2) = plt.subplots(1, 2)
    #Add some padding between the subplots
    fig.tight_layout(w_pad=4, pad=6)
    image_width = image_xz.shape[0]

    for ax, image, label, prediction, plane in zip([ax1, ax2], [image_xz, image_yz], [label_xz, label_yz], [prediction_xz, prediction_yz], ["x", "y"]):
        ax.imshow(image, origin="lower", cmap="Blues", vmin=0, vmax=1)
        
        ax.set_xlabel("$\\delta {}_{{bot}} [pixel]$".format(plane))
        ax.set_ylabel("$\\delta {}_{{top}} [pixel]$".format(plane))
        # Draw a hollow (non filled) circle in the centre of the image to indicate the BGO prediction
        ax.scatter(image_width/2, image_width/2, marker="o", label="BGO track", facecolor="none", edgecolors="blue", s=15)
        # Draw the MC truth with a red circle
        ax.scatter(label[1]+image_width/2, label[0]+image_width/2, marker="o", label="MC truth", facecolor="none", edgecolors="red", s=15)
        if prediction is not None:
            ax.scatter(prediction[1]+image_width/2, prediction[0]+image_width/2, marker="^", label="CNN prediction", facecolor="none", edgecolors="green", s=15)

        #Set axis limits to the image size
        ax.set_xlim(0, image_width)
        ax.set_ylim(0, image_width)
        # Change the y and x ticks so that 0 is -200 and 400 is 200
        ax.set_xticks(np.linspace(0, image_width, 5))
        ax.set_xticklabels(np.linspace(-image_width/2, image_width/2, 5))
        ax.set_yticks(np.linspace(0, image_width, 5))
        ax.set_yticklabels(np.linspace(-image_width/2, image_width/2, 5))
    
    ax1.legend(loc="upper center", bbox_to_anchor=(1.2, 1.2), ncol=3)
    return fig


def plot_to_tf_image(figure):
    # Save the plot in a buffer
    buf = io.BytesIO()
    figure.savefig(buf, format='png')
    plt.close(figure)
    buf.seek(0)
    del figure
    # Convert PNG buffer to TF image
    image = tf.image.decode_png(buf.getvalue(), channels=4)
    del buf
    # Add the batch dimension
    image = tf.expand_dims(image, axis=0)
    return image

def train_model(train_data, validation_data, log_dir,  model, hyper_params, epochs, train_verbose=False):
    #Batch the data
    print("Batching the data with a batch size of {}".format(hyper_params['batch_size']))
    train_data = train_data.batch(hyper_params['batch_size'])
    validation_data = validation_data.batch(hyper_params['batch_size'])
    #Take one batch from the validation data without consuming it
    visualization_data = validation_data.take(1).cache().prefetch(buffer_size=tf.data.AUTOTUNE)

    #Write the figures for tensorboard
    file_writer_visualization = tf.summary.create_file_writer(log_dir + "/stk_prediction/")
    #file_writer_residual_distribution = tf.summary.create_file_writer(log_dir + "/residual_distribution/")

 
    
    # def log_residual_distribution(epoch, logs):
    #     STK_STRIP_XZ_Z_POS = [-209.58199763, -176.78199706, -143.98199706, -111.1819942,   -79.18199573, -47.18195758] #mm
    #     STK_STRIP_YZ_Z_POS = [-206.41799808, -173.61799665, -140.81799712, -108.01799655,  -76.01799274, -44.0179584 ] #mm
    #     STK_HEIGHT_XZ = STK_STRIP_XZ_Z_POS[-1] - STK_STRIP_XZ_Z_POS[0]
    #     STK_HEIGHT_YZ = STK_STRIP_YZ_Z_POS[-1] - STK_STRIP_YZ_Z_POS[0]
    #     RESOLUTION = 100 * 0.001 # mm/pixel
    #     #Use all the validation data
    #     dataset = validation_data
    #     #Predict all the labels from the validation data
    #     prediction = model.predict(dataset)

    #     #TODO: Treat 

    #     #Data is [x_top, x_bot, y_top, y_bot]
    #     #Compute x_top - x_bot and y_top - y_bot
    #     res_slope_x = (prediction[:,0]-prediction[:,1])*RESOLUTION/(STK_HEIGHT_XZ)
    #     res_slope_y = (prediction[:,2]-prediction[:,3])*RESOLUTION/(STK_HEIGHT_YZ)
    #     #Convert to degrees
    #     res_slope_x = np.rad2deg(np.arctan(res_slope_x))
    #     res_slope_y = np.rad2deg(np.arctan(res_slope_y))

    #     fig, (ax1, ax2) = plt.subplots(1,2)
    #     fig.tight_layout(w_pad=4, pad=2)

    #     for ax, res, plane in zip([ax1, ax2], [res_slope_x, res_slope_y], ["x", "y"]):
    #         bins = np.arange(-4, 4, 0.2)
    #         ax.hist(res, bins=bins, density=True, histtype='step')
    #         ax.minorticks_on()
    #         #Store the histogram as tf histogram

    #         ax.set_xlabel("$\\Delta \\Theta_{} [degree]$".format( plane))
    #     # TODO: Save the histogram to the log directory as histogram
    #     # Save figure to the log directory
    #     with file_writer_residual_distribution.as_default():
    #         tf.summary.image("residual_distribution_per_epoch", plot_to_tf_image(fig), step=epoch)
    #         tf.summary.histogram("residual_slope_x_per_epoch", res_slope_x, step=epoch, buckets=100)
    #         tf.summary.histogram("residual_slope_y_per_epoch", res_slope_y, step=epoch, buckets=100)

    tensorboard_callback = LR_TensorBoard(log_dir=log_dir, histogram_freq=1, write_steps_per_second=True, write_graph=True, update_freq=50, prediction_frequency=2000, prediction_data=visualization_data, stk_writer=file_writer_visualization)
    checkpoint_callback = keras.callbacks.ModelCheckpoint(filepath="checkpoint-{epoch:02d}-{val_loss:.2f}.hdf5", save_best_only=True)
    hyper_params_callback = tb_hp_api.KerasCallback(log_dir, hyper_params)
    learning_rate_callback = keras.callbacks.ReduceLROnPlateau(monitor='loss', factor=0.5, patience=1, min_lr=0.000001, verbose=1)
    #residual_distribution_callback = keras.callbacks.LambdaCallback(on_epoch_end=log_residual_distribution)
    print("Starting training for {} epochs".format(epochs))
    history = model.fit(train_data, validation_data=validation_data, verbose=train_verbose, epochs=epochs, callbacks=[tensorboard_callback, checkpoint_callback, hyper_params_callback, learning_rate_callback])
    print("Done training")

    return model, history



def main():
    #Uncomment for debugging
    #tf.debugging.experimental.enable_dump_debug_info(LOG_DIR, tensor_debug_mode="FULL_HEALTH", circular_buffer_size=-1)
    
    parser = argparse.ArgumentParser(description="Train a model to reconstruct the photon track from the STK hits")
    parser.add_argument("--dropout", dest="dropout", type=float, default=0, help="Dropout rate")
    parser.add_argument("--batch-size", "--bs", dest="batch_size", type=int, default=32, help="Batch size")
    parser.add_argument("--epochs", dest="epochs", type=int, default=1, help="Epochs")
    parser.add_argument("--model", dest="model", type=str, default="cnn-andrii", help="Model to use")
    parser.add_argument("--data", dest="data", type=str, default=None, help="Path to the dataset to use")
    parser.add_argument("--data-folder", dest="data_folder", type=str, default=None, help="Path to the folder containing the tf datasets. If provided this overwrites the data option,")
    parser.add_argument("--log-dir", dest="log_dir", type=str, default="logs/", help="Path to the log directory")
    parser.add_argument("--model-weights", dest="model_weights", type=str, default=None, help="Path to the model weights to use. The default is None, which means that the model will be trained from scratch.")
    parser.add_argument("--remove-outside-window", dest="remove_outside_window", action="store_true", help="Remove events with true trajectory outside of the window that are not in the window. Note that this may also be achieved during pre-processing-")
    parser.add_argument("--remove-blank", dest="remove_blank", action="store_true", help="Remove events with no hits in both projections. Note that this may also be achieved during pre-processing.")
    parser.add_argument("--optimizer", dest="optimizer", type=str, default="adam", choices=['adam', 'sgd', 'rmsprop'], help="Optimizer to use")
    parser.add_argument("--learning-rate", "--lr",  dest="learning_rate", type=float, default=1, help="Learning rate")
    parser.add_argument("--conv-kernel-initializer", dest="conv_kernel_initializer", type=str, default="glorot_normal", help="Convolutional kernel initializer")
    parser.add_argument("--dense-kernel-initializer", dest="dense_kernel_initializer", type=str, default="uniform", help="Dense kernel initializer")
    parser.add_argument("--cnn-activation", dest="cnn_activation", type=str, default="relu", help="Activation function for the convolutional layers")
    parser.add_argument("--dense-activation", dest="dense_activation", type=str, default="relu", help="Activation function for the dense layers")
    parser.add_argument("--num-fc-layers", dest="num_fc_layers", type=int, default=1, help="Number of fully connected layers")
    parser.add_argument("--fc-width", dest="fc_width", type=int, default=50, help="Width of the fully connected layers")
    parser.add_argument("--train-verbose", dest="train_verbose", type=int, default=2, help="Verbose level for the training. 0 = silent, 1 = progress bar, 2 = one line per epoch")
    parser.add_argument("--train-ratio", dest="train_ratio", type=float, default=0.7, help="Ratio of the data to use for training")
    parser.add_argument("--validation-ratio", dest="validation_ratio", type=float, default=0.15, help="Ratio of the data to use for validation")
    parser.add_argument("--test-ratio", dest="test_ratio", type=float, default=0.15, help="Ratio of the data to use for testing")
    parser.add_argument("--hp-tuning", dest="hp_tuning", action="store_true", help="Perform hyper-parameter tuning")
    parser.add_argument("--cache", dest="cache", type=str, default=None, help="Cache directory")
    parser.add_argument("--compression", dest="compression", type=str, choices=["None", "GZIP"], default="GZIP", help="Compression algorithm that was used to compress the dataset")
    args = parser.parse_args()

    datsets_in_folder = []

    #Either the data or the data folder must be provided
    if args.data is None and args.data_folder is None or args.data is not None and args.data_folder is not None:
        print("Either the data or the data folder must be provided. Exclusive or!", file=sys.stderr)
        return 1
    #Check that the data path is a tf.data.Dataset and that the folder ends with _dataset
    if args.data is not None and not os.path.isdir(args.data) \
    and not args.data.endswith(DATA_SET_SUFFIX) \
    and not os.path.isfile(os.path.join(args.data, "dataset_spec.pb")):
        print("The folder {} does not exist".format(args.data), file=sys.stderr)
        return 1
    if args.data is not None:
        datsets_in_folder.append(args.data)
    #Find all datasets in the folder and add them to the list of datasets
    if args.data_folder is not None:
        if not os.path.isdir(args.data_folder):
            print("The folder {} does not exist".format(args.data_folder), file=sys.stderr)
            return 1
        #Check if the folder contains subfolder who are tf.data.Dataset folders
        datsets_in_folder = [os.path.join(args.data_folder, f) for f in os.listdir(args.data_folder)]
        #Only keep folder that end with _dataset
        datsets_in_folder = [f for f in datsets_in_folder if f.endswith(DATA_SET_SUFFIX)]
        #Remove any folder that does not contain the dataset_spec.pb file
        datsets_in_folder = [f for f in datsets_in_folder if os.path.isfile(os.path.join(f, "dataset_spec.pb"))]

        if len(datsets_in_folder) == 0:
            print("The folder {} does not contain any tf.data.Dataset folders".format(args.data_folder), file=sys.stderr)
            return 1
       
        #Sort the datasets by name
        datsets_in_folder.sort()

        print("Found {} tf.data.Dataset:".format(len(datsets_in_folder)))
        for dataset in datsets_in_folder:
            print(dataset, end=" ")
        print()         

    #Check that the model weights file exists 
    if args.model_weights and not os.path.isfile(args.model_weights):
        print("The file {} does not exist".format(args.model_weights), file=sys.stderr)
        return 1
    #Check that selected model is valid
    if args.model not in [model.value for model in ModelType]:
        print("The model {} is not valid".format(args.model), file=sys.stderr)
        #Iterate over all the available models and list them
        print("Available models are:")
        for model in ModelType:
            print(model.value)
        return 1
    if args.log_dir and args.log_dir[-1] != '/':
        args.log_dir += '/'

    #Check that the cache directory exists
    if args.cache is not None and not os.path.isdir(args.cache):
        print("The cache directory {} does not exist".format(args.cache), file=sys.stderr)
        return 1
    

    MODEL_NAME = args.model
    EPOCHS = args.epochs
    TRAIN_VERBOSE = args.train_verbose
    
    #GPU setting and dump info
    gpus = tf.config.experimental.list_physical_devices('GPU')
    if gpus:
        try:
            for gpu in gpus:
                tf.config.experimental.set_memory_growth(gpu, True)
        except RuntimeError as e:
            print(e)

    print("Num GPUs Available: ", len(tf.config.list_physical_devices('GPU')))
    if len(gpus) > 0:
        print("Memory available: ", tf.config.experimental.get_virtual_device_configuration(gpus[0]))

    #Define the hyper-parameters for tensorboard
    HP_lr = tb_hp_api.HParam('lr', tb_hp_api.RealInterval(0.0001, 1.0))
    HP_optimizer = tb_hp_api.HParam('optimizer', tb_hp_api.Discrete(['adam', 'sgd', 'rmsprop']))
    HP_cnn_kernel_initializer = tb_hp_api.HParam('conv_kernel_initializer', tb_hp_api.Discrete(['glorot_uniform', 'glorot_normal', 'he_uniform', 'he_normal', 'None']))
    HP_fc_kernel_initializer = tb_hp_api.HParam('dense_kernel_initializer', tb_hp_api.Discrete(['glorot_uniform', 'glorot_normal', 'he_uniform', 'he_normal', 'None']))
    HP_cnn_activation = tb_hp_api.HParam('cnn_activation', tb_hp_api.Discrete(['relu', 'tanh', 'sigmoid', 'leakyrelu']))
    HP_dense_activation = tb_hp_api.HParam('dense_activation', tb_hp_api.Discrete(['relu', 'tanh', 'sigmoid', 'leakyrelu']))
    HP_num_fc_layers = tb_hp_api.HParam('num_fc_layers', tb_hp_api.IntInterval(1, 5))
    HP_fc_width = tb_hp_api.HParam('fc_width', tb_hp_api.IntInterval(1, 100))
    HP_dropout = tb_hp_api.HParam('dropout', tb_hp_api.RealInterval(0.0, 1.0))
    HP_batch_size = tb_hp_api.HParam('batch_size', tb_hp_api.IntInterval(0, 256))


    #Choose the hyper-parameters to use
    tb_hp_api.hparams_config(
        hparams=[HP_lr, HP_optimizer, HP_cnn_kernel_initializer, HP_fc_kernel_initializer, HP_cnn_activation, HP_dense_activation, HP_num_fc_layers, HP_fc_width, HP_dropout, HP_batch_size],
        metrics=[tb_hp_api.Metric('mean_absolute_error', display_name='MAE'), tb_hp_api.Metric('mean_squared_error', display_name='MSE')],
    )
    hyper_params = {
        'lr' : args.learning_rate,
        'optimizer' : args.optimizer,
        'conv_kernel_initializer' : args.conv_kernel_initializer,
        'dense_kernel_initializer' : args.dense_kernel_initializer,
        'cnn_activation' : args.cnn_activation,
        'dense_activation' : args.dense_activation,
        'num_fc_layers' : args.num_fc_layers,
        'fc_width' : args.fc_width,
        'dropout' : args.dropout,
        'batch_size' : args.batch_size,
    }

    #Load the dataset
    complete_dataset = load_datasets_from_folder(datsets_in_folder, compression=args.compression)
    #Split the dataset into training, validation and test data
    train_data, validation_data, test_data = split_dataset_into_train_val_test(complete_dataset, args.train_ratio, args.validation_ratio, args.test_ratio)

    #Clean the data
    if args.remove_outside_window or args.remove_blank:
        print ("Cleaning the data")
        if args.remove_outside_window:
            print("Removing events with true trajectory outside of the window")
            def keep_true_trajectory_inside(x, y):
                return tf.math.reduce_all(tf.math.abs(y) < 200)
            
            
            train_data = train_data.filter(keep_true_trajectory_inside)
            validation_data = validation_data.filter(keep_true_trajectory_inside)
            test_data = test_data.filter(keep_true_trajectory_inside)
            print("Done")
        if args.remove_blank:
            print("Removing events with no hits in both projections")
            def keep_non_blank(x, y):
                #Check that at least one of the projections contains a non zero pixel
                return tf.math.reduce_any(tf.math.reduce_sum(x, axis=(1, 2)) > 0)
            
            
            train_data = train_data.filter(keep_non_blank)
            validation_data = validation_data.filter(keep_non_blank)
            test_data = test_data.filter(keep_non_blank)
            print("Done")
    #Cache the data
    if args.cache is not None:
        print("Caching the data in {}".format(args.cache))
        train_data = train_data.cache(filename=os.path.join(args.cache, "train.cache"))
        validation_data = validation_data.cache(filename=os.path.join(args.cache, "validation.cache"))
        test_data = test_data.cache(filename=os.path.join(args.cache, "test.cache"))

    if args.hp_tuning:
        tuner = kt.Hyperband(
            hypermodel=MyHyperModel(args.batch_size, args.model),
            objective='val_loss',
            max_epochs=15,
            directory=args.log_dir,
            project_name='stk_cnn_hyperband',
            overwrite=True)
        train_data = train_data.batch(args.batch_size)
        validation_data = validation_data.batch(args.batch_size)
        tuner.search_space_summary()
        #learning_rate_callback = keras.callbacks.ReduceLROnPlateau(monitor='loss', factor=0.5, patience=1, min_lr=0.000001, verbose=1)
        #checkpoint_callback = keras.callbacks.ModelCheckpoint(filepath="checkpoint-{epoch:02d}-{val_loss:.2f}.hdf5", save_best_only=True)
        tuner.search(x=train_data, epochs=EPOCHS, validation_data=validation_data, verbose=args.train_verbose, callbacks=[keras.callbacks.TensorBoard(log_dir=args.log_dir+tuner.project_name, histogram_freq=1, write_graph=True, update_freq='batch')])
        return
    else:
        #Prepare the model
        model = None
        print("Preparing the model")
        if MODEL_NAME == ModelType.SIMPLIFIED.value:
            model = get_cnn_stk_simplified(hyper_params=hyper_params)
        elif MODEL_NAME == ModelType.SIMPLIFIED_MAX_POOLING.value:
            model = get_cnn_stk_simplified_max_pooling(hyper_params=hyper_params)
        elif MODEL_NAME == ModelType.ANDRII_SIMPLIFIED.value:
            model = get_cnn_stk_andrii_simplified(hyper_params=hyper_params)
        else:
            print("No model was selected - this should never happen", file=sys.stderr)
            return 1
        #Plot the model
        print("Saving to model architecture visualisation to {}.png".format(MODEL_NAME))
        keras.utils.plot_model(model, "{}.png".format(MODEL_NAME), show_shapes=True)
        #Apply the model weights if they exist
        if args.model_weights:
            print("Loading the weights")
            try:
                model.load_weights(args.model_weights)
            except ValueError:
                print("The model weights could not be loaded. Probably the model architecture does not match the weights", file=sys.stderr)
                #Create a new model
                model = tf.keras.models.load_model(args.model_weights)

        log_dir = args.log_dir  + time.strftime("%Y%m%d-%H%M%S")
        model, history = train_model(train_data=train_data, validation_data=validation_data, log_dir=log_dir,
                                    model=model, hyper_params=hyper_params, epochs=EPOCHS, train_verbose=TRAIN_VERBOSE)
        # Save the model and the training statistics
        name = "{}_lr{}_{}_conv{}_dense{}_cnn{}_dense{}_fc{}_{}_dropout{}_batch{}".format(
            MODEL_NAME, hyper_params['lr'], hyper_params['optimizer'], hyper_params['conv_kernel_initializer'],
            hyper_params['dense_kernel_initializer'], hyper_params['cnn_activation'], hyper_params['dense_activation'],
            hyper_params['num_fc_layers'], hyper_params['fc_width'], hyper_params['dropout'], hyper_params['batch_size'])
        model.save(name+".h5")
        print("Saved model to {}".format(name+".h5"))
        # Save the training history
        np.save(name+"_history.npy", history.history)
        print("Saved training history to {}".format(name+"_history.npy"))
        
        print("Done with training")
        return 0

if __name__ == "__main__":
    main()
